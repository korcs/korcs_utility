﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace CommonPackage
{
    public class DatabaseEditorWindow : EditorWindow
    {
        const string FILE_PATH = "Database";
		const int OFFSET_X = 160;
		const int MARGIN = 6;
        static Color NORMAL_COLOR = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		static Color BG_COLOR = new Color32(165, 165, 165, 255);
		static Color BG_COLOR_PRO = new Color32(88, 88, 88, 255);
		static Color SELECT_COLOR = new Color32(58, 114, 176, 255);

		const string LIST_FILE_NAME = "objectListData.asset";

		[SerializeField]
        string fileName = "";
        [SerializeField]
        DatabaseObject data;
		[SerializeField]
		DatabaseListObject listData;
        [SerializeField]
        Vector2 scrollPos;
		[SerializeField]
		Vector2 scrollPos2;

		[SerializeField]
        bool isEdit = false;
		[SerializeField]
		int listIndex = 0;

		ReorderableList reorderableList = null;
		GUIStyle style = new GUIStyle();

		DateTime preDateTime = DateTime.Now;
		double timeCount = 0;

		DatabaseListObject ListData
		{
			get
			{
				if (listData == null)
				{
					string path = Application.dataPath + "/" + FILE_PATH + "/" + LIST_FILE_NAME;
					if (System.IO.File.Exists(path))
					{
						path = "Assets" + path.Replace(Application.dataPath, "");
						listData = (DatabaseListObject)AssetDatabase.LoadAssetAtPath(path, typeof(DatabaseListObject));
					}
					else
					{
						listData = DatabaseListObject.Create();
					}
				}
				return listData;
			}
		}

		[MenuItem("Tools/korcs/Database Editor", priority = 0)]
        static void Open()
        {
            GetWindow<DatabaseEditorWindow>("Database Editor");
        }

        void OnEnable()
        {
			//titleContent.text = "Database Editor";
			//Texture2D tex = new Texture2D(16, 16, TextureFormat.ARGB32, false);
			//tex.LoadImage(new byte[] { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 16, 0, 0, 0, 16, 8, 6, 0, 0, 0, 31, 243, 255, 97, 0, 0, 0, 133, 73, 68, 65, 84, 56, 17, 165, 81, 139, 10, 192, 32, 8, 172, 177, 143, 233, 255, 127, 112, 195, 193, 137, 156, 103, 14, 26, 68, 102, 247, 202, 205, 181, 214, 56, 249, 238, 130, 252, 136, 254, 20, 189, 113, 137, 166, 34, 27, 76, 246, 89, 32, 130, 204, 17, 11, 62, 241, 254, 235, 177, 0, 128, 28, 151, 207, 192, 201, 39, 216, 101, 114, 114, 6, 21, 156, 160, 116, 34, 158, 31, 213, 95, 80, 34, 101, 34, 78, 224, 202, 161, 136, 228, 36, 222, 9, 108, 201, 102, 210, 9, 32, 72, 114, 198, 197, 95, 1, 224, 211, 174, 134, 24, 65, 165, 51, 64, 93, 2, 155, 65, 156, 3, 120, 190, 119, 2, 14, 172, 138, 227, 39, 188, 204, 247, 11, 162, 166, 132, 2, 106, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130 });
			//tex.filterMode = FilterMode.Point;
			//titleContent.image = tex;
			InitReorderableList();

			Repaint();
        }

        void OnDisable()
        {
            Repaint();
        }

        void OnGUI()
        {
            EditorGUI.BeginDisabledGroup(Application.isPlaying);

			using (new GUILayout.AreaScope(new Rect(MARGIN, MARGIN, OFFSET_X - (MARGIN * 2), position.height - (MARGIN * 2))))
			{
				for (int i = 0; i < ListData.items.Count; i++)
				{
					if (GUILayout.Toggle(i == listIndex, ListData.items[i].dataName, EditorStyles.toolbarButton))
					{
						if (listIndex != i)
						{
							listIndex = i;
							Load(AssetDatabase.GetAssetPath(ListData.items[i]));
						}
					}
				}
			}

			// 設定
			using (new GUILayout.AreaScope(new Rect(OFFSET_X + MARGIN, MARGIN, position.width - (OFFSET_X + MARGIN * 2), 80)))
            {
                using (new GUILayout.HorizontalScope())
                {
                    using (new GUILayout.VerticalScope(GUILayout.Width(80)))
                    {
                        if (GUILayout.Button("新規作成"))
                        {
                            New();
                        }
                        if (GUILayout.Button("読込"))
                        {
                            Load();
                        }
                        if (GUILayout.Button("保存"))
                        {
                            Save();
                        }
                    }
					//GUILayout.FlexibleSpace();
					//using (new GUILayout.VerticalScope())
					//{
					//	EditorGUILayout.BeginHorizontal();
					//	for (int i = 0; i < ListData.items.Count; i++)
					//	{
					//		if (GUILayout.Toggle(i == listIndex, ListData.items[i].dataName, EditorStyles.toolbarButton))
					//		{
					//			if (listIndex != i)
					//			{
					//				listIndex = i;
					//				Load(AssetDatabase.GetAssetPath(ListData.items[i]));
					//			}
					//		}
					//		if (i > 0 && (i + 1) % 4 == 0)
					//		{
					//			EditorGUILayout.EndHorizontal();
					//			EditorGUILayout.BeginHorizontal();
					//		}
					//	}
					//	EditorGUILayout.EndHorizontal();
					//}
					GUILayout.FlexibleSpace();
					if (data != null)
                    {
                        using (new GUILayout.VerticalScope(GUILayout.Width(200)))
                        {
                            using (new GUILayout.HorizontalScope())
                            {
                                EditorGUI.BeginDisabledGroup(AssetDatabase.GetAssetPath(data).Length > 0);
                                GUILayout.Label("ファイル名");
                                fileName = EditorGUILayout.TextField(fileName);
                                GUILayout.Label(".asset");
                                EditorGUI.EndDisabledGroup();
                            }
							using (new GUILayout.HorizontalScope())
							{
								GUILayout.Label("名前");
								data.dataName = EditorGUILayout.TextField(data.dataName);
							}
							isEdit = EditorGUILayout.Toggle("データ設定", isEdit);
                            if (GUILayout.Button("データ追加"))
                            {
                                Undo.RecordObject(data, "Edit Database");
                                data.items.Add(new DatabaseObject.Item());
                            }
                            //data.type = (DatabaseObject.eType)EditorGUILayout.EnumPopup(data.type);
                        }
                    }
                }
            }

            // 設定
            var rect = new Rect(OFFSET_X, 80 + MARGIN * 2, position.width - OFFSET_X, position.height - (80 + MARGIN * 2));
            using (new GUILayout.AreaScope(rect))
            {
                if (data == null)
                {
                    GUILayout.Label("データベースを読み込むか新規作成してください");
                }
                else
                {
                    using (var scrollView = new GUILayout.ScrollViewScope(scrollPos))
                    {
                        if (scrollPos != scrollView.scrollPosition)
                            GUI.FocusControl("");
                        scrollPos = scrollView.scrollPosition;
                        Rect maskRect = new Rect(scrollPos.x, scrollPos.y, position.width - 16, position.height - (80 + MARGIN * 2) - 16);
                        //data.ShowGUI(maskRect);

                        if (isEdit)
                        {
                            ShowSettings(maskRect);
                        }
                        else
                        {
                            ShowItems(maskRect);
                        }
                    }
                }
            }

            if (!Application.isPlaying)
            {
                OnEvent(Event.current);
            }
            else
            {
                GUI.color = new Color(0, 0, 0, 0.5f);
                GUI.DrawTexture(new Rect(0, 0, position.width, position.height), Texture2D.whiteTexture);
                GUI.color = Color.white;
            }

            EditorGUI.EndDisabledGroup();

			TimeCount();
			Repaint();
		}

        void ShowItems(Rect maskRect)
        {
            data.OnEditorGUI(maskRect, timeCount);
        }

        void ShowSettings(Rect maskRect)
        {
            GUILayout.BeginHorizontal();
            using (new GUILayout.VerticalScope(GUILayout.Width(360)))
            {
                using (new GUILayout.HorizontalScope())
                {
                    GUILayout.Label("幅", GUILayout.Width(80));
                    data.contentWidth = EditorGUILayout.IntSlider(data.contentWidth, 200, 1000);
                }
                using (new GUILayout.HorizontalScope())
                {
                    GUILayout.Label("高さ", GUILayout.Width(80));
                    data.contentHeight = EditorGUILayout.IntSlider(data.contentHeight, 40, 400);
                }
                using (new GUILayout.HorizontalScope())
                {
                    GUILayout.Label("横の数", GUILayout.Width(80));
                    data.contentHNum = EditorGUILayout.IntSlider(data.contentHNum, 1, 10);
                }
                using (new GUILayout.HorizontalScope())
                {
                    GUILayout.Label("テクスチャ");
                    data.texture = EditorGUILayout.ObjectField(data.texture, typeof(Texture2D), false) as Texture2D;
                    GUILayout.Label("サイズ");
                    data.chipSize = EditorGUILayout.IntField(data.chipSize, GUILayout.Width(40));
                    GUILayout.Label("横の数");
                    data.chipHNum = EditorGUILayout.IntField(data.chipHNum, GUILayout.Width(40));
                }
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("アニメスピード");
					data.animSpeed = EditorGUILayout.FloatField(data.animSpeed, GUILayout.Width(40));
				}
				GUILayout.Space(MARGIN);
#if false
				for (int i = 0; i < data.itemSettings.Count; i++)
				{
					GUILayout.BeginHorizontal(GUI.skin.box);
					data.itemSettings[i].type = (DatabaseObject.ItemSetting.eType)EditorGUILayout.EnumPopup(data.itemSettings[i].type);
					if (data.itemSettings[i].type >= DatabaseObject.ItemSetting.eType.INT_FIELD)
					{
						GUILayout.Label("Key");
						data.itemSettings[i].key = EditorGUILayout.TextField(data.itemSettings[i].key);
						GUILayout.Label("W");
						data.itemSettings[i].width = EditorGUILayout.IntField(data.itemSettings[i].width);
						GUILayout.Label("H");
						data.itemSettings[i].height = EditorGUILayout.IntField(data.itemSettings[i].height);
					}
					else if (data.itemSettings[i].type == DatabaseObject.ItemSetting.eType.NONE)
					{
						if (GUILayout.Button("削除"))
						{
							Undo.RecordObject(data, "Edit Database");
							data.itemSettings.RemoveAt(i);
						}
					}
					GUILayout.EndHorizontal();
				}
				GUILayout.Space(MARGIN);
				if (GUILayout.Button("追加"))
				{
					Undo.RecordObject(data, "Edit Database");
					data.itemSettings.Add(new DatabaseObject.ItemSetting());
				}
#else
				if (reorderableList != null)
				{
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.FlexibleSpace();
						EditorGUI.BeginDisabledGroup(!CanAdd(reorderableList));
						if (GUILayout.Button("+"))
						{
							Add(reorderableList);
						}
						EditorGUI.BeginDisabledGroup(!CanRemove(reorderableList));
						if (GUILayout.Button("-"))
						{
							Remove(reorderableList);
						}
						EditorGUI.EndDisabledGroup();
					}
					using (var scrollView = new GUILayout.ScrollViewScope(scrollPos2))
					{
						scrollPos2 = scrollView.scrollPosition;
						reorderableList.DoLayoutList();
					}
				}
#endif
			}
            GUILayout.FlexibleSpace();

            EditorGUI.BeginDisabledGroup(true);
            data.OnEditorGUI(maskRect, 0, timeCount);
            EditorGUI.EndDisabledGroup();

            GUILayout.EndHorizontal();
		}

        void OnEvent(Event evt)
        {
            if (evt.type == EventType.MouseDown)
            {
                GUI.FocusControl("");
                Repaint();
            }
            if (evt.type == EventType.KeyUp)
            {
                if (evt.keyCode == KeyCode.S && evt.shift)
                {
                    Save();
                }
                Repaint();
            }
            if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
            {
                // D&D
                var dropArea = new Rect(0, 0, position.width, position.height);
                if (dropArea.Contains(evt.mousePosition))
                {
                    //マウスの形状
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (evt.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();
                        var load = (DatabaseObject)AssetDatabase.LoadAssetAtPath(DragAndDrop.paths[0], typeof(DatabaseObject));
                        if (load != null)
                        {
                            data = load;
                            var tmp = DragAndDrop.paths[0].Split('/');
                            fileName = tmp[tmp.Length - 1].Replace(".asset", "");
                        }
                        DragAndDrop.activeControlID = 0;
                    }
                    evt.Use();
                }
            }
        }

		void TimeCount()
		{
			var t = DateTime.Now - preDateTime;
			timeCount += t.TotalSeconds;
			preDateTime = DateTime.Now;
		}

		#region ReorderableList
		public void InitReorderableList()
		{
			if (data != null)
			{
				reorderableList = new ReorderableList(data.itemSettings, typeof(DatabaseObject.ItemSetting), true, false, false, false);

				reorderableList.onAddCallback += Add;
				reorderableList.onRemoveCallback += Remove;
				reorderableList.onCanAddCallback += CanAdd;
				reorderableList.onCanRemoveCallback += CanRemove;
				reorderableList.onMouseUpCallback += OnMouseUp;
				reorderableList.drawElementCallback += DrawElement;
				reorderableList.drawElementBackgroundCallback += DrawElementBackground;
				reorderableList.drawHeaderCallback += DrawHeader;
				reorderableList.headerHeight = 0;
				reorderableList.footerHeight = 0;
				reorderableList.elementHeight = EditorGUIUtility.singleLineHeight + MARGIN * 2;
			}
		}

		void Add(ReorderableList list)
		{
			if (reorderableList.index >= 0)
				data.itemSettings.Insert(reorderableList.index, new DatabaseObject.ItemSetting());
			else
				data.itemSettings.Add(new DatabaseObject.ItemSetting());
		}
		void Remove(ReorderableList list)
		{
			data.itemSettings.RemoveAt(reorderableList.index);
		}
		bool CanAdd(ReorderableList list)
		{
			return data != null;
		}
		bool CanRemove(ReorderableList list)
		{
			return data != null && data.itemSettings.Count > reorderableList.index && reorderableList.index >= 0;
		}
		void OnMouseUp(ReorderableList list)
		{

		}
		void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			if (data.itemSettings.Count <= index)
			{
				return;
			}
			if (data.itemSettings[index] == null)
			{
				data.itemSettings[index] = new DatabaseObject.ItemSetting();
			}

			if (isActive || isFocused)
			{
				style.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.white;
			}
			else
			{
				style.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
			}
			style.alignment = TextAnchor.MiddleCenter;

			rect.yMax -= MARGIN;
			rect.yMin += MARGIN;
			rect.xMin += MARGIN;
			rect.xMax -= MARGIN;
			
			if (data.itemSettings[index].type >= DatabaseObject.ItemSetting.eType.INT_FIELD)
			{
				float w = rect.width;
				rect.width = w / 4;
				data.itemSettings[index].type = (DatabaseObject.ItemSetting.eType)EditorGUI.EnumPopup(rect, data.itemSettings[index].type);
				rect.x += rect.width;
				rect.width = w / 8;
				GUI.Label(rect, "Key", style);
				rect.x += rect.width;
				rect.width = w / 6;
				data.itemSettings[index].key = EditorGUI.TextField(rect, data.itemSettings[index].key);
				rect.x += rect.width;
				rect.width = w / 16;
				GUI.Label(rect, "W", style);
				rect.x += rect.width;
				rect.width = w / 6;
				data.itemSettings[index].width = EditorGUI.IntField(rect, data.itemSettings[index].width);
				rect.x += rect.width;
				rect.width = w / 16;
				GUI.Label(rect, "H", style);
				rect.x += rect.width;
				rect.width = w / 6;
				data.itemSettings[index].height = EditorGUI.IntField(rect, data.itemSettings[index].height);
			}
			else
			{
				data.itemSettings[index].type = (DatabaseObject.ItemSetting.eType)EditorGUI.EnumPopup(rect, data.itemSettings[index].type);
			}
		}
		void DrawElementBackground(Rect rect, int index, bool isActive, bool isFocused)
		{
			if (isActive || isFocused)
			{
				EditorGUI.DrawRect(rect, EditorGUIUtility.isProSkin ? SELECT_COLOR : SELECT_COLOR);
			}
			else if (index % 2 == 1)
			{
				EditorGUI.DrawRect(rect, EditorGUIUtility.isProSkin ? BG_COLOR_PRO : BG_COLOR);
			}
		}
		void DrawHeader(Rect rect)
		{
			EditorGUI.LabelField(rect, "エディタ設定");
		}
#endregion

		void New()
        {
            data = DatabaseObject.Create();
            fileName = "new";
			AddList(data);

			InitReorderableList();
		}

        void Save()
        {
            var path = AssetDatabase.GetAssetPath(data);
            if (path.Length > 0)
            {
                EditorUtility.SetDirty(data);
            }
            else
            {
                path = EditorUtility.SaveFilePanelInProject("データ保存", fileName, "asset", "message");
                if (path.Length > 0)
                {
                    Utility.CreateFolder(path);
                    AssetDatabase.CreateAsset(data, path);
                }
            }
			string listPath = Application.dataPath + "/" + FILE_PATH + "/" + LIST_FILE_NAME;
			if (!System.IO.File.Exists(listPath))
			{
				listPath = "Assets" + listPath.Replace(Application.dataPath, "");
				Utility.CreateFolder(listPath);
				AssetDatabase.CreateAsset(ListData, listPath);
			}
			else
			{
				EditorUtility.SetDirty(ListData);
			}
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

        void Load()
        {
            var path = EditorUtility.OpenFilePanel("データ読込", Application.dataPath + "/" + FILE_PATH, "asset");
			if (System.IO.File.Exists(path))
			{
				path = "Assets" + path.Replace(Application.dataPath, "");
				Load(path);
			}
			else
			{
				New();
			}
			
			AddList(data);
			for (int i = 0; i < ListData.items.Count; i++)
			{
				if (ListData.items[i] == data)
				{
					listIndex = i;
					break;
				}
			}
		}
		void Load(string path)
		{
			var load = (DatabaseObject)AssetDatabase.LoadAssetAtPath(path, typeof(DatabaseObject));
			if (load != null)
			{
				data = load;
				fileName = path.Replace("Assets/" + FILE_PATH + "/", "").Replace(".asset", "");
			}
			AssetDatabase.Refresh();
			InitReorderableList();
		}

		void AddList(DatabaseObject d)
		{
			var tmp = ListData.items;
			if (!tmp.Contains(d))
			{
				tmp.Add(d);
				ListData.items = tmp;
			}

			EditorUtility.SetDirty(ListData);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
    }
}