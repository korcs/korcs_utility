﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CommonPackage.Chiptune;

#pragma warning disable 0168, 0219

namespace CommonPackage
{
    public class StageEditorWindow : EditorWindow
    {
        const string FILE_PATH = "/Database/StageData";
        const int LAYER_MAX = 16;
        const int SELECT_CHIP_DRAW_SIZE = 32;
        const int SELECT_CHIP_H_NUM = 4;
        const int MAP_OFFSET_X = (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2) * SELECT_CHIP_H_NUM + 22;
        const int MAP_OFFSET_Y = 132;
        const int MARGIN = 6;
        const int CHIP_MARGIN = 2;
        static Color NORMAL_COLOR = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        static Color UNDER_COLOR = new Color(0.3f, 0.3f, 0.3f, 0.5f);
        static Color UPPER_COLOR = new Color(0.6f, 0.6f, 0.6f, 0.2f);
        static Color GUID_COLOR = new Color(0.1f, 0.1f, 0.1f, 0.05f);
        static Color EVENT_COLOR = new Color(0.5f, 0.1f, 0.1f, 0.1f);
        static Color EVENT_SELECT_COLOR = new Color(1f, 0.1f, 0.1f, 1f);

        Texture2D mapTexture
        {
            get
            {
                return (stageData && stageData.tileData) ? stageData.tileData.texture : null;
            }
        }

        int select;
        Vector2 scrollPos;
        Vector2 mapScrollPos;
        Vector2 mapScrollPosBegin;
        Vector2 mapScrollTouchBegin;
        int editMode = 0;
        Vector2 prePos;
        //int selectEnemy = 0;
        //int selectStart = 0;
        int selectTag = 0;
        int selectEvent = 0;

        float zoom = 1;
        float CHIP_DRAW_SIZE { get { return 32 * zoom; } }
		int CHIP_SIZE { get { return stageData != null && stageData.tileData != null ? stageData.tileData.size : 16; } }

		[SerializeField]
        StageDataObject stageData;

		EventEditorCore eventCore = null;

        DateTime preDateTime = DateTime.Now;
        double timeCount = 0;

        enum eEditMode : int
        {
            MAP = 0,
            TAG,
            EVENT,
        }


        const string ENCRYPT_KEY = "FCyfCvpSss4d3cqrj2IaQWxEFih2XHz1";

        [MenuItem("Tools/korcs/Stage Editor", priority = 10)]
        static void Open()
        {
            GetWindow<StageEditorWindow>("Stage Editor");
        }

        void OnEnable()
        {
            //titleContent.text = "Stage Editor";
            //Texture2D tex = new Texture2D(16, 16, TextureFormat.ARGB32, false);
            //tex.LoadImage(new byte[]{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,16,0,0,0,16,8,6,0,0,0,31,243,255,97,0,0,0,126,73,68,65,84,56,17,221,146,65,14,192,32,8,4,161,233,99,120,138,79,246,119,54,80,247,80,178,164,38,30,154,148,139,97,148,85,86,212,204,100,39,142,162,120,16,206,152,84,2,164,158,163,239,5,206,249,48,214,223,10,83,8,104,234,208,139,181,181,22,184,247,238,43,99,175,38,122,209,128,80,168,221,66,193,156,111,155,168,197,32,197,115,231,141,88,24,19,120,224,155,57,86,216,31,76,132,7,185,127,228,49,31,115,14,30,12,73,245,141,121,176,252,60,99,114,1,161,81,42,73,57,189,5,25,0,0,0,0,73,69,78,68,174,66,96,130});
            //tex.filterMode = FilterMode.Point;
            //titleContent.image = tex;
            Repaint();
        }

        void OnDisable()
        {
            Repaint();
        }

        void OnGUI()
        {
            if (stageData == null)
            {
                New();
            }

            EditorGUI.BeginDisabledGroup(Application.isPlaying);

            float barH = 24;
            float w = (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2) * SELECT_CHIP_H_NUM;
            float h = (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2) * (128 / SELECT_CHIP_H_NUM) - barH;
            editMode = GUILayout.Toolbar(editMode, new string[] { "MAP", "TAG", "EVENT" }, GUILayout.Width(w), GUILayout.Height(barH));
            switch (editMode)
            {
                case (int)eEditMode.MAP:
                    // マップチップ
                    using (var scrollView = new GUI.ScrollViewScope(new Rect(0, barH, w + 16, position.height - barH), scrollPos, new Rect(0, 0, w, h)))
                    {
                        scrollPos = scrollView.scrollPosition;
                        Rect buttonRect = new Rect(0, 0, SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2, SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2);
                        Rect imageRect = new Rect(0, 0, SELECT_CHIP_DRAW_SIZE, SELECT_CHIP_DRAW_SIZE);
                        Rect maskRect = new Rect(0, scrollPos.y, w, position.height);

                        if (mapTexture != null)
                        {
                            for (int i = 0; stageData.tileData != null && i < stageData.tileData.tiles.Count; i++)
                            {
                                //int n = stageData.tileData.tiles[i].ids[0];
                                buttonRect.x = i % SELECT_CHIP_H_NUM * (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2);
                                buttonRect.y = i / SELECT_CHIP_H_NUM * (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2);
                                imageRect.x = i % SELECT_CHIP_H_NUM * (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2) + CHIP_MARGIN;
                                imageRect.y = i / SELECT_CHIP_H_NUM * (SELECT_CHIP_DRAW_SIZE + CHIP_MARGIN * 2) + CHIP_MARGIN;
                                if (GUI.Toggle(buttonRect, select == i, "", GUI.skin.button))
                                {
                                    select = i;
                                }
                                DrawMapChip(imageRect, stageData.tileData.tiles[i], NORMAL_COLOR, maskRect);
                            }
                        }
                    }
                    break;
                case (int)eEditMode.TAG:
                    using (new GUILayout.AreaScope(new Rect(0, barH, w, position.height - barH)))
                    {
                        if (stageData.tileData != null && stageData.tileData.tagData != null)
                        {
                            for (int i = 0; i < stageData.tileData.tagData.tags.Count; i++)
                            {
                                if (GUILayout.Toggle(selectTag == i, stageData.tileData.tagData.tags[i].name, GUI.skin.button))
                                {
                                    selectTag = i;
                                }
                            }
                        }
                    }
                    break;
                case (int)eEditMode.EVENT:
                    using (new GUILayout.AreaScope(new Rect(0, barH, w + 16, position.height - barH)))
                    {
						if (stageData.events == null) stageData.events = new List<StageDataObject.Event>();
						if (selectEvent >= 0 && selectEvent < stageData.events.Count && stageData.events[selectEvent] != null)
						{
							if (GUILayout.Button("削除"))
							{
								var ev = stageData.events[selectEvent].ev;
								if (ev != null && AssetDatabase.IsSubAsset(ev))
								{
									AssetDatabase.RemoveObjectFromAsset(ev);
								}
								stageData.events.RemoveAt(selectEvent);
								if (selectEvent >= stageData.events.Count)
								{
									selectEvent = 0;
								}
							}
						}
						if (selectEvent >= 0 && selectEvent < stageData.events.Count && stageData.events[selectEvent] != null)
						{
							var pos = EditorGUILayout.Vector2IntField("場所", new Vector2Int(stageData.events[selectEvent].x, stageData.events[selectEvent].y));
                            stageData.events[selectEvent].x = pos.x;
                            stageData.events[selectEvent].y = pos.y;

                            using (new GUILayout.HorizontalScope())
                            {
                                GUILayout.Label("isLocal", GUILayout.Width(60));
                                stageData.events[selectEvent].isLocal = EditorGUILayout.Toggle(stageData.events[selectEvent].isLocal);
                            }

							if (eventCore == null) eventCore = CreateInstance<EventEditorCore>();
                            if (!stageData.events[selectEvent].isLocal)
							{
                                stageData.events[selectEvent].ev = EditorGUILayout.ObjectField(stageData.events[selectEvent].ev, typeof(EventDataObject), false) as EventDataObject;

                            }
							else if (stageData.events[selectEvent].ev == null)
							{
								stageData.events[selectEvent].ev = CreateInstance<EventDataObject>();
								stageData.events[selectEvent].ev.name = "event";
								AssetDatabase.AddObjectToAsset(stageData.events[selectEvent].ev, stageData);
							}

                            if (stageData.events[selectEvent].ev != null)
							{
                                eventCore.SetData(stageData.events[selectEvent].ev);
                                eventCore.popupRect.x = MAP_OFFSET_X;
                                eventCore.popupRect.y = 0;

                                eventCore.ShowItems((int)w + 16);
                            }
						}
                    }
                    break;
            }


            // 設定
            using (new GUILayout.AreaScope(new Rect(w + 16 + MARGIN, MARGIN, position.width - (w + 16 + MARGIN * 2), 120)))
            {
                using (new GUILayout.HorizontalScope())
                {
                    using (new GUILayout.VerticalScope(GUILayout.Width(80)))
                    {
                        if (GUILayout.Button("新規作成"))
                        {
                            New();
                        }
                        if (GUILayout.Button("読込"))
                        {
                            Load();
                        }
                        if (GUILayout.Button("保存"))
                        {
                            Save();
                        }
                    }
                    GUILayout.FlexibleSpace();
                    using (new GUILayout.VerticalScope(GUILayout.Width(200)))
                    {
                        using (new GUILayout.HorizontalScope())
                        {
                            EditorGUI.BeginDisabledGroup(AssetDatabase.GetAssetPath(stageData).Length > 0);
                            GUILayout.Label("ファイル名", GUILayout.Width(80));
                            stageData.fileName = GUILayout.TextField(stageData.fileName, 13);
                            EditorGUI.EndDisabledGroup();
                        }
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("ステージ名", GUILayout.Width(80));
                            stageData.name = EditorGUILayout.TextField(stageData.name);
                        }
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("幅", GUILayout.Width(80));
                            stageData.sizeX = (byte)(Mathf.Clamp(EditorGUILayout.IntField(stageData.sizeX / stageData.screenSizeX), 1, 128) * stageData.screenSizeX);
                        }
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("高さ", GUILayout.Width(80));
                            stageData.sizeY = (byte)(Mathf.Clamp(EditorGUILayout.IntField(stageData.sizeY / stageData.screenSizeY), 1, 128) * stageData.screenSizeY);
                        }
                    }
                    using (new GUILayout.VerticalScope(GUILayout.Width(200)))
                    {
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("タイルデータ", GUILayout.Width(80));
                            stageData.tileData = EditorGUILayout.ObjectField(stageData.tileData, typeof(TileDataObject), false) as TileDataObject;
                        }
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("BGM", GUILayout.Width(80));
                            //stageData.bgmID = (int)((SoundManager.eBGM)EditorGUILayout.EnumPopup((SoundManager.eBGM)stageData.bgmID));
                            stageData.bgmData = EditorGUILayout.ObjectField(stageData.bgmData, typeof(ChiptuneDataObject), false) as ChiptuneDataObject;
                        }
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("画面の幅", GUILayout.Width(80));
                            stageData.screenSizeX = (byte)Mathf.Clamp(EditorGUILayout.IntField(stageData.screenSizeX), 3, 128);
                        }
                        using (new GUILayout.HorizontalScope())
                        {
                            GUILayout.Label("画面の高さ", GUILayout.Width(80));
                            stageData.screenSizeY = (byte)Mathf.Clamp(EditorGUILayout.IntField(stageData.screenSizeY), 3, 128);
                        }
                    }
                }
                GUILayout.FlexibleSpace();
                using (new GUILayout.HorizontalScope())
                {
                    using (new GUILayout.HorizontalScope())
                    {
                        GUILayout.Label("表示倍率", GUILayout.Width(60));
                        zoom = EditorGUILayout.Slider(zoom, 0.5f, 1.0f, GUILayout.Width(120));
                        zoom = (int)(zoom * CHIP_SIZE) / (float)CHIP_SIZE;
                    }
                    GUILayout.FlexibleSpace();
                    //editMode = GUILayout.Toolbar(editMode, new string[] { "マップ", "タグ", "イベント" });
                }
            }

            // マップ
            using (var scrollView = new GUI.ScrollViewScope(
                new Rect(MAP_OFFSET_X, MAP_OFFSET_Y, position.width - MAP_OFFSET_X, position.height - MAP_OFFSET_Y), mapScrollPos,
                new Rect(0, 0, CHIP_DRAW_SIZE * stageData.sizeX, CHIP_DRAW_SIZE * stageData.sizeY)))
            {
                mapScrollPos = scrollView.scrollPosition;

                Rect maskRect = new Rect(mapScrollPos.x, mapScrollPos.y, position.width - MAP_OFFSET_X - 16, position.height - MAP_OFFSET_Y - 16);

                //GUI.color = Color.black;
                //GUI.DrawTexture(new Rect(0, 0, CHIP_DRAW_SIZE * stageData.sizeX, CHIP_DRAW_SIZE * stageData.sizeY), Texture2D.whiteTexture);
                GUI.color = Color.white;
                Rect mapRect = new Rect(0, 0, CHIP_DRAW_SIZE, CHIP_DRAW_SIZE);
                for (int x = 0; x < stageData.sizeX; x++)
                {
                    if (x * CHIP_DRAW_SIZE - mapScrollPos.x < -1 * CHIP_DRAW_SIZE ||
                        x * CHIP_DRAW_SIZE - mapScrollPos.x > (position.width - MAP_OFFSET_X)) continue;
                    for (int y = 0; y < stageData.sizeY; y++)
                    {
                        if (y * CHIP_DRAW_SIZE - mapScrollPos.y < -1 * CHIP_DRAW_SIZE ||
                            y * CHIP_DRAW_SIZE - mapScrollPos.y > (position.height - MAP_OFFSET_Y)) continue;

                        mapRect.x = 0 + x * CHIP_DRAW_SIZE;
                        mapRect.y = 0 + y * CHIP_DRAW_SIZE;

                        Utility.DrawTexture(new Rect(), new Rect(mapRect.x + 1, mapRect.y + 1, mapRect.width - 2, mapRect.height - 2), GUID_COLOR, Texture2D.whiteTexture, maskRect, 0);

                        int index = GetChip(x, y);
                        //if (index <= 0) continue;
                        if (index >= 0)
                        {
                            DrawMapChip(mapRect, stageData.tileData.tiles[index], NORMAL_COLOR, maskRect);
                        }
                        if (editMode == (int)eEditMode.TAG && stageData.tileData != null && stageData.tileData.tagData != null)
                        {
                            int tag = GetChipData(x, y).tag;
                            if (tag < stageData.tileData.tagData.tags.Count)
                            {
                                Utility.DrawTexture(new Rect(), mapRect, stageData.tileData.tagData.tags[tag].color, Texture2D.whiteTexture, maskRect, 0);
                                EditorGUI.LabelField(mapRect, stageData.tileData.tagData.tags[tag].label, stageData.tileData.tagData.tags[tag].style);
                            }
                        }
                    }
                }
                Handles.color = Color.gray;
                for (int x = stageData.screenSizeX; x < stageData.sizeX; x += stageData.screenSizeX)
                {
                    Handles.DrawLine(new Vector3(CHIP_DRAW_SIZE * x, 0), new Vector3(CHIP_DRAW_SIZE * x, CHIP_DRAW_SIZE * stageData.sizeY));
                }
                for (int y = stageData.screenSizeY; y < stageData.sizeY; y += stageData.screenSizeY)
                {
                    Handles.DrawLine(new Vector3(0, CHIP_DRAW_SIZE * y), new Vector3(CHIP_DRAW_SIZE * stageData.sizeX, CHIP_DRAW_SIZE * y));
                }
                if (editMode == (int)eEditMode.EVENT)
                {
                    int i = 0;
                    foreach (var ev in stageData.events)
                    {
						if (ev == null) continue;
                        mapRect.x = 0 + ev.x * CHIP_DRAW_SIZE;
                        mapRect.y = 0 + ev.y * CHIP_DRAW_SIZE;
                        Utility.DrawTexture(new Rect(), mapRect, EVENT_COLOR, Texture2D.whiteTexture, maskRect, 0);
                        if (ev.ev != null && ev.ev.tex != null && ev.ev.indexes.Count > 0)
						{
                            int index = ev.ev.indexes[(int)((timeCount * ev.ev.speed) % ev.ev.indexes.Count)];
                            DrawMapChip(mapRect, index, NORMAL_COLOR, ev.ev.tex, ev.ev.size.x,ev.ev.size.y, maskRect);
						}
                        if (i == selectEvent)
                        {
                            Handles.color = EVENT_SELECT_COLOR;
                            Handles.DrawLine(new Vector3(mapRect.xMin, mapRect.yMax), new Vector3(mapRect.xMax, mapRect.yMax));
                            Handles.DrawLine(new Vector3(mapRect.xMin, mapRect.yMin), new Vector3(mapRect.xMax, mapRect.yMin));
                            Handles.DrawLine(new Vector3(mapRect.xMin, mapRect.yMin), new Vector3(mapRect.xMin, mapRect.yMax));
                            Handles.DrawLine(new Vector3(mapRect.xMax, mapRect.yMin), new Vector3(mapRect.xMax, mapRect.yMax));
                        }
                        i++;
                    }
                }
            }

            GUI.color = Color.white;

            if (!Application.isPlaying)
            {
                OnEvent(Event.current);
            }
            else
            {
                GUI.color = new Color(0, 0, 0, 0.5f);
                GUI.DrawTexture(new Rect(0, 0, position.width, position.height), Texture2D.whiteTexture);
                GUI.color = Color.white;
            }
            EditorGUI.EndDisabledGroup();

            TimeCount();
            Repaint();
        }

        void DrawMapChip(Rect rect, TileDataObject.Tile tile, Color color, Rect maskRect = new Rect())
        {
            int n = 0;
            if (tile.isAnimate)
            {
                n = (int)((timeCount * tile.speed) % tile.ids.Count);
            }
            int i = Mathf.Max(tile.ids[n], 0);
            DrawMapChip(rect, i, color, mapTexture, CHIP_SIZE, CHIP_SIZE, maskRect);
        }

        void DrawMapChip(Rect rect, int index, Color color, Rect maskRect = new Rect())
        {
            DrawMapChip(rect, index, color, mapTexture, CHIP_SIZE, CHIP_SIZE, maskRect);
        }

        public void DrawMapChip(Rect rect, int index, Color color, Texture2D texture, int sizeX, int sizeY, Rect maskRect = new Rect())
        {
            //if (index < 0 || stageData == null || stageData.tileData == null ||
            //	stageData.tileData.tiles.Count >= index || stageData.tileData.tiles[index].ids.Count > 0) return;

            int hNum = texture.width / sizeX;
            Rect srcRect = new Rect(
                (float)(index % hNum * sizeX) / texture.width,
                (float)(texture.height - index / hNum * sizeY - sizeY) / texture.height,
                (float)sizeX / texture.width,
                (float)sizeY / texture.height);
            Utility.DrawTexture(srcRect, rect, color, texture, maskRect);
        }

        void TimeCount()
        {
            var t = DateTime.Now - preDateTime;
            timeCount += t.TotalSeconds;
            preDateTime = DateTime.Now;
        }

        void OnEvent(Event evt)
        {
            if (AssetDatabase.GetAssetPath(stageData).Length <= 0) return;

            var rect = new Rect(MAP_OFFSET_X, MAP_OFFSET_Y, CHIP_DRAW_SIZE * stageData.sizeX, CHIP_DRAW_SIZE * stageData.sizeY);
            if (rect.Contains(evt.mousePosition))
            {
                rect.width = CHIP_DRAW_SIZE;
                rect.height = CHIP_DRAW_SIZE;
                for (int x = 0; x < stageData.sizeX; x++)
                {
                    for (int y = 0; y < stageData.sizeY; y++)
                    {
                        rect.x = MAP_OFFSET_X + x * CHIP_DRAW_SIZE - mapScrollPos.x;
                        rect.y = MAP_OFFSET_Y + y * CHIP_DRAW_SIZE - mapScrollPos.y;
                        if (rect.Contains(evt.mousePosition))
                        {
                            switch (evt.type)
                            {
                                case EventType.MouseDown:
                                    Undo.RecordObject(stageData, "Stage Editor : Draw");
                                    if (evt.button == 0)
                                    {
                                        if (editMode == (int)eEditMode.MAP || editMode == (int)eEditMode.TAG)
                                        {
                                            PutChip(select, x, y);
                                            prePos = new Vector2(x, y);
                                        }
                                        else if (editMode == (int)eEditMode.EVENT)
                                        {
                                            var id = stageData.events.FindIndex(a => a != null && a.x == x && a.y == y);
                                            if (id >= 0)
                                            {
                                                selectEvent = id;
                                            }
                                            else
                                            {
                                                var move = new StageDataObject.Event();
                                                move.x = x;
                                                move.y = y;
                                                stageData.events.Add(move);
                                                selectEvent = stageData.events.Count - 1;
                                            }
                                        }
										GUI.FocusControl("");

									}
                                    else if (evt.button == 1)
                                    {
                                        if (editMode == (int)eEditMode.MAP)
                                        {
                                            select = GetChipID(x, y);
                                        }
                                        else if (editMode == (int)eEditMode.TAG)
                                        {
                                            selectTag = GetChipData(x, y).tag;
                                        }
                                        else if (editMode == (int)eEditMode.EVENT)
                                        {
                                            if (selectEvent < stageData.events.Count)
                                            {
                                                stageData.events[selectEvent].x = x;
                                                stageData.events[selectEvent].y = y;
                                            }
                                        }
										GUI.FocusControl("");
									}
                                    else if (evt.button == 2)
                                    {
                                        mapScrollTouchBegin = evt.mousePosition;
                                        mapScrollPosBegin = mapScrollPos;
                                    }
                                    break;
                                case EventType.MouseDrag:
                                    Undo.RecordObject(stageData, "Stage Editor : Draw");
                                    if (evt.button == 0)
                                    {
                                        if (editMode == (int)eEditMode.MAP || editMode == (int)eEditMode.TAG)
                                        {
                                            if ((x != prePos.x || y != prePos.y)
                                           && x >= 0 && y >= 0 && prePos.x >= 0 && prePos.y >= 0)
                                            {
                                                var pos = new Vector2(x, y);
                                                float distance = Vector2.Distance(prePos, pos);
                                                for (float i = 0; i < distance; i++)
                                                {
                                                    PutChip(select,
                                                        x + (int)((prePos.x - pos.x) * i / distance),
                                                        y + (int)((prePos.y - pos.y) * i / distance));
                                                }
                                                prePos = pos;
                                            }
                                        }
                                    }
                                    else if (evt.button == 2)
                                    {
                                        mapScrollPos = mapScrollPosBegin + (mapScrollTouchBegin - evt.mousePosition);
                                        if (mapScrollPos.x < 0) mapScrollPos.x = 0;
                                        if (mapScrollPos.y < 0) mapScrollPos.y = 0;
                                    }
                                    break;
                                case EventType.MouseUp:
                                case EventType.MouseLeaveWindow:
                                    if (evt.button == 2)
                                    {
                                        mapScrollPosBegin = mapScrollTouchBegin = Vector2.zero;
                                    }
                                    break;
                                case EventType.KeyDown:
                                    if (evt.keyCode == KeyCode.Space)
                                    {
                                        // 塗りつぶし
                                        Undo.RecordObject(stageData, "Stage Editor : Fill");
                                        FillChip(select, x, y, GetChip(x, y));
                                        Repaint();
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            if (evt.type == EventType.KeyUp)
            {
                if (evt.keyCode == KeyCode.S && evt.shift)
                {
                    Save();
                }
                Repaint();
            }
            if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
            {
                // D&D
                var dropArea = new Rect(0, 0, position.width, position.height);
                if (dropArea.Contains(evt.mousePosition))
                {
                    //マウスの形状
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (evt.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();
                        if (DragAndDrop.paths[0].Contains(".asset"))
                        {
                            var data = (StageDataObject)AssetDatabase.LoadAssetAtPath(DragAndDrop.paths[0], typeof(StageDataObject));
                            if (data != null)
                            {
                                data.fileName = Regex.Match(DragAndDrop.paths[0], @"\/([^/]*?)\.asset").Groups[1].Value;
                                stageData = data;
                            }
                        }
                        DragAndDrop.activeControlID = 0;
                    }
                    evt.Use();
                }
            }
        }

        void PutChip(int index, int x, int y, bool repaint = true)
        {
            if (x < 0 || y < 0 || x >= stageData.sizeX || y >= stageData.sizeY) return;
            //if (layer == 0 && index <= 0) return;
            if (editMode == (int)eEditMode.MAP)
            {
                if (repaint && stageData.blocks[x].y[y].i != index) Repaint();
                stageData.blocks[x].y[y].i = index;
                stageData.blocks[x].y[y].hash = stageData.tileData.tiles[index].hash;
                stageData.blocks[x].y[y].tag = stageData.tileData.tiles[index].tag;
            }
            else if (editMode == (int)eEditMode.TAG)
            {
                stageData.blocks[x].y[y].tag = selectTag;
            }
        }

        int GetChip(int x, int y)
        {
            if (x < 0 || y < 0 || x >= stageData.sizeX || y >= stageData.sizeY || stageData.tileData == null) return -1;

            int id = GetChipID(x, y);
            //return stageData.tileData.tiles[id].ids[0];
            return id;
        }

        int GetChipID(int x, int y)
        {
            var data = GetChipData(x, y);
            if (data != null)
            {
                if (data.i != select) Repaint();
                if (data.hash != stageData.tileData.tiles[data.i].hash)
                {
                    int id = GetChipIDFromHash(data.hash);
                    return id >= 0 ? id : data.i;
                }
                return data.i;
            }
            else
            {
                return -1;
            }
        }

        int GetChipIDFromHash(int hash)
        {
            for (var i = 0; i < stageData.tileData.tiles.Count; i++)
            {
                var tile = stageData.tileData.tiles[i];
                if (tile.hash == hash) return i;
            }
            return -1;
        }

        StageDataObject.Y GetChipData(int x, int y)
        {
            if (x < 0 || y < 0 || x >= stageData.sizeX || y >= stageData.sizeY) return null;

            while (stageData.blocks.Count <= x)
            {
                stageData.blocks.Add(new StageDataObject.X());
            }
            while (stageData.blocks[x].y.Count <= y)
            {
                stageData.blocks[x].y.Add(new StageDataObject.Y());
            }

            return stageData.blocks[x].y[y];
        }

        void FillChip(int index, int x, int y, int target)
        {
            if (x < 0 || y < 0 || x >= stageData.sizeX || y >= stageData.sizeY) return;

            int chip = GetChip(x, y);
            if (chip != target || target == index || index == chip) return;

            PutChip(index, x, y, false);

            FillChip(index, x - 1, y, target);
            FillChip(index, x + 1, y, target);
            FillChip(index, x, y - 1, target);
            FillChip(index, x, y + 1, target);
        }

        void EraseBlock(int index)
        {
            for (int x = 0; x < stageData.sizeX; x++)
            {
                for (int y = 0; y < stageData.sizeY; y++)
                {
                    for (int z = 0; z < LAYER_MAX; z++)
                    {
                        if (stageData.blocks[x].y[y].i == index)
                        {
                            stageData.blocks[x].y[y].i = 0;
                        }
                    }
                }
            }
        }


        void New()
        {
            stageData = StageDataObject.Create();
            string path;
            int i = 0;
            while (true)
            {
                path = string.Format(Application.dataPath + FILE_PATH + "/stage{0:000}.asset", i);
                if (!System.IO.File.Exists(path)) break;
                i++;
            }
            stageData.fileName = string.Format("stage{0:000}", i);
        }

        void Save()
        {
            var path = AssetDatabase.GetAssetPath(stageData);
            if (path.Length > 0)
            {
                EditorUtility.SetDirty(stageData);
            }
            else
            {
                //path = "Assets" + FILE_PATH + "/" + stageData.fileName + ".asset";
                path = EditorUtility.SaveFilePanelInProject("ステージデータ保存", stageData.fileName, "asset", "message");
                if (path.Length > 0)
				{
                    Utility.CreateFolder(path);
                    AssetDatabase.CreateAsset(stageData, path);
                }
            }
            foreach(var ev in stageData.events)
			{
                if (ev.ev != null)
				{
                    EditorUtility.SetDirty(ev.ev);
                }
			}
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        void Load()
        {
            var path = EditorUtility.OpenFilePanel("ステージデータ読込", Application.dataPath + FILE_PATH, "asset");
            if (path.Length > 0)
            {
                path = "Assets" + path.Replace(Application.dataPath, "");
                var data = (StageDataObject)AssetDatabase.LoadAssetAtPath(path, typeof(StageDataObject));
                if (data != null)
                {
                    data.fileName = Regex.Match(path, @"\/([^/]*?)\.asset").Groups[1].Value;
                    stageData = data;
                }
                AssetDatabase.Refresh();
            }
        }
    }
}