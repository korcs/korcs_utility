﻿using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEditorInternal;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;


namespace CommonPackage
{
	public class EventEditorWindow : EditorWindow
	{
		const string FILE_PATH = "Database/Event";
		const int MARGIN = 6;

		const int CONTENT_HEIGHT = 50;
		const int CONTENT_WIDTH = 140;

		[SerializeField]
		string fileName = "";
		[SerializeField]
		EventDataObject data;
		[SerializeField]
		Vector2 scrollPos;
		
		EventEditorCore core = null;

		[MenuItem("Tools/korcs/Event Editor", priority = 40)]
		static void Open()
		{
			GetWindow<EventEditorWindow>("Event Editor");
		}

		void OnEnable()
		{
			core = CreateInstance<EventEditorCore>();
			core.data = data;
			core.InitReorderableList();

			Repaint();
		}

		void OnDisable()
		{
			Repaint();
		}

		void OnGUI()
		{
			EditorGUI.BeginDisabledGroup(Application.isPlaying);

			// 設定
			using (new GUILayout.AreaScope(new Rect(MARGIN, MARGIN, position.width - (MARGIN * 2), 80)))
			{
				using (new GUILayout.HorizontalScope())
				{
					using (new GUILayout.VerticalScope(GUILayout.Width(80)))
					{
						if (GUILayout.Button("新規作成"))
						{
							New();
						}
						if (GUILayout.Button("読込"))
						{
							Load();
						}
						if (GUILayout.Button("保存"))
						{
							Save();
						}
					}
					GUILayout.FlexibleSpace();
					if (data != null)
					{
						using (new GUILayout.VerticalScope(GUILayout.Width(200)))
						{
							using (new GUILayout.HorizontalScope())
							{
								EditorGUI.BeginDisabledGroup(AssetDatabase.GetAssetPath(data).Length > 0);
								GUILayout.Label("ファイル名");
								fileName = EditorGUILayout.TextField(fileName);
								GUILayout.Label(".asset");
								EditorGUI.EndDisabledGroup();
							}
							if (GUILayout.Button("データ追加"))
							{
								Undo.RecordObject(data, "Edit Database");
								data.events.Add(null);
							}
						}
					}
				}
			}

			// 設定
			var rect = new Rect(0, 80 + MARGIN * 2, position.width, position.height - (80 + MARGIN * 2));
			using (new GUILayout.AreaScope(rect))
			{
				if (data == null)
				{
					GUILayout.Label("データベースを読み込むか新規作成してください");
				}
				else
				{
					core.ShowItems(CONTENT_WIDTH);
				}
			}

			if (!Application.isPlaying)
			{
				OnEvent(Event.current);
			}
			else
			{
				GUI.color = new Color(0, 0, 0, 0.5f);
				GUI.DrawTexture(new Rect(0, 0, position.width, position.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
			}

			EditorGUI.EndDisabledGroup();

			Repaint();
		}
		
		void OnEvent(Event evt)
		{
			if (evt.type == EventType.MouseDown)
			{
				GUI.FocusControl("");
				Repaint();
			}
			if (evt.type == EventType.KeyUp)
			{
				if (evt.keyCode == KeyCode.S && evt.shift)
				{
					Save();
				}
				Repaint();
			}
			if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
			{
				// D&D
				var dropArea = new Rect(0, 0, position.width, position.height);
				if (dropArea.Contains(evt.mousePosition))
				{
					//マウスの形状
					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

					if (evt.type == EventType.DragPerform)
					{
						DragAndDrop.AcceptDrag();
						var load = (EventDataObject)AssetDatabase.LoadAssetAtPath(DragAndDrop.paths[0], typeof(EventDataObject));
						if (load != null)
						{
							data = load;
							var tmp = DragAndDrop.paths[0].Split('/');
							fileName = tmp[tmp.Length - 1].Replace(".asset", "");
						}
						DragAndDrop.activeControlID = 0;
					}
					evt.Use();
				}
			}
		}

		void New()
		{
			data = EventDataObject.Create();
			fileName = "new";
			core.data = data;
			core.InitReorderableList();
		}

		void Save()
		{
			var path = AssetDatabase.GetAssetPath(data);
			if (path.Length > 0)
			{
				EditorUtility.SetDirty(data);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			else
			{
				path = EditorUtility.SaveFilePanelInProject("データ保存", data.fileName, "asset", "message");
				if (path.Length > 0)
				{
					Utility.CreateFolder(path);
					AssetDatabase.CreateAsset(data, path);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}
			}
		}

		void Load()
		{
			var path = EditorUtility.OpenFilePanel("データ読込", Application.dataPath + FILE_PATH, "asset");
			if (System.IO.File.Exists(path))
			{
				path = "Assets" + path.Replace(Application.dataPath, "");
				var load = (EventDataObject)AssetDatabase.LoadAssetAtPath(path, typeof(EventDataObject));
				if (load != null)
				{
					data = load;
					fileName = path.Replace("Assets/" + FILE_PATH + "/", "").Replace(".asset", "");
					core.data = data;
					core.InitReorderableList();
				}
				AssetDatabase.Refresh();
			}
			else
			{
				New();
			}
		}
	}

	[Serializable]
	public class EventEditorCore : ScriptableObject
	{
		static Color NORMAL_COLOR = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		static Color BG_COLOR = new Color32(165, 165, 165, 255);
		static Color BG_COLOR_PRO = new Color32(88, 88, 88, 255);
		static Color SELECT_COLOR = new Color32(58, 114, 176, 255);

		const int CHIP_SIZE = 32;

		ReorderableList reorderableList = null;
		public EventDataObject data;
		public Rect popupRect;
		[SerializeField]
		Vector2 scrollPos1;
		Vector2 scrollPos2;

		DateTime preDateTime = DateTime.Now;
		double timeCount = 0;

		GUIStyle style = new GUIStyle();

		public void OnEnable()
		{
			InitReorderableList();

			popupRect.x = 160;
			popupRect.y = 0;
		}

		public void SetData(EventDataObject data)
		{
			if (this.data != data)
			{
				this.data = data;
				InitReorderableList();
			}
		}

		public void ShowItems(int width)
		{
			GUILayout.BeginVertical(GUILayout.MaxWidth(width));
			if (data != null)
			{
				GUILayout.BeginVertical(GUILayout.Height(140));
				float w = data.size.y > 0 ? Mathf.Clamp(CHIP_SIZE * data.size.x / data.size.y, 1, width - 16) : 0;
				var rect = EditorGUILayout.GetControlRect(GUILayout.Width(w), GUILayout.Height(CHIP_SIZE));
				if (data.indexes.Count > 0)
					DrawChip(rect, data.indexes[(int)((timeCount * data.speed) % data.indexes.Count)]);
				using (new GUILayout.HorizontalScope())
				{
					EditorGUI.BeginChangeCheck();
					data.tex = EditorGUILayout.ObjectField(data.tex, typeof(Texture2D), false) as Texture2D;
					if (EditorGUI.EndChangeCheck())
					{
						data.size.x = data.size.y = 16;
					}
				}
				using (new GUILayout.HorizontalScope())
				{
					data.size = EditorGUILayout.Vector2IntField("Size", data.size, GUILayout.Width(width - 16));
					data.size.x = Mathf.Clamp(data.size.x, 1, data.width);
					data.size.y = Mathf.Clamp(data.size.y, 1, data.height);
				}
				int n = data.indexes.Count;
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("Index");
					EditorGUI.BeginChangeCheck();
					n = Mathf.Clamp(EditorGUILayout.IntField(n), 0, 16);
					if (EditorGUI.EndChangeCheck())
					{
						while(n > data.indexes.Count)
						{
							data.indexes.Add(0);
						}
						while (n < data.indexes.Count)
						{
							data.indexes.RemoveAt(data.indexes.Count - 1);
						}
					}
				}
				using (var scrollView = new GUILayout.ScrollViewScope(scrollPos2, GUILayout.MaxWidth(width), GUILayout.Height(48)))
				{
					scrollPos2 = scrollView.scrollPosition;
					using (new GUILayout.HorizontalScope(GUI.skin.box))
					{
						for (int i = 0; i < data.indexes.Count; i++)
						{
							data.indexes[i] = EditorGUILayout.IntField(data.indexes[i], GUILayout.Width(32));
						}
					}
				}
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("Speed");
					data.speed = EditorGUILayout.FloatField(data.speed);
				}
				GUILayout.EndVertical();
			}

			EditorGUILayout.Space();

			if (reorderableList != null)
			{
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					EditorGUI.BeginDisabledGroup(!CanAdd(reorderableList));
					if (GUILayout.Button("+"))
					{
						Add(reorderableList);
					}
					EditorGUI.BeginDisabledGroup(!CanRemove(reorderableList));
					if (GUILayout.Button("-"))
					{
						Remove(reorderableList);
					}
					EditorGUI.EndDisabledGroup();
				}
				using (var scrollView = new GUILayout.ScrollViewScope(scrollPos1))
				{
					scrollPos1 = scrollView.scrollPosition;
					reorderableList.DoLayoutList();
				}
			}
			GUILayout.EndVertical();

			TimeCount();
		}

		public void DrawChip(Rect rect, int index)
		{
			if (data == null || data.tex == null) return;
			Rect srcRect = new Rect(
				(float)(index % data.hNum * data.size.x) / data.width,
				(float)(data.height - index / data.hNum * data.size.y - data.size.y) / data.height,
				(float)data.size.x / data.width,
				(float)data.size.y / data.height);
			Utility.DrawTexture(srcRect, rect, NORMAL_COLOR, data.tex);
		}

		void TimeCount()
		{
			var t = DateTime.Now - preDateTime;
			timeCount += t.TotalSeconds;
			preDateTime = DateTime.Now;
		}

		#region ReorderableList
		public void InitReorderableList()
		{
			if (data != null)
			{
				reorderableList = new ReorderableList(data.events, typeof(EventDataObject.IEvent), true, false, false, false);

				reorderableList.onAddCallback += Add;
				reorderableList.onRemoveCallback += Remove;
				reorderableList.onCanAddCallback += CanAdd;
				reorderableList.onCanRemoveCallback += CanRemove;
				reorderableList.onMouseUpCallback += OnMouseUp;
				reorderableList.drawElementCallback += DrawElement;
				reorderableList.drawElementBackgroundCallback += DrawElementBackground;
				reorderableList.drawHeaderCallback += DrawHeader;
				reorderableList.headerHeight = 0;
				reorderableList.footerHeight = 0;
			}
		}

		void Add(ReorderableList list)
		{
			if (reorderableList.index >= 0)
				data.events.Insert(reorderableList.index, null);
			else
				data.events.Add(null);
		}
		void Remove(ReorderableList list)
		{
			data.events.RemoveAt(reorderableList.index);
		}
		bool CanAdd(ReorderableList list)
		{
			return data != null;
		}
		bool CanRemove(ReorderableList list)
		{
			return data != null && data.events.Count > reorderableList.index && reorderableList.index >= 0;
		}
		void OnMouseUp(ReorderableList list)
		{
			if (data != null && reorderableList.index < data.events.Count)
			{
				var window = new EventEditorPopupWindow();
				window.data = data;
				window.index = reorderableList.index;
				
				PopupWindow.Show(popupRect, window);
			}
		}
		void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			if (isActive || isFocused)
			{
				style.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.white;
			}
			else
			{
				style.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
			}
			style.alignment = TextAnchor.MiddleLeft;

			if (data != null && index < data.events.Count && data.events[index] != null)
			{
				GUI.Label(rect, string.Format("{0} [{1}]", data.events[index].name, data.events[index].content), style);
			}
			else
			{
				GUI.Label(rect, "None", style);
			}
		}
		void DrawElementBackground(Rect rect, int index, bool isActive, bool isFocused)
		{
			if (isActive || isFocused)
			{
				EditorGUI.DrawRect(rect, EditorGUIUtility.isProSkin ? SELECT_COLOR : SELECT_COLOR);
			}
			else if (index % 2 == 1)
			{
				EditorGUI.DrawRect(rect, EditorGUIUtility.isProSkin ? BG_COLOR_PRO : BG_COLOR);
			}
		}
		void DrawHeader(Rect rect)
		{
			EditorGUI.LabelField(rect, "イベント");
		}
		#endregion
	}


	public class EventEditorPopupWindow : PopupWindowContent
	{
		public EventDataObject data = null;
		public int index = 0;

		public static EventDataObject.IEvent copy = null;
		
		int typeIndex = 0;
		TypeSet[] types = null;
		
		Vector2 windowSize = new Vector2(400, 10);
		AnimFloat anim = new AnimFloat(10);

		class TypeSet
		{
			public Type type = null;
			public string name = string.Empty;
			public EventDataObject.IEvent instance = null;
			public TypeSet(Type type, string name, EventDataObject.IEvent instance)
			{
				this.type = type;
				this.name = name;
				this.instance = instance;
			}
		}

		public override Vector2 GetWindowSize()
		{
			return windowSize;
		}

		public override void OnGUI(Rect rect)
		{
			int i = index;

			if (Event.current.type == EventType.Layout)
			{
				for (int j = 0; j < types.Length; j++)
				{
					if (data.events[i] != null && types[j].type == data.events[i].GetType())
					{
						typeIndex = j;
						break;
					}
				}
			}
			EditorGUILayout.BeginFadeGroup(anim.value / anim.target);
			EditorGUI.BeginChangeCheck();
#if false
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("Type", GUILayout.Width(60));
				typeIndex = EditorGUILayout.Popup(typeIndex, typeNames);
			}
#else
			EditorGUILayout.BeginHorizontal();
			for (int j = 0; j < types.Length; j++)
			{
				if (GUILayout.Toggle(j == typeIndex, types[j].name, EditorStyles.toolbarButton))
				{
					typeIndex = j;
				}
				if(j > 0 && (j + 1) % 4 == 0)
				{
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.BeginHorizontal();
				}
			}
			EditorGUILayout.EndHorizontal();
#endif
			if (EditorGUI.EndChangeCheck())
			{
				if (types[typeIndex] != null)
					data.events[i] = Activator.CreateInstance(types[typeIndex].type) as EventDataObject.IEvent;
				else
					data.events[i] = null;
			}
			EditorGUILayout.Space();
			if (data.events[i] != null)
			{
				using (new EditorGUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("コピー"))
					{
						copy = data.events[i];
					}
				}
				EditorGUILayout.Space();
				using (new EditorGUILayout.HorizontalScope())
				{
					EditorGUILayout.Space();
					using (new EditorGUILayout.VerticalScope())
					{
						data.events[i].Edit();
					}
					EditorGUILayout.Space();
				}
			}
			else
			{
				using (new EditorGUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					EditorGUI.BeginDisabledGroup(copy == null);
					if (GUILayout.Button("ペースト"))
					{
						using (MemoryStream stream = new MemoryStream())
						{
							BinaryFormatter f = new BinaryFormatter();
							f.Serialize(stream, copy);
							stream.Position = 0L;
							data.events[i] = (EventDataObject.IEvent)f.Deserialize(stream);
						}
					}
					EditorGUI.EndDisabledGroup();
				}
			}
			EditorGUILayout.Space();
			if (Event.current.type == EventType.Layout)
			{
				EditorGUILayout.GetControlRect(GUILayout.Height(0));
			}
			else if (Event.current.type == EventType.Repaint)
			{
				float y = EditorGUILayout.GetControlRect(GUILayout.Height(0)).yMax;
				if (y != anim.target)
				{
					anim.target = y;
				}
				windowSize.y = Mathf.Abs(anim.value);
				if (anim.isAnimating)
				{
					//Debug.Log(anim.value);
					GUI.color = Color.black;
					GUI.DrawTexture(new Rect(0, 0, windowSize.x, windowSize.y), Texture2D.whiteTexture);
					GUI.color = Color.white;
				}
			}
			EditorGUILayout.EndFadeGroup();
		}

		public override void OnOpen()
		{
			base.OnOpen();
			
			var typeList = Utility.GetInheritedTypes<EventDataObject.IEvent>();
			//typeList.Insert(0, null);
			//types = typeList.ToArray();
			List<TypeSet> tmp = new List<TypeSet>();
			foreach (var t in typeList)
			{
				if (t != null)
				{
					var inst = Activator.CreateInstance(t);
					var eventName = (string)t.GetProperty("name").GetValue(inst);
					tmp.Add(new TypeSet(t, eventName, (EventDataObject.IEvent)inst));
				}
				else
				{
					//tmp.Add(new TypeSet(null, "None", null));
				}
			}
			tmp.Sort((a, b) => (a.instance != null ? a.instance.priority : -9999) - (b.instance != null ? b.instance.priority : -9999));
			types = tmp.ToArray();

			anim.speed = 100;
			//anim.valueChanged.AddListener(HandleUtility.Repaint);
		}
	}
}