﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CommonPackage.Chiptune
{
	public class ChiptuneEditorWindow : EditorWindow
	{
		// TODO:　コード強調表示

		const string FILE_PATH = "/Database/Chiptune";
		const int MARGIN = 6;
		const int MAP_OFFSET_X = 0;
		const int MAP_OFFSET_Y = 132;
		const int NOTE_WIDTH = 16;
		const int NOTE_HEIGHT = 16;
		const int SCALE_MAX = 88;
		static Color BG_COLOR_1 = new Color(0.0f, 0.0f, 0.1f, 1);
		static Color BG_COLOR_2 = new Color(0.1f, 0.1f, 0.2f, 1);
		static Color BG_COLOR_3 = new Color(0.05f, 0.2f, 0.2f, 1);
		static Color NORMAL_COLOR = new Color(0.95f, 1.0f, 0.5f, 1);
		static Color SELECTED_COLOR = new Color(1.0f, 0.5f, 0.9f, 1);
		static Color[] UNDER_COLOR = new Color[] { new Color(0.4f, 0.2f, 0.2f, 0.5f), new Color(0.4f, 0.4f, 0.2f, 0.5f), new Color(0.2f, 0.4f, 0.2f, 0.5f), new Color(0.5f, 0.2f, 0.5f, 0.5f) };
		static Color ENVELOPE_COLOR = new Color(0.5f, 0.0f, 0.0f, 0.5f);
		static Color GUID_COLOR_1 = new Color(0.2f, 0.2f, 0.3f, 0.8f);
		static Color GUID_COLOR_2 = new Color(0.5f, 0.5f, 0.6f, 0.8f);
		static Color GUID_COLOR_3 = new Color(0.4f, 0.4f, 0.5f, 0.8f);
		static Color PLAYING_COLOR = new Color(1.0f, 0.2f, 0.2f, 1);
		static Color LOOP_COLOR = new Color(0.2f, 1.0f, 0.9f, 1);


		int select = -1;
		Vector2 scrollPos;
		Vector2 mapScrollPos;
		Vector2 mapScrollPosBegin;
		Vector2 mapScrollTouchBegin;
		bool onionSkin = true;

		int channel = 0;
		int noteLength = 0;
		Vector2Int currentEnvelope = new Vector2Int(128, 128);

		[SerializeField]
		ChiptuneDataObject chiptuneData;

		bool isPlaying = false;

		int selectedScale1 = 3;
		int selectedScale2 = 0;

		int continuePoint = 0;

		//string[] musical_scale_name = new string[12]
		//{
		//	"ラ", "ラ＃", "シ", "ド", "ド＃", "レ", "レ＃", "ミ", "ファ", "ファ＃", "ソ", "ソ＃",
		//};

		string[] musical_scale_name = new string[12]
		{
		"A", "A＃", "B", "C", "C＃", "D", "D＃", "E", "F", "F＃", "G", "G＃",
		};

		bool[] half_tone_flag = new bool[12]
		{
		false,true,false,false,true,false,true,false,false,true,false,true
		};

		bool[] major_scale_flag = new bool[12]
		{
		true,false,true,false,true,true,false,true,false,true,false,true,
		};

		bool[] minor_scale_flag = new bool[12]
		{
		true,false,true,true,false,true,false,true,true,false,true,false,
		};

		string[] scale_type_name = new string[2]
		{
		"メジャー", "マイナー"
		};

		string[] tone_color_name = new string[]
		{
		"なし", "矩形波12.5%", "矩形波25%", "矩形波50%", "三角波", "疑似三角波", "正弦波", "ノコギリ波", "長周期ノイズ", "短周期ノイズ", "波形メモリ"
		};

		string[][] tone_color_names = new string[][]
		{
			new string[]{ "なし", "矩形波12.5%", "矩形波25%", "矩形波50%" },
			new string[]{ "なし", "矩形波12.5%", "矩形波25%", "矩形波50%" },
			new string[]{ "なし", "波形メモリ", "疑似三角波", "正弦波", "ノコギリ波" },
			new string[]{ "なし", "長周期ノイズ", "短周期ノイズ" }
		};
		int[][] tone_color_values = new int[][]
		{
			new int[]{ 0, 1, 2, 3 },
			new int[]{ 0, 1, 2, 3 },
			new int[]{ 0, 10, 5, 6, 7 },
			new int[]{ 0, 8, 9 }
		};

		//string[] note_name = new string[8]
		//{
		//	"全音符", "付点2分音符", "2分音符", "付点4分音符", "4分音符", "付点8分音符", "8分音符", "16分音符"
		//};
		string[] note_name = new string[8]
		{
		"16", "12", "8", "6", "4", "3", "2", "1"
		};

		int[] note_length = new int[8]
		{
		16, 12, 8, 6, 4, 3, 2, 1
		};

		string[] channel_name = new string[4] { "CH1", "CH2", "CH3", "CH4" };

		GUIStyle labelStyle = new GUIStyle();

		ChiptunePlayer _player = null;
		ChiptunePlayer player
		{
			get
			{
				if (_player == null)
				{
					_player = FindObjectOfType<ChiptunePlayer>();
				}
				return _player;
			}
		}

		ChiptuneEditorPlayer _editorPlayer = null;
		ChiptuneEditorPlayer editorPlayer
		{
			get
			{
				if (_editorPlayer == null)
				{
					_editorPlayer = new ChiptuneEditorPlayer();
				}
				return _editorPlayer;
			}
		}

		[MenuItem("Tools/korcs/Chiptune Editor", priority = 120)]
		static void Open()
		{
			GetWindow<ChiptuneEditorWindow>("Chiptune Editor");
		}

		void OnEnable()
		{
			//titleContent.text = "Chiptune Editor";
			//Texture2D tex = new Texture2D(16, 16, TextureFormat.ARGB32, false);
			//tex.LoadImage(new byte[] { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 16, 0, 0, 0, 16, 8, 6, 0, 0, 0, 31, 243, 255, 97, 0, 0, 0, 92, 73, 68, 65, 84, 56, 17, 189, 145, 65, 10, 0, 32, 8, 4, 43, 122, 140, 255, 255, 96, 145, 68, 12, 65, 116, 88, 171, 211, 182, 168, 59, 104, 54, 179, 164, 188, 162, 52, 143, 94, 121, 64, 157, 4, 13, 36, 25, 154, 62, 108, 151, 94, 23, 70, 112, 74, 165, 191, 19, 248, 63, 140, 128, 211, 175, 169, 44, 126, 66, 192, 128, 127, 87, 96, 18, 119, 64, 77, 178, 165, 195, 118, 112, 77, 90, 145, 155, 144, 9, 228, 1, 29, 88, 78, 5, 168, 29, 245, 0, 111, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130 });
			//tex.filterMode = FilterMode.Point;
			//titleContent.image = tex;
			Repaint();
		}

		void OnDisable()
		{
			Repaint();
		}

		void OnGUI()
		{
			if (chiptuneData == null)
			{
				New();
				//return;
			}


			// 設定
			using (new GUILayout.AreaScope(new Rect(MARGIN, MARGIN, position.width - (MARGIN * 2), 120)))
			{
				using (new GUILayout.HorizontalScope())
				{
					using (new GUILayout.VerticalScope(GUILayout.Width(80)))
					{
						if (GUILayout.Button("新規作成"))
						{
							New();
						}
						if (GUILayout.Button("読込"))
						{
							Load();
						}
						if (GUILayout.Button("保存"))
						{
							Save();
						}
					}
					using (new GUILayout.VerticalScope(GUILayout.Width(200)))
					{
						using (new GUILayout.HorizontalScope())
						{
							EditorGUI.BeginDisabledGroup(AssetDatabase.GetAssetPath(chiptuneData).Length > 0);
							GUILayout.Label("ファイル名", GUILayout.Width(90));
							chiptuneData.fileName = GUILayout.TextField(chiptuneData.fileName, 13);
							EditorGUI.EndDisabledGroup();
						}
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label("小節数", GUILayout.Width(90));
							chiptuneData.length = EditorGUILayout.IntField(chiptuneData.length / 16) * 16;
						}
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label("BPM", GUILayout.Width(90));
							chiptuneData.bpm = EditorGUILayout.IntField(chiptuneData.bpm);
						}
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label("ループポイント", GUILayout.Width(90));
							chiptuneData.loopPoint = EditorGUILayout.IntSlider(chiptuneData.loopPoint / 16, 0, chiptuneData.length / 16) * 16;
						}
					}
					using (new GUILayout.VerticalScope(GUILayout.Width(300)))
					{
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label("途中再生", GUILayout.Width(60));
							continuePoint = EditorGUILayout.IntSlider(continuePoint / 16, 0, chiptuneData.length / 16) * 16;
						}
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label("波形メモリ", GUILayout.Width(64));
							for (int i = 0; i < 16; i++)
							{
								if (chiptuneData.waveformMemorys.Count <= i)
									chiptuneData.waveformMemorys.Add(128);
								chiptuneData.waveformMemorys[i] = (byte)GUILayout.VerticalSlider(chiptuneData.waveformMemorys[i], 255, 0, GUILayout.Width(10), GUILayout.Height(64));
							}
							GUILayout.BeginVertical();
							GUILayout.BeginHorizontal();
							for (int i = 0; i < 16; i++)
							{
								if (i % 4 == 0 && i > 0)
								{
									GUILayout.EndHorizontal();
									GUILayout.BeginHorizontal();
								}
								chiptuneData.waveformMemorys[i] = (byte)EditorGUILayout.IntField(chiptuneData.waveformMemorys[i], GUILayout.Width(32));
							}
							GUILayout.EndHorizontal();
							GUILayout.EndVertical();
						}
					}
					GUILayout.FlexibleSpace();
					using (new GUILayout.VerticalScope(GUILayout.Width(200)))
					{
						noteLength = GUILayout.Toolbar(noteLength, note_name);
						GUILayout.Label("エンベロープ");
						if (select >= 0)
						{
							using (new GUILayout.HorizontalScope())
							{
								GUILayout.Label("開始音量", GUILayout.Width(80));
								chiptuneData.channels[channel].noteDatas[select].beginGain =
									(byte)EditorGUILayout.IntSlider(chiptuneData.channels[channel].noteDatas[select].beginGain, 0, 128);
							}
							using (new GUILayout.HorizontalScope())
							{
								GUILayout.Label("終了音量", GUILayout.Width(80));
								chiptuneData.channels[channel].noteDatas[select].endGain =
									(byte)EditorGUILayout.IntSlider(chiptuneData.channels[channel].noteDatas[select].endGain, 0, 128);
							}
							currentEnvelope.x = chiptuneData.channels[channel].noteDatas[select].beginGain;
							currentEnvelope.y = chiptuneData.channels[channel].noteDatas[select].endGain;
							labelStyle.normal.textColor = Color.gray;
							GUILayout.Label("A:￣ S:／ D:＼ F:―", labelStyle);
						}
					}

				}
				GUILayout.FlexibleSpace();
				using (new GUILayout.HorizontalScope(GUILayout.Height(24)))
				{
					int tmp = channel;
					channel = GUILayout.Toolbar(channel, channel_name);
					if (channel != tmp)
					{
						select = -1;
					}
					//chiptuneData.channels[channel].state = (ChiptuneChannel.PlayState)EditorGUILayout.EnumPopup(chiptuneData.channels[channel].state, GUILayout.Width(120));
					chiptuneData.channels[channel].state = 
						(ChiptuneWave.PlayState)EditorGUILayout.IntPopup((int)chiptuneData.channels[channel].state, tone_color_names[channel], tone_color_values[channel], GUILayout.Width(120));
					onionSkin = GUILayout.Toggle(onionSkin, "オニオンスキン", GUILayout.Width(100));
					
					// キー
					GUILayout.Label("キー");
					selectedScale1 = EditorGUILayout.Popup(selectedScale1, musical_scale_name, GUILayout.Width(40));
					selectedScale2 = EditorGUILayout.Popup(selectedScale2, scale_type_name, GUILayout.Width(60));

					if (GUILayout.Button("オクターブ↑"))
					{
						Shift(channel, 12);
					}
					if (GUILayout.Button("オクターブ↓"))
					{
						Shift(channel, -12);
					}

					GUILayout.FlexibleSpace();

					EditorGUI.BeginDisabledGroup(!Application.isPlaying);
					if (!Application.isPlaying)
					{
						isPlaying = false;
					}
					GUI.color = isPlaying ? Color.gray : Color.white;
					if (GUILayout.Button(isPlaying ? "停止" : "再生", GUILayout.Width(80), GUILayout.Height(20)))
					{
						isPlaying = TestPlay(isPlaying);
					}
					GUI.color = Color.white;
					EditorGUI.EndDisabledGroup();
				}
			}

			// ノート
			using (var scrollView = new GUI.ScrollViewScope(
				new Rect(MAP_OFFSET_X, MAP_OFFSET_Y, position.width - MAP_OFFSET_X, position.height - MAP_OFFSET_Y), mapScrollPos,
				new Rect(0, 0, NOTE_WIDTH * chiptuneData.length + 1, NOTE_HEIGHT * SCALE_MAX + 1)))
			{
				mapScrollPos = scrollView.scrollPosition;
				//scrollView.handleScrollWheel = false;

				//Rect maskRect = new Rect(mapScrollPos.x, mapScrollPos.y, position.width - MAP_OFFSET_X - 16, position.height - MAP_OFFSET_Y - 16);

				Handles.color = Color.white;
				labelStyle.normal.textColor = NORMAL_COLOR;
				for (int y = 0; y < SCALE_MAX; y++)
				{
					Handles.DrawLine(new Vector3(0, NOTE_HEIGHT * y), new Vector3(NOTE_WIDTH * chiptuneData.length, NOTE_HEIGHT * y));
					int id = (SCALE_MAX - y);
					var color = (half_tone_flag[(id - 1) % musical_scale_name.Length]) ? BG_COLOR_1 : BG_COLOR_2;
					//if (selectedScale2 == 0 && (major_scale_flag[(y + 1 + selectedScale1) % musical_scale_name.Length]))
					//{
					//	color = BG_COLOR_3;
					//}
					//else if(selectedScale2 == 1 && (minor_scale_flag[(y + 1 + selectedScale1) % musical_scale_name.Length]))
					//{
					//	color = BG_COLOR_3;
					//}
					Handles.DrawSolidRectangleWithOutline(new Rect(0, NOTE_HEIGHT * y, NOTE_WIDTH * chiptuneData.length, NOTE_HEIGHT), color, GUID_COLOR_3);
					Handles.Label(new Vector3(mapScrollPos.x, NOTE_HEIGHT * y), id + musical_scale_name[(id - 1) % musical_scale_name.Length], labelStyle);
				}
				for (int x = 1; x < chiptuneData.length; x++)
				{
					Handles.color = (x == chiptuneData.loopPoint) ? LOOP_COLOR : (x % 16 == 0) ? GUID_COLOR_2 : GUID_COLOR_1;
					Handles.DrawLine(new Vector3(NOTE_WIDTH * x, 0), new Vector3(NOTE_WIDTH * x, NOTE_HEIGHT * SCALE_MAX));

					if (x % 16 == 0)
					{
						Handles.Label(new Vector3(NOTE_WIDTH * x, mapScrollPos.y), (x / 16 + 1).ToString(), labelStyle);
					}
				}
				if (isPlaying)
				{
					float x = GetPlayerTime();
					Handles.color = PLAYING_COLOR;
					Handles.DrawLine(new Vector3(NOTE_WIDTH * x, 0), new Vector3(NOTE_WIDTH * x, NOTE_HEIGHT * SCALE_MAX));
					Repaint();
				}
				else
				{
					float x = continuePoint;
					Handles.color = PLAYING_COLOR;
					Handles.DrawLine(new Vector3(NOTE_WIDTH * x, 0), new Vector3(NOTE_WIDTH * x, NOTE_HEIGHT * SCALE_MAX));
				}

				Handles.color = Color.white;
				for (int ch = 0; ch < 4; ch++)
				{
					while (chiptuneData.channels[ch].noteDatas.Count < chiptuneData.length)
					{
						chiptuneData.channels[ch].noteDatas.Add(null);
					}
					if (ch != channel && onionSkin)
					{
						DrawNotes(ch);
					}
				}
				DrawNotes(channel);
			}

			GUI.color = Color.white;

			OnEvent(Event.current);

			editorPlayer.Update();
		}

		void DrawNotes(int ch)
		{
			float y1 = 0, y2 = 0;
			var faceColor = (ch == channel) ? NORMAL_COLOR : UNDER_COLOR[ch];
			var outlineColor = UNDER_COLOR[ch];
			for (int i = 0; i < chiptuneData.length; i++)
			{
				if (chiptuneData.channels[ch].noteDatas[i] != null && chiptuneData.channels[ch].noteDatas[i].length > 0)
				{
					Handles.DrawSolidRectangleWithOutline(
						new Rect(NOTE_WIDTH * i, NOTE_HEIGHT * (SCALE_MAX - chiptuneData.channels[ch].noteDatas[i].musicalScale),
						NOTE_WIDTH * chiptuneData.channels[ch].noteDatas[i].length - 1, NOTE_HEIGHT - 1),
						(ch == channel && i == select) ? SELECTED_COLOR : faceColor, outlineColor);
					if (ch == channel)
					{
						y1 = (NOTE_HEIGHT - 1) * (1 - chiptuneData.channels[ch].noteDatas[i].beginGain / 128.0f);
						y2 = (NOTE_HEIGHT - 1) * (1 - chiptuneData.channels[ch].noteDatas[i].endGain / 128.0f);
						Handles.DrawSolidRectangleWithOutline(
							new Vector3[]
							{
							new Vector3(
								NOTE_WIDTH * i,
								NOTE_HEIGHT * (SCALE_MAX - chiptuneData.channels[ch].noteDatas[i].musicalScale) + y1),
							new Vector3(
								NOTE_WIDTH * (i + chiptuneData.channels[ch].noteDatas[i].length) - 1,
								NOTE_HEIGHT * (SCALE_MAX - chiptuneData.channels[ch].noteDatas[i].musicalScale) + y2),
							new Vector3(
								NOTE_WIDTH * (i + chiptuneData.channels[ch].noteDatas[i].length) - 1,
								NOTE_HEIGHT * (SCALE_MAX - chiptuneData.channels[ch].noteDatas[i].musicalScale + 1) - 1),
							new Vector3(
								NOTE_WIDTH * i,
								NOTE_HEIGHT * (SCALE_MAX - chiptuneData.channels[ch].noteDatas[i].musicalScale + 1) - 1)
							},
							ENVELOPE_COLOR, ENVELOPE_COLOR);
					}

				}

			}
		}


		void OnEvent(Event evt)
		{
			var rect = new Rect(MAP_OFFSET_X, MAP_OFFSET_Y, position.width - MAP_OFFSET_X, position.height - MAP_OFFSET_Y);
			if (rect.Contains(evt.mousePosition))
			{
				if (evt.button == 0 || evt.button == 1)
				{
					rect.width = NOTE_WIDTH;
					rect.height = NOTE_HEIGHT;
					for (int x = 0; x < chiptuneData.length; x++)
					{
						for (int y = 0; y < SCALE_MAX; y++)
						{
							rect.x = MAP_OFFSET_X + x * NOTE_WIDTH - mapScrollPos.x;
							rect.y = MAP_OFFSET_Y + (SCALE_MAX - y) * NOTE_HEIGHT - mapScrollPos.y;
							if (rect.Contains(evt.mousePosition))
							{
								EditNote(evt, x, y);
							}
						}
					}
				}
				else if (evt.button == 2)
				{
					Scroll(evt);
				}
				if (evt.type == EventType.ScrollWheel)
				{
					Repaint();
				}
			}
			if (evt.type == EventType.KeyUp)
			{
				if (evt.keyCode == KeyCode.S && evt.shift)
				{
					Save();
				}
				Repaint();
			}
			if (evt.type == EventType.KeyDown)
			{
				if (evt.keyCode == KeyCode.A)
					SetEnvelope(128, 128);
				if (evt.keyCode == KeyCode.S)
					SetEnvelope(0, 128);
				if (evt.keyCode == KeyCode.D)
					SetEnvelope(128, 0);
				if (evt.keyCode == KeyCode.F)
					SetEnvelope(64, 64);
			}
			if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
			{
				// D&D
				var dropArea = new Rect(0, 0, position.width, position.height);
				if (dropArea.Contains(evt.mousePosition))
				{
					//マウスの形状
					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

					if (evt.type == EventType.DragPerform)
					{
						DragAndDrop.AcceptDrag();
						if (DragAndDrop.paths[0].Contains(".asset"))
						{
							var data = (ChiptuneDataObject)AssetDatabase.LoadAssetAtPath(DragAndDrop.paths[0], typeof(ChiptuneDataObject));
							if (data != null)
							{
								data.fileName = Regex.Match(DragAndDrop.paths[0], @"\/([^/]*?)\.asset").Groups[1].Value;
								chiptuneData = data;
							}
						}
						DragAndDrop.activeControlID = 0;
					}
					evt.Use();
				}
			}
		}

		void EditNote(Event evt, int x, int y)
		{
			switch (evt.type)
			{
				case EventType.MouseDown:
					if (evt.button == 0)
					{
						Undo.RecordObject(chiptuneData, "Chiptune Editor : Put");
						PutNote(x, y);
					}
					else
					{
						Undo.RecordObject(chiptuneData, "Chiptune Editor : Delete");
						DeleteNote(x, y);
					}
					GUI.FocusControl("");
					Repaint();
					break;
				case EventType.MouseDrag:
					break;
				case EventType.MouseUp:
				case EventType.MouseLeaveWindow:
					break;
			}
		}

		void Scroll(Event evt)
		{
			switch (evt.type)
			{
				case EventType.MouseDown:
					mapScrollTouchBegin = evt.mousePosition;
					mapScrollPosBegin = mapScrollPos;
					break;
				case EventType.MouseDrag:
					mapScrollPos = mapScrollPosBegin + (mapScrollTouchBegin - evt.mousePosition);
					if (mapScrollPos.x < 0) mapScrollPos.x = 0;
					if (mapScrollPos.y < 0) mapScrollPos.y = 0;
					Repaint();
					break;
				case EventType.MouseUp:
				case EventType.MouseLeaveWindow:
					mapScrollPosBegin = mapScrollTouchBegin = Vector2.zero;
					break;
			}
		}

		void PutNote(int x, int y)
		{
			if (x < 0 || x >= chiptuneData.length || y < 0 || y >= SCALE_MAX) return;

			select = GetNote(x, y);
			if (select >= 0)
			{
				TestPlayTone(channel, select);
				return;
			}

			for (int i = 0; i < note_length[noteLength]; i++)
			{
				DeleteNote(x + i, -1);
			}

			chiptuneData.channels[channel].noteDatas[x].length = (byte)note_length[noteLength];
			chiptuneData.channels[channel].noteDatas[x].musicalScale = (byte)y;
			chiptuneData.channels[channel].noteDatas[x].beginGain = (byte)currentEnvelope.x;
			chiptuneData.channels[channel].noteDatas[x].endGain = (byte)currentEnvelope.y;
			select = x;
			TestPlayTone(channel, select);
		}

		void DeleteNote(int x, int y)
		{
			if (x < 0 || x >= chiptuneData.length || y >= SCALE_MAX) return;

			for (int i = 0; i < 16; i++)
			{
				if (x - i >= 0 && chiptuneData.channels[channel].noteDatas[x - i].length > i)
				{
					if (y < 0 || chiptuneData.channels[channel].noteDatas[x - i].musicalScale == y)
					{
						chiptuneData.channels[channel].noteDatas[x - i].length = 0;
					}
				}
			}
			select = -1;
			TestStopTone();
		}

		int GetNote(int x, int y)
		{
			if (x < 0 || x >= chiptuneData.length || y >= SCALE_MAX) return -1;
			for (int i = 0; i < 16; i++)
			{
				if (x - i >= 0 && chiptuneData.channels[channel].noteDatas[x - i].length > i)
				{
					if (y < 0 || chiptuneData.channels[channel].noteDatas[x - i].musicalScale == y)
					{
						return x - i;
					}
				}
			}
			return -1;
		}

		void SetEnvelope(byte begin, byte end)
		{
			if (select >= 0)
			{
				chiptuneData.channels[channel].noteDatas[select].beginGain = begin;
				chiptuneData.channels[channel].noteDatas[select].endGain = end;
				Repaint();
			}
		}

		void Shift(int channel, int shift)
		{
			Undo.RecordObject(chiptuneData, "Chiptune Editor : Shift");
			for (int i = 0; i < chiptuneData.channels[channel].noteDatas.Count; i++)
			{
				chiptuneData.channels[channel].noteDatas[i].musicalScale = (byte)Mathf.Clamp(chiptuneData.channels[channel].noteDatas[i].musicalScale + shift, 1, SCALE_MAX);
			}
		}

		void New()
		{
			chiptuneData = ChiptuneDataObject.Create();
			string path;
			int i = 0;
			while (true)
			{
				path = string.Format(Application.dataPath + FILE_PATH + "/sound{0:000}.asset", i);
				if (!System.IO.File.Exists(path)) break;
				i++;
			}
			chiptuneData.fileName = string.Format("sound{0:000}", i);
		}

		void Save()
		{
			var path = AssetDatabase.GetAssetPath(chiptuneData);
			if (path.Length > 0)
			{
				EditorUtility.SetDirty(chiptuneData);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			else
			{
				path = EditorUtility.SaveFilePanelInProject("データ保存", chiptuneData.fileName, "asset", "message");
				if (path.Length > 0)
				{
					Utility.CreateFolder(path);
					AssetDatabase.CreateAsset(chiptuneData, path);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}
			}
		}

		void Load()
		{
			var path = EditorUtility.OpenFilePanel("データ読込", Application.dataPath + FILE_PATH, "asset");
			if (path.Length > 0)
			{
				path = "Assets" + path.Replace(Application.dataPath, "");
				var data = (ChiptuneDataObject)AssetDatabase.LoadAssetAtPath(path, typeof(ChiptuneDataObject));
				if (data != null)
				{
					data.fileName = Regex.Match(path, @"\/([^/]*?)\.asset").Groups[1].Value;
					chiptuneData = data;
				}
				AssetDatabase.Refresh();
				select = -1;
				scrollPos = Vector2.zero;
			}
		}


		bool TestPlay(bool isPlaying)
		{
			if (!Application.isPlaying)
			{
				//if (isPlaying)
				//	editorPlayer.StopBGM();
				//else
				//	editorPlayer.Play(chiptuneData);
				//return !isPlaying;
			}

			else if (player != null)
			{
				if (isPlaying)
					player.StopBGM();
				else
					player.PlayBGM(chiptuneData, continuePoint);
				return !isPlaying;
			}
			return false;
		}

		void TestPlayTone(int ch, int index)
		{
			if (!Application.isPlaying)
			{
				//if (isPlaying)
				//	editorPlayer.StopBGM();
				//isPlaying = false;

				//ChiptuneDataObject.NoteData noteData = chiptuneData.channels[ch].noteDatas[index];
				//editorPlayer.PlayTone(chiptuneData.channels[ch].state, noteData.beginGain, noteData.endGain, noteData.musicalScale, noteData.length, chiptuneData.bpm);
			}

			else if (player != null)
			{
				if (isPlaying)
					player.StopBGM();
				isPlaying = false;

				ChiptuneDataObject.NoteData noteData = chiptuneData.channels[ch].noteDatas[index];
				player.PlayTone(chiptuneData.channels[ch].state, noteData.beginGain, noteData.endGain, noteData.musicalScale, noteData.length, chiptuneData.bpm);
			}
		}

		void TestStopTone()
		{
			if (!Application.isPlaying) return;

			if (player != null)
			{
				player.StopTone();
			}
		}

		float GetPlayerTime()
		{
			if (player != null)
			{
				return player.GetTime();
			}
			return 0;
		}
	}
}