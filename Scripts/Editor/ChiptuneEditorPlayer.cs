﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace CommonPackage.Chiptune
{
	public class ChiptuneEditorPlayer
	{
		float volume = 0.5f;

		AudioClip[] clips = null;
		ChiptuneWave[] waves = null;

		ChiptuneDataObject data = null;
		bool isPlaying = false;

		float time = 0;

		public int position = 0;
		public int samplerate = 44100;

		//float preTime = -1;
		DateTime preDateTime = DateTime.Now;

		List<int> playingNoteIndexs = new List<int>();

		IEnumerator iEnumrator = null;

		const int CHANNEL_NUM = 4;

		public ChiptuneEditorPlayer()
		{
			clips = new AudioClip[CHANNEL_NUM];
			waves = new ChiptuneWave[CHANNEL_NUM];

			for (int i = 0; i < CHANNEL_NUM; i++)
			{
				var wave = new ChiptuneWave();
				wave.Set(ChiptuneWave.PlayState.None);
				waves[i] = wave;

				playingNoteIndexs.Add(0);
			}
		}

		void OnAudioRead(float[] data, ChiptuneWave wave)
		{
			wave.OnAudioRead(data, 1);
		}

		void OnAudioSetPosition(int newPosition)
		{
			position = newPosition;
		}

		public void Update()
		{
			float delta = (float)(DateTime.Now - preDateTime).TotalSeconds;

			if (data != null && isPlaying)
			{
				time += delta;
				float t = time * (data.bpm / 15.0f);
				if (t >= data.length)
				{
					t = t - data.length + data.loopPoint;
					time = t / (data.bpm / 15.0f);
				}
				for (int ch = 0; ch < CHANNEL_NUM; ch++)
				{
					// 再生中のノート
					var n = data.channels[ch].noteDatas[playingNoteIndexs[ch]];
					if (n == null || t > playingNoteIndexs[ch] + n.length || t < playingNoteIndexs[ch])
					{
						playingNoteIndexs[ch] = (int)t;
						n = data.channels[ch].noteDatas[playingNoteIndexs[ch]];
					}

					if (n != null && n.length > 0)
					{
						float f = t - playingNoteIndexs[ch];
						float gain = (n.beginGain + (n.endGain - n.beginGain) * (f / n.length)) / 128.0f * volume;
						waves[ch].Set(data.channels[ch].state, gain, n.musicalScale);
					}
					else
					{
						waves[ch].Set(ChiptuneWave.PlayState.None);
					}

				}
			}
			if (iEnumrator != null)
			{
				iEnumrator.MoveNext();
			}

			preDateTime = DateTime.Now;
		}

		public void Play(ChiptuneDataObject data)
		{
			this.data = data;
			time = 0;
			isPlaying = true;

			for (int i = 0; i < CHANNEL_NUM; i++)
			{
				var wave = waves[i];
				clips[i] = AudioClip.Create("Chiptune", samplerate * (int)(data.length / (data.bpm / 15.0f)), 1, samplerate, true, (float[] d) => { OnAudioRead(d, wave); }, OnAudioSetPosition);

				PlayClip(clips[i]);
			}
		}

		public void StopBGM()
		{
			data = null;
			isPlaying = false;

			for (int ch = 0; ch < CHANNEL_NUM; ch++)
			{
				waves[ch].Set(ChiptuneWave.PlayState.None);
				StopClip(clips[ch]);
			}
		}

		public void PlayTone(ChiptuneWave.PlayState state, int beginGain, int endGain, int scale, int length, int bpm)
		{
			StopBGM();
			iEnumrator = Tone(state, beginGain, endGain, scale, length, bpm);
			var wave = waves[0];
			clips[0] = AudioClip.Create("Chiptune", (int)(samplerate * length / (bpm / 15.0f)), 1, samplerate, true, (float[] d) => { OnAudioRead(d, wave); }, OnAudioSetPosition);

			PlayClip(clips[0]);
		}

		IEnumerator Tone(ChiptuneWave.PlayState state, int beginGain, int endGain, int scale, int length, int bpm)
		{
			float t = 0;
			while (t < length)
			{
				float delta = (float)(DateTime.Now - preDateTime).TotalSeconds;
				t += delta * (bpm / 15.0f);

				float gain = (beginGain + (endGain - beginGain) * (t / length)) / 128.0f * volume;
				waves[0].Set(state, gain, scale);
				//channels[0].audioSource.volume = volume;
				preDateTime = DateTime.Now;
				yield return null;
			}
			waves[0].Set(ChiptuneWave.PlayState.None);
			iEnumrator = null;
		}

		public void StopTone()
		{
			StopBGM();
			iEnumrator = null;
		}

		public float GetTime()
		{
			if (data == null) return 0;
			return time * (data.bpm / 15.0f);
		}

		void PlayClip(AudioClip clip)
		{
			if (clip == null) return;

			var unityEditorAssembly = typeof(AudioImporter).Assembly;
			var audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			var method = audioUtilClass.GetMethod
			(
				"PlayClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new Type[] { typeof(AudioClip) },
				null
			);

			method.Invoke(null, new object[] { clip });
		}

		void StopClip(AudioClip clip)
		{
			if (clip == null) return;

			var unityEditorAssembly = typeof(AudioImporter).Assembly;
			var audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			var method = audioUtilClass.GetMethod(
				"StopClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new Type[] { typeof(AudioClip) },
				null
			);

			method.Invoke(null, new object[] { clip });
		}
	}
}