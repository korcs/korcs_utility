﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace CommonPackage
{
	public class TileEditorWindow : EditorWindow
	{
		const string FILE_PATH = "Database";
		const int MARGIN = 6;
		static Color NORMAL_COLOR = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		const int CHIP_SIZE = 16;
		const int CHIP_H_NUM = 16;
		const int CHIP_DRAW_SIZE = 32;

		const int CONTENT_HEIGHT = 110;
		const int CONTENT_WIDTH = 200;
		const int CONTENT_H_NUM = 4;

		[SerializeField]
		string fileName = "";
		[SerializeField]
		TileDataObject data;
		[SerializeField]
		Vector2 scrollPos;

		DateTime preDateTime = DateTime.Now;
		double timeCount = 0;

		string[] _tagNames = { "-" };
		string[] tagNames
		{
			get
			{
				if (data == null || data.tagData == null) return null;
				if (_tagNames == null || _tagNames.Length != data.tagData.tags.Count)
				{
					_tagNames = new string[data.tagData.tags.Count];
					for (int i = 0; i < data.tagData.tags.Count; i++)
					{
						_tagNames[i] = data.tagData.tags[i].name;
					}
				}
				return _tagNames;
			}
		}

		[MenuItem("Tools/korcs/Tile Editor", priority = 20)]
		static void Open()
		{
			GetWindow<TileEditorWindow>("Tile Editor");
		}

		void OnEnable()
		{
			//titleContent.text = "Tile Editor";
			//Texture2D tex = new Texture2D(16, 16, TextureFormat.ARGB32, false);
			//tex.LoadImage(new byte[] { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 16, 0, 0, 0, 16, 8, 6, 0, 0, 0, 31, 243, 255, 97, 0, 0, 0, 172, 73, 68, 65, 84, 56, 17, 165, 147, 81, 14, 195, 32, 12, 67, 151, 169, 135, 225, 40, 28, 153, 219, 181, 53, 194, 81, 234, 132, 77, 211, 250, 67, 113, 236, 71, 130, 90, 107, 173, 189, 254, 121, 142, 34, 124, 22, 90, 148, 44, 110, 20, 112, 246, 222, 99, 61, 189, 143, 49, 112, 128, 67, 44, 140, 224, 225, 219, 148, 130, 16, 8, 95, 245, 9, 121, 171, 115, 23, 134, 175, 170, 37, 128, 2, 191, 237, 9, 240, 139, 99, 155, 85, 80, 106, 51, 195, 59, 152, 243, 163, 69, 49, 37, 14, 61, 107, 28, 123, 0, 146, 123, 35, 132, 187, 48, 142, 176, 177, 102, 153, 29, 176, 242, 51, 0, 35, 178, 125, 64, 8, 176, 208, 22, 225, 229, 170, 62, 2, 74, 179, 138, 218, 62, 234, 17, 80, 118, 193, 19, 25, 94, 251, 242, 83, 230, 129, 254, 77, 80, 144, 213, 195, 208, 245, 103, 130, 246, 48, 64, 248, 244, 92, 13, 110, 61, 68, 165, 97, 218, 91, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130 });
			//tex.filterMode = FilterMode.Point;
			//titleContent.image = tex;
			Repaint();
		}

		void OnDisable()
		{
			Repaint();
		}

		void OnGUI()
		{
			EditorGUI.BeginDisabledGroup(Application.isPlaying);

			// 設定
			using (new GUILayout.AreaScope(new Rect(MARGIN, MARGIN, position.width - (MARGIN * 2), 80)))
			{
				using (new GUILayout.HorizontalScope())
				{
					using (new GUILayout.VerticalScope(GUILayout.Width(80)))
					{
						if (GUILayout.Button("新規作成"))
						{
							New();
						}
						if (GUILayout.Button("読込"))
						{
							Load();
						}
						if (GUILayout.Button("保存"))
						{
							Save();
						}
					}
					GUILayout.FlexibleSpace();
					if (data != null)
					{
						using (new GUILayout.VerticalScope(GUILayout.Width(200)))
						{
							using (new GUILayout.HorizontalScope())
							{
								EditorGUI.BeginDisabledGroup(AssetDatabase.GetAssetPath(data).Length > 0);
								GUILayout.Label("ファイル名", GUILayout.Width(80));
								fileName = EditorGUILayout.TextField(fileName);
								GUILayout.Label(".asset");
								EditorGUI.EndDisabledGroup();
							}
							using (new GUILayout.HorizontalScope())
							{
								GUILayout.Label("Texture", GUILayout.Width(80));
								data.texture = EditorGUILayout.ObjectField(data.texture, typeof(Texture2D), false) as Texture2D;
							}
							using (new GUILayout.HorizontalScope())
							{
								GUILayout.Label("Tag", GUILayout.Width(80));
								data.tagData = EditorGUILayout.ObjectField(data.tagData, typeof(TagDataObject), false) as TagDataObject;
							}
							using (new GUILayout.HorizontalScope())
							{
								GUILayout.Label("Size", GUILayout.Width(80));
								data.size = EditorGUILayout.IntField(data.size);
							}
						}
						using (new GUILayout.VerticalScope(GUILayout.Width(200)))
						{
							if (GUILayout.Button("データ追加"))
							{
								Undo.RecordObject(data, "Edit Database");
								data.tiles.Add(new TileDataObject.Tile());
							}
						}
					}
				}
			}

			// 設定
			var rect = new Rect(0, 80 + MARGIN * 2, position.width, position.height - (80 + MARGIN * 2));
			using (new GUILayout.AreaScope(rect))
			{
				if (data == null)
				{
					GUILayout.Label("データベースを読み込むか新規作成してください");
				}
				else
				{
					using (var scrollView = new GUILayout.ScrollViewScope(scrollPos))
					{
						if (scrollPos != scrollView.scrollPosition)
							GUI.FocusControl("");
						scrollPos = scrollView.scrollPosition;
						Rect maskRect = new Rect(scrollPos.x, scrollPos.y, position.width - 16, position.height - (80 + MARGIN * 2) - 16);
						//data.ShowGUI(maskRect);

						using (new GUILayout.VerticalScope())
						{
							int min = Mathf.Max(0, (int)(maskRect.yMin / (CONTENT_HEIGHT / CONTENT_H_NUM)) - 1);
							int max = Mathf.Min(data.tiles.Count, (int)(maskRect.yMax / (CONTENT_HEIGHT / CONTENT_H_NUM)) + 1);
							min = Mathf.Clamp(min - min % CONTENT_H_NUM, 0, data.tiles.Count);
							max = Mathf.Clamp(max + CONTENT_H_NUM - (max % CONTENT_H_NUM), 0, data.tiles.Count);
							EditorGUILayout.GetControlRect(GUILayout.Height((min) * CONTENT_HEIGHT / CONTENT_H_NUM));
							GUILayout.BeginHorizontal();
							for (int i = min; i < max; i++)
							{
								if (i % CONTENT_H_NUM == 0 && i != 0)
								{
									GUILayout.EndHorizontal();
									GUILayout.BeginHorizontal();
								}

								ShowItem(i, maskRect);
							}
							GUILayout.EndHorizontal();
							EditorGUILayout.GetControlRect(GUILayout.Height(((data.tiles.Count - max) + 1) * CONTENT_HEIGHT / CONTENT_H_NUM));
						}
					}
				}
			}

			if (!Application.isPlaying)
			{
				OnEvent(Event.current);
			}
			else
			{
				GUI.color = new Color(0, 0, 0, 0.5f);
				GUI.DrawTexture(new Rect(0, 0, position.width, position.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
			}

			EditorGUI.EndDisabledGroup();

			TimeCount();
			Repaint();
		}

		void ShowItem(int i, Rect maskRect)
		{
			if (data.tiles[i].ids.Count < 1)
				data.tiles[i].ids.Add(0);

			GUILayout.BeginVertical(GUI.skin.box, GUILayout.Height(CONTENT_HEIGHT), GUILayout.Width(CONTENT_WIDTH));
			GUILayout.BeginHorizontal();
			GUILayout.Label("ID" + i.ToString("000"));
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("↑Add", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(42)))
			{
				Undo.RecordObject(this, "Database Editor : Add");
				data.tiles.Insert(i, new TileDataObject.Tile());
			}
			EditorGUI.BeginDisabledGroup(i == 0);
			if (GUILayout.Button("▲", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(32)))
			{
				Undo.RecordObject(this, "Database Editor : Move");
				var tmp = data.tiles[i];
				data.tiles.RemoveAt(i);
				data.tiles.Insert(i - 1, tmp);
			}
			EditorGUI.EndDisabledGroup();
			EditorGUI.BeginDisabledGroup(i == data.tiles.Count - 1);
			if (GUILayout.Button("▼", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(32)))
			{
				Undo.RecordObject(this, "Database Editor : Move");
				var tmp = data.tiles[i];
				data.tiles.RemoveAt(i);
				data.tiles.Insert(i + 1, tmp);
			}
			EditorGUI.EndDisabledGroup();
			if (GUILayout.Button("×", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(20)))
			{
				Undo.RecordObject(this, "Database Editor : Remove");
				data.tiles.RemoveAt(i);
				return;
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			GUILayout.BeginVertical();
			using (new GUILayout.HorizontalScope())
			{
				//GUILayout.Label("isMovable");
				//data.tiles[i].isMovable = EditorGUILayout.Toggle(data.tiles[i].isMovable);
				if (data.tagData != null)
				{
					GUILayout.Label("tag");
					data.tiles[i].tag = EditorGUILayout.Popup(data.tiles[i].tag, tagNames);
				}
			}
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("isAnimate");
				data.tiles[i].isAnimate = EditorGUILayout.Toggle(data.tiles[i].isAnimate);
			}
			GUILayout.EndVertical();
			int n = 0;
			if (data.tiles[i].isAnimate)
			{
				n = (int)((timeCount * data.tiles[i].speed) % data.tiles[i].ids.Count);
			}
			var rect = EditorGUILayout.GetControlRect(GUILayout.Width(CHIP_DRAW_SIZE), GUILayout.Height(CHIP_DRAW_SIZE));
			DrawChip(rect, data.tiles[i].ids[n], NORMAL_COLOR, data.texture, maskRect);
			GUILayout.EndHorizontal();

			if (data.tiles[i].isAnimate)
			{
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("id");
					for (int j = 0; j < 4; j++)
					{
						if (data.tiles[i].ids.Count <= j) data.tiles[i].ids.Add(0);
						data.tiles[i].ids[j] = EditorGUILayout.IntField(data.tiles[i].ids[j], GUILayout.Width(30));
					}
				}
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("speed");
					data.tiles[i].speed = EditorGUILayout.FloatField(data.tiles[i].speed, GUILayout.Width(60));
				}
			}
			else
			{
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("id");
					data.tiles[i].ids[0] = EditorGUILayout.IntField(data.tiles[i].ids[0], GUILayout.Width(60));
				}
			}
			data.SetHash(i);

			GUILayout.EndVertical();
		}

		public void DrawChip(Rect rect, int index, Color color, Texture2D texture, Rect maskRect = new Rect(), float angle = 0)
		{
			if (texture == null) return;
			Rect srcRect = new Rect(
				(float)(index % CHIP_H_NUM * CHIP_SIZE) / texture.width,
				(float)(texture.height - index / CHIP_H_NUM * CHIP_SIZE - CHIP_SIZE) / texture.height,
				(float)CHIP_SIZE / texture.width,
				(float)CHIP_SIZE / texture.height);
			Utility.DrawTexture(srcRect, rect, color, texture, maskRect, angle);
		}

		void TimeCount()
		{
			var t = DateTime.Now - preDateTime;
			timeCount += t.TotalSeconds;
			preDateTime = DateTime.Now;
		}

		void OnEvent(Event evt)
		{
			if (evt.type == EventType.MouseDown)
			{
				GUI.FocusControl("");
				Repaint();
			}
			if (evt.type == EventType.KeyUp)
			{
				if (evt.keyCode == KeyCode.S && evt.shift)
				{
					Save();
				}
				Repaint();
			}
			if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
			{
				// D&D
				var dropArea = new Rect(0, 0, position.width, position.height);
				if (dropArea.Contains(evt.mousePosition))
				{
					//マウスの形状
					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

					if (evt.type == EventType.DragPerform)
					{
						DragAndDrop.AcceptDrag();
						var load = (TileDataObject)AssetDatabase.LoadAssetAtPath(DragAndDrop.paths[0], typeof(TileDataObject));
						if (load != null)
						{
							data = load;
							var tmp = DragAndDrop.paths[0].Split('/');
							fileName = tmp[tmp.Length - 1].Replace(".asset", "");
						}
						DragAndDrop.activeControlID = 0;
					}
					evt.Use();
				}
			}
		}

		void New()
		{
			data = TileDataObject.Create();
			fileName = "new";
		}

		void Save()
		{
			var path = AssetDatabase.GetAssetPath(data);
			if (path.Length > 0)
			{
				EditorUtility.SetDirty(data);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			else
			{
				path = EditorUtility.SaveFilePanelInProject("データ保存", data.fileName, "asset", "message");
				if (path.Length > 0)
				{
					Utility.CreateFolder(path);
					AssetDatabase.CreateAsset(data, path);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}
			}
		}

		void Load()
		{
			var path = EditorUtility.OpenFilePanel("データ読込", Application.dataPath + FILE_PATH, "asset");
			if (System.IO.File.Exists(path))
			{
				path = "Assets" + path.Replace(Application.dataPath, "");
				var load = (TileDataObject)AssetDatabase.LoadAssetAtPath(path, typeof(TileDataObject));
				if (load != null)
				{
					data = load;
					fileName = path.Replace("Assets/" + FILE_PATH + "/", "").Replace(".asset", "");
				}
				AssetDatabase.Refresh();
			}
			else
			{
				New();
			}
		}
	}
}