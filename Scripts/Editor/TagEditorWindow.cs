﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace CommonPackage
{
	public class TagEditorWindow : EditorWindow
	{
		const string FILE_PATH = "Database";
		const int MARGIN = 6;
		static Color NORMAL_COLOR = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		const int CONTENT_HEIGHT = 100;
		const int CONTENT_WIDTH = 160;
		const int CONTENT_H_NUM = 4;

		[SerializeField]
		string fileName = "";
		[SerializeField]
		TagDataObject data;
		[SerializeField]
		Vector2 scrollPos;

		DateTime preDateTime = DateTime.Now;
		double timeCount = 0;

		[MenuItem("Tools/korcs/Tag Editor", priority = 30)]
		static void Open()
		{
			GetWindow<TagEditorWindow>("Tag Editor");
		}

		void OnEnable()
		{
			//titleContent.text = "Tag Editor";
			//Texture2D tex = new Texture2D(16, 16, TextureFormat.ARGB32, false);
			//tex.LoadImage(new byte[] { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 16, 0, 0, 0, 16, 8, 6, 0, 0, 0, 31, 243, 255, 97, 0, 0, 0, 134, 73, 68, 65, 84, 56, 17, 181, 144, 81, 14, 192, 32, 8, 67, 117, 217, 97, 188, 255, 5, 183, 65, 120, 166, 152, 248, 177, 168, 36, 11, 45, 180, 12, 172, 173, 181, 178, 18, 215, 138, 217, 188, 91, 7, 60, 223, 64, 251, 8, 229, 138, 233, 123, 190, 19, 203, 164, 6, 53, 179, 98, 84, 94, 211, 1, 136, 16, 144, 181, 174, 216, 251, 71, 223, 128, 13, 198, 251, 19, 215, 19, 48, 144, 77, 104, 193, 218, 35, 247, 166, 14, 64, 232, 13, 49, 206, 184, 215, 143, 191, 129, 222, 171, 152, 173, 138, 158, 208, 139, 1, 204, 192, 89, 35, 54, 137, 247, 116, 0, 226, 240, 119, 115, 23, 71, 35, 233, 182, 190, 1, 127, 254, 149, 95, 54, 249, 25, 157, 233, 25, 239, 49, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130 });
			//tex.filterMode = FilterMode.Point;
			//titleContent.image = tex;
			Repaint();
		}

		void OnDisable()
		{
			Repaint();
		}

		void OnGUI()
		{
			EditorGUI.BeginDisabledGroup(Application.isPlaying);

			// 設定
			using (new GUILayout.AreaScope(new Rect(MARGIN, MARGIN, position.width - (MARGIN * 2), 80)))
			{
				using (new GUILayout.HorizontalScope())
				{
					using (new GUILayout.VerticalScope(GUILayout.Width(80)))
					{
						if (GUILayout.Button("新規作成"))
						{
							New();
						}
						if (GUILayout.Button("読込"))
						{
							Load();
						}
						if (GUILayout.Button("保存"))
						{
							Save();
						}
					}
					GUILayout.FlexibleSpace();
					if (data != null)
					{
						using (new GUILayout.VerticalScope(GUILayout.Width(200)))
						{
							using (new GUILayout.HorizontalScope())
							{
								EditorGUI.BeginDisabledGroup(AssetDatabase.GetAssetPath(data).Length > 0);
								GUILayout.Label("ファイル名");
								fileName = EditorGUILayout.TextField(fileName);
								GUILayout.Label(".asset");
								EditorGUI.EndDisabledGroup();
							}
							if (GUILayout.Button("データ追加"))
							{
								Undo.RecordObject(data, "Edit Database");
								data.tags.Add(new TagDataObject.Tag());
							}
						}
					}
				}
			}

			// 設定
			var rect = new Rect(0, 80 + MARGIN * 2, position.width, position.height - (80 + MARGIN * 2));
			using (new GUILayout.AreaScope(rect))
			{
				if (data == null)
				{
					GUILayout.Label("データベースを読み込むか新規作成してください");
				}
				else
				{
					using (var scrollView = new GUILayout.ScrollViewScope(scrollPos))
					{
						if (scrollPos != scrollView.scrollPosition)
							GUI.FocusControl("");
						scrollPos = scrollView.scrollPosition;
						Rect maskRect = new Rect(scrollPos.x, scrollPos.y, position.width - 16, position.height - (80 + MARGIN * 2) - 16);
						//data.ShowGUI(maskRect);

						using (new GUILayout.VerticalScope())
						{
							int min = Mathf.Max(0, (int)(maskRect.yMin / (CONTENT_HEIGHT / CONTENT_H_NUM)) - 1);
							int max = Mathf.Min(data.tags.Count, (int)(maskRect.yMax / (CONTENT_HEIGHT / CONTENT_H_NUM)) + 1);
							min = Mathf.Clamp(min - min % CONTENT_H_NUM, 0, data.tags.Count);
							max = Mathf.Clamp(max + CONTENT_H_NUM - (max % CONTENT_H_NUM), 0, data.tags.Count);
							EditorGUILayout.GetControlRect(GUILayout.Height((min) * CONTENT_HEIGHT / CONTENT_H_NUM));
							GUILayout.BeginHorizontal();
							for (int i = min; i < max; i++)
							{
								if (i % CONTENT_H_NUM == 0 && i != 0)
								{
									GUILayout.EndHorizontal();
									GUILayout.BeginHorizontal();
								}

								ShowItem(i, maskRect);
							}
							GUILayout.EndHorizontal();
							EditorGUILayout.GetControlRect(GUILayout.Height(((data.tags.Count - max) + 1) * CONTENT_HEIGHT / CONTENT_H_NUM));
						}
					}
				}
			}

			if (!Application.isPlaying)
			{
				OnEvent(Event.current);
			}
			else
			{
				GUI.color = new Color(0, 0, 0, 0.5f);
				GUI.DrawTexture(new Rect(0, 0, position.width, position.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
			}

			EditorGUI.EndDisabledGroup();

			TimeCount();
			Repaint();
		}

		void ShowItem(int i, Rect maskRect)
		{

			GUILayout.BeginVertical(GUI.skin.box, GUILayout.Height(CONTENT_HEIGHT), GUILayout.Width(CONTENT_WIDTH));
			GUILayout.BeginHorizontal();
			GUILayout.Label("ID" + i.ToString("000"));
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("↑Add", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(42)))
			{
				Undo.RecordObject(this, "Database Editor : Add");
				data.tags.Insert(i, new TagDataObject.Tag());
			}
			EditorGUI.BeginDisabledGroup(i == 0);
			if (GUILayout.Button("▲", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(32)))
			{
				Undo.RecordObject(this, "Database Editor : Move");
				var tmp = data.tags[i];
				data.tags.RemoveAt(i);
				data.tags.Insert(i - 1, tmp);
			}
			EditorGUI.EndDisabledGroup();
			EditorGUI.BeginDisabledGroup(i == data.tags.Count - 1);
			if (GUILayout.Button("▼", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(32)))
			{
				Undo.RecordObject(this, "Database Editor : Move");
				var tmp = data.tags[i];
				data.tags.RemoveAt(i);
				data.tags.Insert(i + 1, tmp);
			}
			EditorGUI.EndDisabledGroup();
			if (GUILayout.Button("×", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(20)))
			{
				Undo.RecordObject(this, "Database Editor : Remove");
				data.tags.RemoveAt(i);
				return;
			}
			GUILayout.EndHorizontal();

			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("Name", GUILayout.Width(60));
				data.tags[i].name = EditorGUILayout.TextField(data.tags[i].name);
			}
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("Label", GUILayout.Width(60));
				data.tags[i].label = EditorGUILayout.TextField(data.tags[i].label);
			}
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("BG Color", GUILayout.Width(60));
				data.tags[i].color = EditorGUILayout.ColorField(data.tags[i].color);
			}
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("Color", GUILayout.Width(60));
				data.tags[i].style.normal.textColor = EditorGUILayout.ColorField(data.tags[i].style.normal.textColor);
				data.tags[i].style.fontSize = 20;
				data.tags[i].style.fontStyle = FontStyle.Bold;
				data.tags[i].style.alignment = TextAnchor.MiddleCenter;
			}

			GUILayout.EndVertical();
		}

		void TimeCount()
		{
			var t = DateTime.Now - preDateTime;
			timeCount += t.TotalSeconds;
			preDateTime = DateTime.Now;
		}

		void OnEvent(Event evt)
		{
			if (evt.type == EventType.MouseDown)
			{
				GUI.FocusControl("");
				Repaint();
			}
			if (evt.type == EventType.KeyUp)
			{
				if (evt.keyCode == KeyCode.S && evt.shift)
				{
					Save();
				}
				Repaint();
			}
			if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
			{
				// D&D
				var dropArea = new Rect(0, 0, position.width, position.height);
				if (dropArea.Contains(evt.mousePosition))
				{
					//マウスの形状
					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

					if (evt.type == EventType.DragPerform)
					{
						DragAndDrop.AcceptDrag();
						var load = (TagDataObject)AssetDatabase.LoadAssetAtPath(DragAndDrop.paths[0], typeof(TagDataObject));
						if (load != null)
						{
							data = load;
							var tmp = DragAndDrop.paths[0].Split('/');
							fileName = tmp[tmp.Length - 1].Replace(".asset", "");
						}
						DragAndDrop.activeControlID = 0;
					}
					evt.Use();
				}
			}
		}

		void New()
		{
			data = TagDataObject.Create();
			fileName = "new";
		}

		void Save()
		{
			var path = AssetDatabase.GetAssetPath(data);
			if (path.Length > 0)
			{
				EditorUtility.SetDirty(data);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			else
			{
				path = EditorUtility.SaveFilePanelInProject("データ保存", data.fileName, "asset", "message");
				if (path.Length > 0)
				{
					Utility.CreateFolder(path);
					AssetDatabase.CreateAsset(data, path);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}
			}
		}

		void Load()
		{
			var path = EditorUtility.OpenFilePanel("データ読込", Application.dataPath + FILE_PATH, "asset");
			if (System.IO.File.Exists(path))
			{
				path = "Assets" + path.Replace(Application.dataPath, "");
				var load = (TagDataObject)AssetDatabase.LoadAssetAtPath(path, typeof(TagDataObject));
				if (load != null)
				{
					data = load;
					fileName = path.Replace("Assets/" + FILE_PATH + "/", "").Replace(".asset", "");
				}
				AssetDatabase.Refresh();
			}
			else
			{
				New();
			}
		}
	}
}