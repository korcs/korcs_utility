﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CommonPackage
{
    /// <summary>
    /// Imageで数値を表示
    /// </summary>
    [RequireComponent(typeof(CanvasRenderer))]
    [AddComponentMenu("UI/Number")]
    public class Number : MaskableGraphic, ILayoutElement
    {
        [SerializeField]
        int _num = 0;
        public int num { get { return _num; } set { _num = value; UpdateNum(_num); } }
        [SerializeField]
        int digit = 3;
        [SerializeField]
        bool fill = false;
        [SerializeField]
        TextAnchor alignment = TextAnchor.MiddleCenter;
        [SerializeField]
        float spacing = 0;
        [SerializeField]
        List<Sprite> spriteList = new List<Sprite>(10);
        List<Image> imageList = new List<Image>();
        [SerializeField]
        float animationTime = 1.0f;
        [SerializeField]
        float animationInterval = 0.05f;

        [SerializeField]
        bool isAtlas = true;
        [SerializeField]
        Texture texture = null;
        [SerializeField]
        float charWidth = 1;
        [SerializeField]
        float charHeight = 1;
        int textureWidth = 0;
        int textureHeight = 0;
        int hNum = 1;
        int showNum = 0;

        new void Awake()
        {
            base.Awake();
            if (GetComponent<CanvasRenderer>() == null)
            {
                gameObject.AddComponent<CanvasRenderer>();
            }
            if (!isAtlas && Application.isPlaying)
            {
                if (spriteList.Count != 10)
                {
                    Debug.LogError("Sprite Listは0~9の順で10個のSpriteを設定してください。");
                }

                var layout = gameObject.AddComponent<HorizontalLayoutGroup>();
                layout.childAlignment = alignment;
                layout.childForceExpandHeight = false;
                layout.childForceExpandWidth = false;
                layout.spacing = spacing;

                // Imageを作成
                for (int i = 0; i < digit; i++)
                {
                    GameObject obj = new GameObject("" + i);
                    Image image = obj.AddComponent<Image>();
                    image.sprite = spriteList[0];
                    image.rectTransform.SetParent(GetComponent<RectTransform>());
                    image.rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
                    image.rectTransform.localScale = Vector3.one;
                    image.rectTransform.localPosition = Vector3.zero;

                    imageList.Add(image);
                }
                UpdateNum(num);
            }
            SetTextureSize();
        }

        void SetTextureSize()
        {
            if (isAtlas && texture != null)
            {
                textureWidth = texture.width;
                textureHeight = texture.height;
                hNum = (int)(textureWidth / charWidth);
            }
        }

        public void SetNum(int num, bool isAnimation = false)
        {
            int current = this.num;
            this.num = num;
            if (isAnimation)
            {
                StartCoroutine(StartAnimation(current, num));
            }
            else
            {
                UpdateNum(num);
            }
        }

        public int GetNum()
        {
            return num;
        }

        void UpdateNum(int num)
        {
            showNum = num;
            if (isAtlas)
            {
                // メッシュを更新する（他にいい方法があるのでは……）
                SetVerticesDirty();
                SetLayoutDirty();
                var tmp = rectTransform.localScale;
                rectTransform.localScale = Vector3.zero;
                rectTransform.localScale = tmp;
                return;
            }
            int n = num;
            for (int i = digit - 1; i >= 0; i--)
            {
                if (!fill && i != digit - 1 && n == 0)
                {
                    imageList[i].sprite = null;
                    imageList[i].gameObject.SetActive(false);
                }
                else
                {
                    imageList[i].sprite = spriteList[n % 10];
                    imageList[i].gameObject.SetActive(true);
                }

                n /= 10;
            }
        }

        IEnumerator StartAnimation(int from, int to)
        {
            int add = (int)((to - from) / animationTime * animationInterval);
            int n = from;
            int min = Mathf.Min(from, to);
            int max = Mathf.Max(from, to);
            while (true)
            {
                n = Mathf.Clamp(n + add, min, max);
                if (n == to)
                {
                    break;
                }
                UpdateNum(n);
                yield return new WaitForSeconds(animationInterval);
            }
            UpdateNum(to);
        }

        // 1callで描画するバージョン
        public override Texture mainTexture
        {
            get
            {
                return texture;
            }
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            if (!isAtlas || texture == null || charWidth < 0 || charHeight < 0) return;

            float w = rectTransform.sizeDelta.x;
            float h = rectTransform.sizeDelta.y;
            float x = w * -(rectTransform.pivot.x - 0.5f);
            float y = h * -(rectTransform.pivot.y - 0.5f);

            float uv_w = (float)charWidth / textureWidth;
            float uv_h = (float)charHeight / textureHeight;

            switch (alignment)
            {
                case TextAnchor.LowerLeft:
                case TextAnchor.LowerCenter:
                case TextAnchor.LowerRight:
                    y -= h / 2 - charHeight / 2;
                    break;
                default:
                case TextAnchor.MiddleLeft:
                case TextAnchor.MiddleCenter:
                case TextAnchor.MiddleRight:
                    break;
                case TextAnchor.UpperLeft:
                case TextAnchor.UpperCenter:
                case TextAnchor.UpperRight:
                    y += h / 2 - charHeight / 2;
                    break;
            }

            int n = showNum;
            int d = fill ? digit : Mathf.Min((showNum == 0) ? 1 : ((int)Mathf.Log10(showNum) + 1), digit);
            for (int i = d - 1; i >= 0; i--)
            {
                if (fill || i == d - 1 || n > 0)
                {
                    float n_x = GetN_X(i, d, w);

                    //Debug.Log(string.Format("{0}: {1} {2} {3} {4} {5} {6}", n, x, y, w, h, uv_w, uv_h));
                    vh.AddUIVertexQuad(GetUIVertex(n % 10, x + n_x, y, charWidth, charHeight, uv_w, uv_h, color));
                }

                n /= 10;
            }
        }

        float GetN_X(int i, int d, float w)
        {
            float n_x = (charWidth + spacing) * (i - (float)d / 2 + 0.5f);
            switch (alignment)
            {
                case TextAnchor.LowerLeft:
                case TextAnchor.MiddleLeft:
                case TextAnchor.UpperLeft:
                    n_x -= (w / 2.0f) - (charWidth * d + spacing * (d - 1)) / 2;
                    break;
                default:
                case TextAnchor.LowerCenter:
                case TextAnchor.MiddleCenter:
                case TextAnchor.UpperCenter:
                    break;
                case TextAnchor.LowerRight:
                case TextAnchor.MiddleRight:
                case TextAnchor.UpperRight:
                    n_x += (w / 2.0f) - (charWidth * d + spacing * (d - 1)) / 2;
                    break;
            }

            return n_x;
        }

        UIVertex[] GetUIVertex(int id, float x, float y, float w, float h, float uv_w, float uv_h, Color color)
        {
            if (hNum == 0) return new UIVertex[] { UIVertex.simpleVert, UIVertex.simpleVert, UIVertex.simpleVert, UIVertex.simpleVert };
            float uv_x = (id % hNum) * uv_w;
            float uv_y = 1 - ((id / hNum) * uv_h) - uv_h;

            // 左上
            UIVertex lt = UIVertex.simpleVert;
            lt.color = color;
            lt.uv0 = new Vector2(uv_x, uv_y + uv_h);

            // 右上
            UIVertex rt = UIVertex.simpleVert;
            rt.color = color;
            rt.uv0 = new Vector2(uv_x + uv_w, uv_y + uv_h);

            // 右下
            UIVertex rb = UIVertex.simpleVert;
            rb.color = color;
            rb.uv0 = new Vector2(uv_x + uv_w, uv_y);

            // 左下
            UIVertex lb = UIVertex.simpleVert;
            lb.color = color;
            lb.uv0 = new Vector2(uv_x, uv_y);

            {
                lt.position = new Vector3(x - w / 2, y + h / 2, 0);
                rt.position = new Vector3(x + w / 2, y + h / 2, 0);
                rb.position = new Vector3(x + w / 2, y - h / 2, 0);
                lb.position = new Vector3(x - w / 2, y - h / 2, 0);
            }

            return new UIVertex[] { lb, rb, rt, lt };
        }

        public virtual void CalculateLayoutInputHorizontal() { }
        public virtual void CalculateLayoutInputVertical() { }

        public virtual float minWidth { get { return 0; } }

        public virtual float preferredWidth
        {
            get
            {
                if (texture == null)
                    return 0;
                int n = 0;
                if (fill) n = digit;
                else n = (showNum == 0) ? 1 : ((int)Mathf.Log10(showNum) + 1);
                return charWidth * n + spacing * (n - 1);
            }
        }

        public virtual float flexibleWidth { get { return -1; } }

        public virtual float minHeight { get { return 0; } }

        public virtual float preferredHeight
        {
            get
            {
                if (texture == null)
                    return 0;
                return charHeight;
            }
        }

        public virtual float flexibleHeight { get { return -1; } }

        public virtual int layoutPriority { get { return 0; } }


        #region 拡張コード
#if UNITY_EDITOR
        [CustomEditor(typeof(Number))]
        public class NumberEditor : Editor
        {
            bool foldingSprite = false;

            public override void OnInspectorGUI()
            {
                Number instance = target as Number;

                if (EditorApplication.isPlaying || instance.isAtlas)
                    instance.num = EditorGUILayout.IntField("Num", instance._num);
                else
                    instance._num = EditorGUILayout.IntField("Num", instance._num);
                instance.digit = EditorGUILayout.IntField("Digit", instance.digit);
                instance.fill = EditorGUILayout.Toggle("Fill", instance.fill);
                instance.alignment = (TextAnchor)EditorGUILayout.EnumPopup("Alignment", instance.alignment);
                instance.spacing = EditorGUILayout.FloatField("Spacing", instance.spacing);
                instance.animationTime = EditorGUILayout.FloatField("Animation Time", instance.animationTime);
                instance.animationInterval = EditorGUILayout.FloatField("Animation Interval", instance.animationInterval);

                instance.isAtlas = EditorGUILayout.Toggle("isAtlas", instance.isAtlas);
                if (!instance.isAtlas)
                {
                    if (foldingSprite = EditorGUILayout.Foldout(foldingSprite, "Sprite List"))
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            if (instance.spriteList.Count <= i)
                            {
                                instance.spriteList.Add(null);
                            }
                            instance.spriteList[i] = EditorGUILayout.ObjectField(
                                i.ToString(), instance.spriteList[i],
                                typeof(Sprite), false,
                                GUILayout.Height(16)) as Sprite;
                        }
                    }
                }
                else
                {
                    instance.texture = EditorGUILayout.ObjectField("Texture", instance.texture, typeof(Texture), false, GUILayout.Height(16)) as Texture;
                    instance.SetTextureSize();
                    instance.color = EditorGUILayout.ColorField("Color", instance.color);
                    instance.charWidth = EditorGUILayout.FloatField("Char Width", instance.charWidth);
                    instance.charHeight = EditorGUILayout.FloatField("Char Height", instance.charHeight);
                }
                instance.SetAllDirty();
            }
        }
#endif
        #endregion
    }
}