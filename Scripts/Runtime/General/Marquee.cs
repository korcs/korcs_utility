﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CommonPackage
{
    /// <summary>
    /// 範囲をオーバーしたTextを横スクロール
    /// </summary>
    [RequireComponent(typeof(RectTransform)), RequireComponent(typeof(Mask))]
    public class Marquee : MonoBehaviour
    {
        [SerializeField]
        private Text text = null;
        private Text text2 = null;

        [SerializeField]
        private float scrollSpeed = 20.0f;

        private float width = 0;
        private bool isRun = false;

        void Start()
        {
            width = GetComponent<RectTransform>().rect.width;

            var rect = Instantiate(text.gameObject).GetComponent<RectTransform>();
            rect.SetParent(text.rectTransform);
            rect.localPosition = Vector3.zero;
            rect.anchorMin = new Vector2(1.0f, 0.5f);
            rect.anchorMax = new Vector2(1.0f, 0.5f);
            rect.anchoredPosition = new Vector2(0, 0);
            rect.localScale = Vector3.one;
            text2 = rect.GetComponent<Text>();

            SetText(text.text);
            Run();
        }

        void Update()
        {

        }

        public void SetText(string str)
        {
            text.text = text2.text = str;
        }

        public void Run()
        {
            if (!isRun)
            {
                isRun = true;
                StartCoroutine(Scroll());
            }
        }

        public void Stop()
        {
            isRun = false;
        }

        IEnumerator Scroll()
        {
            RectTransform rect = text.GetComponent<RectTransform>();
            Vector3 pos = rect.anchoredPosition;
            float textWidth;
            while (isRun)
            {
                textWidth = rect.sizeDelta.x;
                if (width < textWidth)
                {
                    // はみ出したらスクロール
                    pos.x -= Time.deltaTime * scrollSpeed;
                    if (pos.x < -textWidth)
                    {
                        pos.x += textWidth;
                    }
                    text2.gameObject.SetActive(true);
                }
                else
                {
                    pos.x = 0;
                    text2.gameObject.SetActive(false);
                }
                rect.anchoredPosition = pos;
                yield return null;
            }
        }
    }
}