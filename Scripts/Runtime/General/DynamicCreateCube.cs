﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CommonPackage
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class DynamicCreateCube : MonoBehaviour
    {
        MeshRenderer _renderer;
        protected MeshRenderer Renderer
        {
            get
            {
                if (_renderer == null)
                {
                    _renderer = GetComponent<MeshRenderer>();
                }
                return _renderer;
            }
        }
        MeshFilter _filter;
		protected MeshFilter Filter
        {
            get
            {
                if (_filter == null)
                {
                    _filter = GetComponent<MeshFilter>();
                }
                return _filter;
            }
        }

#if UNITY_EDITOR
        [SerializeField]
        eType type = eType.NONE;
        [SerializeField]
        Material mat;
        [SerializeField]
        int size = 16;
        [SerializeField]
        int top = 0;
        [SerializeField]
        int side = 0;
        [SerializeField]
        int front = 0;
        [SerializeField]
        int back = 0;
        [SerializeField]
        int steps = 4;
        [SerializeField]
        string meshPath = "";
#endif

        enum eType : int
        {
            NONE = 0,
            CUBE,
            STEP,
            SLOPE,
            RECTANGULAR,
        }

        public void CreateCube(Material mat, int size, int top, int side)
        {
            var mesh = new Mesh();
            mesh.vertices = new Vector3[] {
            new Vector3 (-0.5f, 0.5f, -0.5f),
            new Vector3 (0.5f, 0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),

            new Vector3 (-0.5f, -0.5f, -0.5f),
            new Vector3 (0.5f, -0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, -0.5f),
            new Vector3 (0.5f, 0.5f, -0.5f),

            new Vector3 (0.5f, -0.5f, -0.5f),
            new Vector3 (0.5f, -0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, -0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),

            new Vector3 (0.5f, -0.5f, 0.5f),
            new Vector3 (-0.5f, -0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),

            new Vector3 (-0.5f, -0.5f, 0.5f),
            new Vector3 (-0.5f, -0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),
            new Vector3 (-0.5f, 0.5f, -0.5f),
        };
            mesh.triangles = new int[] {
            0, 2, 1,
            2, 3, 1,

            4, 6, 5,
            6, 7, 5,

            8, 10, 9,
            10, 11, 9,

            12, 14, 13,
            14, 15, 13,

            16, 18, 17,
            18, 19, 17,
        };
            float sx = 0;
            float sy = 0;
            Vector2 topPos = new Vector2();
            Vector2 sidePos = new Vector2();
            var tex = mat.mainTexture;
            if (tex != null)
            {
                int x = tex.width / size;
                int y = tex.height / size;
                sx = (float)size / tex.width;
                sy = (float)size / tex.height;

                topPos.x = (top % x) * sx;
                topPos.y = (y - top / x - 1) * sy;
                sidePos.x = (side % x) * sx;
                sidePos.y = (y - side / x - 1) * sy;
            }
            mesh.uv = new Vector2[] {
            new Vector2 (topPos.x, topPos.y),
            new Vector2 (topPos.x + sx, topPos.y),
            new Vector2 (topPos.x, topPos.y + sy),
            new Vector2 (topPos.x + sx, topPos.y + sy),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy),
            new Vector2 (sidePos.x + sx, sidePos.y + sy),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy),
            new Vector2 (sidePos.x + sx, sidePos.y + sy),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy),
            new Vector2 (sidePos.x + sx, sidePos.y + sy),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy),
            new Vector2 (sidePos.x + sx, sidePos.y + sy),
        };

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            Filter.sharedMesh = mesh;
            Renderer.material = mat;

#if UNITY_EDITOR
            AssetDatabase.CreateAsset(mesh, meshPath);
#endif
        }

        static public DynamicCreateCube CreateCubeObject(Material mat, int size = 16, int top = 0, int side = 0, string name = "DynamicCube")
        {
            var obj = new GameObject(name);
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<MeshFilter>();
            var ret = obj.AddComponent<DynamicCreateCube>();

            ret.CreateCube(mat, size, top, side);

            return ret;
        }

        public void CreateStep(Material mat, int size, int top, int front, int side, int back, int steps)
        {
            var mesh = new Mesh();
            var vert = new List<Vector3>();// (2 * (steps + 1) * 2) + (4 * steps * 2) + 4
            var triangles = new List<int>();// (steps * 2 * 4 + 2) * 3
            var uv = new List<Vector2>();// mesh.vertices.Length
            float sx = 0;
            float sy = 0;
            Vector2 topPos = new Vector2();
            Vector2 sidePos = new Vector2();
            Vector2 frontPos = new Vector2();
            Vector2 backPos = new Vector2();
            var tex = mat.mainTexture;
            if (tex != null)
            {
                int x = tex.width / size;
                int y = tex.height / size;
                sx = (float)size / tex.width;
                sy = (float)size / tex.height;

                topPos.x = (top % x) * sx;
                topPos.y = (y - top / x - 1) * sy;
                sidePos.x = (side % x) * sx;
                sidePos.y = (y - side / x - 1) * sy;
                frontPos.x = (front % x) * sx;
                frontPos.y = (y - front / x - 1) * sy;
                backPos.x = (back % x) * sx;
                backPos.y = (y - back / x - 1) * sy;
            }

            // 背面
            vert.Add(new Vector3(0.5f, -0.5f, 0.5f));
            vert.Add(new Vector3(-0.5f, -0.5f, 0.5f));
            vert.Add(new Vector3(0.5f, 0.5f, 0.5f));
            vert.Add(new Vector3(-0.5f, 0.5f, 0.5f));
            triangles.AddRange(new int[] {
            0, 2, 1,
            2, 3, 1,
        });
            uv.Add(new Vector2(backPos.x, backPos.y));
            uv.Add(new Vector2(backPos.x + sx, backPos.y));
            uv.Add(new Vector2(backPos.x, backPos.y + sy));
            uv.Add(new Vector2(backPos.x + sx, backPos.y + sy));

            // 上面・正面
            for (int i = 0; i < steps; i++)
            {
                int offset = vert.Count;
                float h = (float)(steps - i) / steps - 0.5f;
                float s = 1.0f / steps;
                vert.Add(new Vector3(-0.5f, h, h - s));
                vert.Add(new Vector3(0.5f, h, h - s));
                vert.Add(new Vector3(-0.5f, h, h));
                vert.Add(new Vector3(0.5f, h, h));
                triangles.AddRange(new int[] {
                offset+0, offset+2, offset+1,
                offset+2, offset+3, offset+1,
            });
                float v0 = topPos.y + (float)(steps - i - 1) / steps * sy;
                float v1 = v0 + sy / steps;
                uv.Add(new Vector2(topPos.x, v0));
                uv.Add(new Vector2(topPos.x + sx, v0));
                uv.Add(new Vector2(topPos.x, v1));
                uv.Add(new Vector2(topPos.x + sx, v1));

                offset = vert.Count;
                vert.Add(new Vector3(-0.5f, h - s, h - s));
                vert.Add(new Vector3(0.5f, h - s, h - s));
                vert.Add(new Vector3(-0.5f, h, h - s));
                vert.Add(new Vector3(0.5f, h, h - s));
                triangles.AddRange(new int[] {
                offset+0, offset+2, offset+1,
                offset+2, offset+3, offset+1,
            });
                v0 = frontPos.y + (float)(steps - i - 1) / steps * sy;
                v1 = v0 + sy / steps;
                uv.Add(new Vector2(frontPos.x, v0));
                uv.Add(new Vector2(frontPos.x + sx, v0));
                uv.Add(new Vector2(frontPos.x, v1));
                uv.Add(new Vector2(frontPos.x + sx, v1));
            }

            // 側面
            for (int n = 0; n < 2; n++)
            {
                int origin = vert.Count + 1;
                float x = (n == 0) ? -0.5f : 0.5f;
                for (int i = 0; i < steps; i++)
                {
                    int offset = vert.Count;
                    float h = (float)(steps - i) / steps - 0.5f;
                    float s = 1.0f / steps;
                    if (i == 0)
                    {
                        vert.Add(new Vector3(x, 0.5f, 0.5f));
                        vert.Add(new Vector3(x, -0.5f, 0.5f));
                    }
                    vert.Add(new Vector3(x, h - s, h - s));
                    vert.Add(new Vector3(x, h, h - s));
                    if (i == 0)
                    {
                        offset += 2;
                    }
                    if (n == 0)
                    {
                        triangles.AddRange(new int[] {
                        origin, offset-2, offset+0,
                        offset-2, offset+1, offset+0,
                    });
                    }
                    else
                    {
                        triangles.AddRange(new int[] {
                        origin, offset+0, offset-2,
                        offset-2, offset+0, offset+1,
                    });
                    }

                    if (i == 0)
                    {
                        uv.Add(new Vector2(sidePos.x, sidePos.y + sy));
                        uv.Add(new Vector2(sidePos.x, sidePos.y));
                    }
                    uv.Add(new Vector2(sidePos.x + (i + 1) * sx / steps, sidePos.y + (steps - i - 1) * sy / steps));
                    uv.Add(new Vector2(sidePos.x + (i + 1) * sx / steps, sidePos.y + (steps - i) * sy / steps));
                }
            }

            mesh.vertices = vert.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.uv = uv.ToArray();

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            ;

            Filter.sharedMesh = mesh;
            Renderer.material = mat;

#if UNITY_EDITOR
            AssetDatabase.CreateAsset(mesh, meshPath);
#endif
        }

        static public DynamicCreateCube CreateStepObject(Material mat, int size = 16, int top = 0, int front = 0, int side = 0, int back = 0, int steps = 4, string name = "DynamicStep")
        {
            var obj = new GameObject(name);
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<MeshFilter>();
            var ret = obj.AddComponent<DynamicCreateCube>();

            ret.CreateStep(mat, size, top, front, side, back, steps);

            return ret;
        }

        public void CreateSlope(Material mat, int size, int top, int side, int back)
        {
            var mesh = new Mesh();
            mesh.vertices = new Vector3[] {
            new Vector3 (-0.5f, -0.5f, -0.5f),
            new Vector3 (0.5f, -0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),

            new Vector3 (0.5f, -0.5f, 0.5f),
            new Vector3 (-0.5f, -0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),

            new Vector3 (0.5f, -0.5f, -0.5f),
            new Vector3 (0.5f, -0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),

            new Vector3 (-0.5f, -0.5f, 0.5f),
            new Vector3 (-0.5f, -0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),
        };
            mesh.triangles = new int[] {
            0, 2, 1,
            2, 3, 1,

            4, 6, 5,
            6, 7, 5,

            8, 10, 9,

            11, 13, 12,
        };
            float sx = 0;
            float sy = 0;
            Vector2 topPos = new Vector2();
            Vector2 sidePos = new Vector2();
            Vector2 backPos = new Vector2();
            var tex = mat.mainTexture;
            if (tex != null)
            {
                int x = tex.width / size;
                int y = tex.height / size;
                sx = (float)size / tex.width;
                sy = (float)size / tex.height;

                topPos.x = (top % x) * sx;
                topPos.y = (y - top / x - 1) * sy;
                sidePos.x = (side % x) * sx;
                sidePos.y = (y - side / x - 1) * sy;
                backPos.x = (back % x) * sx;
                backPos.y = (y - back / x - 1) * sy;
            }
            mesh.uv = new Vector2[] {
            new Vector2 (topPos.x, topPos.y),
            new Vector2 (topPos.x + sx, topPos.y),
            new Vector2 (topPos.x, topPos.y + sy),
            new Vector2 (topPos.x + sx, topPos.y + sy),

            new Vector2 (backPos.x, backPos.y),
            new Vector2 (backPos.x + sx, backPos.y),
            new Vector2 (backPos.x, backPos.y + sy),
            new Vector2 (backPos.x + sx, backPos.y + sy),

            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy),
        };

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            Filter.sharedMesh = mesh;
            Renderer.material = mat;

#if UNITY_EDITOR
            AssetDatabase.CreateAsset(mesh, meshPath);
#endif
        }

        static public DynamicCreateCube CreateSlopeObject(Material mat, int size = 16, int top = 0, int side = 0, int back = 0, string name = "DynamicSlope")
        {
            var obj = new GameObject(name);
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<MeshFilter>();
            var ret = obj.AddComponent<DynamicCreateCube>();

            ret.CreateSlope(mat, size, top, side, back);

            return ret;
        }

        public void CreateRectangular(Material mat, int size, int top, int side, float height, float offset)
        {
            var mesh = new Mesh();
            {
                float x = 0.5f, y = height / 2, z = 0.5f;
                mesh.vertices = new Vector3[] {
                new Vector3 (-x, y, -z),
                new Vector3 (x, y, -z),
                new Vector3 (-x, y, z),
                new Vector3 (x, y, z),

                new Vector3 (-x, -y, -z),
                new Vector3 (x, -y, -z),
                new Vector3 (-x, y, -z),
                new Vector3 (x, y, -z),

                new Vector3 (x, -y, -z),
                new Vector3 (x, -y, z),
                new Vector3 (x, y, -z),
                new Vector3 (x, y, z),

                new Vector3 (x, -y, z),
                new Vector3 (-x, -y, z),
                new Vector3 (x, y, z),
                new Vector3 (-x, y, z),

                new Vector3 (-x, -y, z),
                new Vector3 (-x, -y, -z),
                new Vector3 (-x, y, z),
                new Vector3 (-x, y, -z),
            };
            }
            mesh.triangles = new int[] {
            0, 2, 1,
            2, 3, 1,

            4, 6, 5,
            6, 7, 5,

            8, 10, 9,
            10, 11, 9,

            12, 14, 13,
            14, 15, 13,

            16, 18, 17,
            18, 19, 17,
        };
            float sx = 0;
            float sy = 0;
            float sy2 = 0;
            Vector2 topPos = new Vector2();
            Vector2 sidePos = new Vector2();
            var tex = mat.mainTexture;
            if (tex != null)
            {
                int x = tex.width / size;
                int y = tex.height / size;
                sx = (float)size / tex.width;
                sy = (float)size / tex.height;
                sy2 = sy * height;

                topPos.x = (top % x) * sx;
                topPos.y = (y - top / x - 1) * sy;
                sidePos.x = (side % x) * sx;
                sidePos.y = (y - side / x) * sy - sy2 + sy * offset;
            }
            mesh.uv = new Vector2[] {
            new Vector2 (topPos.x, topPos.y),
            new Vector2 (topPos.x + sx, topPos.y),
            new Vector2 (topPos.x, topPos.y + sy),
            new Vector2 (topPos.x + sx, topPos.y + sy),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy2),
            new Vector2 (sidePos.x + sx, sidePos.y + sy2),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy2),
            new Vector2 (sidePos.x + sx, sidePos.y + sy2),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy2),
            new Vector2 (sidePos.x + sx, sidePos.y + sy2),

            new Vector2 (sidePos.x, sidePos.y),
            new Vector2 (sidePos.x + sx, sidePos.y),
            new Vector2 (sidePos.x, sidePos.y + sy2),
            new Vector2 (sidePos.x + sx, sidePos.y + sy2),
        };
            mesh.uv2 = new Vector2[] {
            new Vector2 (0, 0),
            new Vector2 (1, 0),
            new Vector2 (0, 1),
            new Vector2 (1, 1),

            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),

            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),

            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),

            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
            new Vector2 (0.5f, 0.5f),
        };

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            Filter.sharedMesh = mesh;
            Renderer.material = mat;

#if UNITY_EDITOR
            AssetDatabase.CreateAsset(mesh, meshPath);
#endif
        }

        static public DynamicCreateCube CreateRectangularObject(Material mat, int size = 16, int top = 0, int side = 0, float height = 1.0f, float offset = 0, string name = "DynamicCube")
        {
            var obj = new GameObject(name);
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<MeshFilter>();
            var ret = obj.AddComponent<DynamicCreateCube>();

            ret.CreateRectangular(mat, size, top, side, height, offset);

            return ret;
        }

        static public DynamicCreateCube BatchDynamicCreateCubes(List<DynamicCreateCube> cubes, bool isColider = true, string name = "BatchedCubes")
        {
            var obj = new GameObject(name);
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<MeshFilter>();
            var ret = obj.AddComponent<DynamicCreateCube>();

            if (cubes.Count > 0)
            {
                var mat = cubes[0].Renderer.material;
                var mesh = new Mesh();

                var vertices = new List<Vector3>();
                var triangles = new List<int>();
                var uv = new List<Vector2>();
                var uv2 = new List<Vector2>();

                foreach (var cube in cubes)
                {
                    int cnt = vertices.Count;
                    foreach (var vert in cube.Filter.sharedMesh.vertices)
                    {
                        Vector3 pos = cube.transform.localRotation * vert + cube.transform.localPosition;
                        vertices.Add(pos);
                    }
                    foreach (var triangle in cube.Filter.sharedMesh.triangles)
                    {
                        int n = cnt + triangle;
                        triangles.Add(n);
                    }
                    uv.AddRange(cube.Filter.sharedMesh.uv);
                    uv2.AddRange(cube.Filter.sharedMesh.uv2);
                }

                mesh.vertices = vertices.ToArray();
                mesh.triangles = triangles.ToArray();
                mesh.uv = uv.ToArray();
                mesh.uv2 = uv2.ToArray();

                mesh.RecalculateNormals();
                mesh.RecalculateBounds();

                ret.Filter.sharedMesh = mesh;
                ret.Renderer.material = mat;
                if (isColider)
                {
                    var colider = obj.AddComponent<MeshCollider>();
                    colider.sharedMesh = mesh;
                }
            }


            return ret;
        }

        #region 拡張コード
#if UNITY_EDITOR
        [CustomEditor(typeof(DynamicCreateCube))]
        public class DynamicCreateCubeEditor : Editor
        {

            public override void OnInspectorGUI()
            {
                DynamicCreateCube instance = target as DynamicCreateCube;

                instance.type = (eType)EditorGUILayout.EnumPopup("Type", instance.type);

                if (instance.type != eType.NONE)
                {
                    GUILayout.BeginHorizontal();
                    //GUILayout.Label(instance.meshPath);
                    instance.meshPath = EditorGUILayout.TextField("Path", instance.meshPath);
                    if (GUILayout.Button("...", GUILayout.Width(32)))
                    {
                        instance.meshPath = EditorUtility.SaveFilePanel("Mesh Path", instance.meshPath, "CustomMesh", "asset");
                        instance.meshPath = "Assets" + instance.meshPath.Replace(Application.dataPath, "");
                    }
                    GUILayout.EndHorizontal();
                    instance.mat = (Material)EditorGUILayout.ObjectField("Material", instance.mat, typeof(Material), false);
                    instance.size = EditorGUILayout.IntField("Grid Size", instance.size);
                }
                if (instance.type == eType.CUBE)
                {
                    instance.top = EditorGUILayout.IntField("Top Index", instance.top);
                    instance.side = EditorGUILayout.IntField("Side Index", instance.side);

                    if (GUILayout.Button("Create Mesh (cube)"))
                    {
                        instance.CreateCube(instance.mat, instance.size, instance.top, instance.side);
                    }
                }
                else if (instance.type == eType.STEP)
                {
                    instance.top = EditorGUILayout.IntField("Top Index", instance.top);
                    instance.front = EditorGUILayout.IntField("Front Index", instance.front);
                    instance.side = EditorGUILayout.IntField("Side Index", instance.side);
                    instance.back = EditorGUILayout.IntField("Back Index", instance.back);
                    instance.steps = EditorGUILayout.IntField("Steps Num", instance.steps);

                    if (GUILayout.Button("Create Mesh (step)"))
                    {
                        instance.CreateStep(instance.mat, instance.size, instance.top, instance.front, instance.side, instance.back, instance.steps);
                    }
                }
                else if (instance.type == eType.SLOPE)
                {
                    instance.top = EditorGUILayout.IntField("Top Index", instance.top);
                    instance.side = EditorGUILayout.IntField("Side Index", instance.side);
                    instance.back = EditorGUILayout.IntField("Back Index", instance.back);

                    if (GUILayout.Button("Create Mesh (slope)"))
                    {
                        instance.CreateSlope(instance.mat, instance.size, instance.top, instance.side, instance.back);
                    }
                }
                else if (instance.type == eType.RECTANGULAR)
                {
                    instance.top = EditorGUILayout.IntField("Top Index", instance.top);
                    instance.side = EditorGUILayout.IntField("Side Index", instance.side);
                    instance.steps = EditorGUILayout.IntField("Height Per", instance.steps);
                    instance.back = EditorGUILayout.IntField("Offset Y", instance.back);

                    if (GUILayout.Button("Create Mesh (rectangular)"))
                    {
                        instance.CreateRectangular(instance.mat, instance.size, instance.top, instance.side, 1.0f / instance.steps, -1.0f / instance.steps * instance.back);
                    }
                }
            }
        }
#endif
        #endregion
    }
}