﻿using UnityEngine;
using System.Collections;

namespace CommonPackage
{
    /// <summary>
    /// シングルトンな基底クラス
    /// </summary>
    /// <typeparam name="T">型</typeparam>
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
    {
        [SerializeField]
        protected bool dontDestroy = false;

        protected static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if (instance == null)
                    {
                        //Debug.LogWarning(typeof(T) + " is nothing");
                    }
                }

                return instance;
            }
        }

        protected void Awake()
        {
            CheckInstance();
        }

        protected bool CheckInstance()
        {
            if (instance == null)
            {
                instance = (T)this;
                if (dontDestroy)
                {
                    DontDestroyOnLoad(gameObject);
                }
                return true;
            }
            else if (Instance == this)
            {
                return true;
            }

            Destroy(gameObject);
            return false;
        }
    }
}