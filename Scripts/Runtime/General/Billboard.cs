﻿using UnityEngine;
using System.Collections;

namespace CommonPackage
{
    public class Billboard : MonoBehaviour
    {
        private Transform cameraTransform;
        private bool isOrthographic = true;

        void Start()
        {
            cameraTransform = Camera.main.transform;
            isOrthographic = Camera.main.orthographic;
        }

        void LateUpdate()
        {
            // カメラの方向へ回転
            if (isOrthographic)
            {
                transform.rotation = cameraTransform.rotation;
            }
            else
            {
                this.transform.LookAt(cameraTransform.position);
                transform.Rotate(0, 180, 0, Space.Self);
            }
        }
    }
}