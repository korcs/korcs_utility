﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

namespace CommonPackage
{
    /// <summary>
    /// 絵文字付きText
    /// </summary>
    public class PictoText : MonoBehaviour
    {
        [Multiline]
        public string text = "";
        public Font font;
        public int fontSize = 16;
        public TextAnchor alignment = TextAnchor.MiddleLeft;
        public Color color = Color.white;
        public Texture2D pictographTexture;
        public int pictographWidth = 16;
        public int pictographHeight = 16;
        public int pixelsPerUnit = 100;

        private int pictographXnum = 1;
        private int pictographYnum = 1;

        private List<GameObject> lines = new List<GameObject>();

        protected void Start()
        {
            var verticalLayout = gameObject.AddComponent<VerticalLayoutGroup>();
            verticalLayout.childForceExpandHeight = false;
            verticalLayout.childForceExpandWidth = false;
            verticalLayout.childAlignment = alignment;

            pictographXnum = pictographTexture.width / pictographWidth;
            pictographYnum = pictographTexture.height / pictographHeight;

            if (this.text != null)
            {
                SetText(this.text);
            }
        }

        // テキストをセット（'[n]'で絵文字指定）
        public void SetText(string text)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            lines.Clear();
            this.text = text;

            string[] textLines = text.Split('\n');

            for (int i = 0; i < textLines.Length; i++)
            {
                var obj = new GameObject(textLines[i]);
                obj.transform.SetParent(transform);
                var horizontalLayout = obj.AddComponent<HorizontalLayoutGroup>();
                horizontalLayout.childForceExpandHeight = false;
                horizontalLayout.childForceExpandWidth = false;
                horizontalLayout.childAlignment = alignment;
                obj.GetComponent<RectTransform>().localScale = Vector3.one;
                lines.Add(obj);

                string tmp = "";
                var mc = Regex.Matches(textLines[i], @"(?<=\[)\d+(?=\])");
                for (int j = 0; j < textLines[i].Length; j++)
                {
                    int index = 0;
                    int length = 0;
                    foreach (Match m in mc)
                    {
                        if (m.Index - 1 == j)
                        {
                            index = int.Parse(m.Value);
                            length = m.Length + 1;
                            break;
                        }
                    }
                    if (length > 0)
                    {
                        AddText(tmp, i);
                        tmp = "";
                        AddImage(index, i);
                        j += length;
                    }
                    else
                    {
                        tmp += textLines[i].Substring(j, 1);
                    }
                }
                AddText(tmp, i);
            }
        }

        // 文字列追加
        void AddText(string str, int line)
        {
            if (str.Equals("")) return;

            var obj = new GameObject(str);
            obj.transform.SetParent(lines[line].transform);
            var text = obj.AddComponent<Text>();
            text.text = str;
            text.font = this.font;
            text.fontSize = this.fontSize;
            text.color = this.color;
            text.rectTransform.localScale = Vector3.one;
            text.raycastTarget = false;
        }

        // 絵文字追加
        void AddImage(int index, int line)
        {
            //if (index >= pictographs.Count) return;

            var obj = new GameObject(String.Format("image[{0}]", index));
            obj.transform.SetParent(lines[line].transform);
            var image = obj.AddComponent<Image>();
            //image.sprite = pictographs[index];
            image.sprite = Sprite.Create(pictographTexture,
                new Rect((index % pictographXnum) * pictographWidth,
                            (pictographYnum - index / pictographXnum - 1) * pictographHeight,
                            pictographWidth,
                            pictographHeight),
                new Vector2(0.5f, 0.5f), pixelsPerUnit);
            image.rectTransform.localScale = Vector3.one;
            image.raycastTarget = false;
        }
    }
}