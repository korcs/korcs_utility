﻿#if UNITY_SWITCH && !UNITY_EDITOR
#define NPAD
#else
#define USE_INPUT_SYSTEM
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if USE_INPUT_SYSTEM
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
#endif
#if NPAD
using nn.hid;
#endif

namespace CommonPackage
{
    public class InputEx
    {
#if NPAD
        static Dictionary<eButton, nn.hid.NpadButton> joyList = new Dictionary<eButton, nn.hid.NpadButton>();

        static NpadId npadId = NpadId.Invalid;
        static NpadStyle npadStyle = NpadStyle.Invalid;
        static NpadState npadState = new NpadState();
#elif USE_INPUT_SYSTEM
        static Dictionary<eButton, GamepadButton> joyList = new Dictionary<eButton, GamepadButton>();
        static Dictionary<eButton, Key> keyList = new Dictionary<eButton, Key>();
#else
        static Dictionary<eButton, KeyCode> joyList = new Dictionary<eButton, KeyCode>();
        static Dictionary<eButton, KeyCode> keyList = new Dictionary<eButton, KeyCode>();
        static Dictionary<eButton, Axis> axisList = new Dictionary<eButton, Axis>();
#endif

        static List<float> repeatTime = new List<float>();
        static List<float> pressTime = new List<float>();

        public enum eButton : int
        {
            D_U = 0,
            D_R,
            D_D,
            D_L,
            A,
            B,
            X,
            Y,
            L1,
            R1,
            L2,
            R2,
            START,
            SELECT,
            OK,
            CANCEL,
            MAX,
        };

        const float REP_FIRST = 0.2f;
        const float REP = 0.12f;
        const float LONG = 1.2f;
        const float AXIS_THRESHOLD = 0.25f;

        struct Axis
        {
            public string Name;
            public bool IsPositive;
            public Axis(string name, bool isPositive)
            {
                Name = name;
                IsPositive = isPositive;
            }
        }

#if NPAD
        public static void SetJoy(eButton btn, nn.hid.NpadButton name)
        {
            if (joyList.ContainsKey(btn))
            {
                joyList.Remove(btn);
            }
            joyList.Add(btn, name);
        }

        public static nn.hid.NpadButton GetJoy(eButton btn)
        {
            if (joyList.ContainsKey(btn))
            {
                return joyList[btn];
            }
            else
            {
                return nn.hid.NpadButton.None;
            }
        }
#elif USE_INPUT_SYSTEM
        public static void SetJoy(eButton btn, GamepadButton name)
        {
            if (joyList.ContainsKey(btn))
            {
                joyList.Remove(btn);
            }
            joyList.Add(btn, name);
        }

        public static GamepadButton GetJoy(eButton btn)
        {
            if (joyList.ContainsKey(btn))
            {
                return joyList[btn];
            }
            else
            {
                return GamepadButton.Start;
            }
        }

        public static void SetKey(eButton btn, Key name)
        {
            if (keyList.ContainsKey(btn))
            {
                keyList.Remove(btn);
            }
            keyList.Add(btn, name);
        }

        public static Key GetKey(eButton btn)
        {
            if (keyList.ContainsKey(btn))
            {
                return keyList[btn];
            }
            else
            {
                return Key.None;
            }
        }
#else
        public static void SetJoy(eButton btn, KeyCode name)
        {
            // 設定したボタンが他のボタンと被っていたら交換
            //foreach (var l in joyList) {
            //	if(l.Key != btn && l.Value == name && IsSameType(l.Key, btn) && name != KeyCode.None && l.Value != KeyCode.None && joyList.ContainsKey(btn)) {
            //		joyList[l.Key] = joyList[btn];
            //	}
            //}
            if (joyList.ContainsKey(btn))
            {
                joyList.Remove(btn);
            }
            joyList.Add(btn, name);
        }

        public static KeyCode GetJoy(eButton btn)
        {
            if (joyList.ContainsKey(btn))
            {
                return joyList[btn];
            }
            else
            {
                return KeyCode.None;
            }
        }

        public static void SetKey(eButton btn, KeyCode name)
        {
            // 設定したボタンが他のボタンと被っていたら交換
            //foreach (var l in keyList) {
            //	if(l.Value == name && IsSameType(l.Key, btn)) {
            //		keyList[l.Key] = keyList[btn];
            //	}
            //}
            if (keyList.ContainsKey(btn))
            {
                keyList.Remove(btn);
            }
            keyList.Add(btn, name);
        }

        public static KeyCode GetKey(eButton btn)
        {
            if (keyList.ContainsKey(btn))
            {
                return keyList[btn];
            }
            else
            {
                return KeyCode.None;
            }
        }

        public static void SetAxis(eButton btn, string name, bool isPositive)
        {
            Axis axis = new Axis(name, isPositive);
            if (axisList.ContainsKey(btn))
            {
                axisList.Remove(btn);
            }
            axisList.Add(btn, axis);
        }

        public static void SetAxis(eButton btn, int num, bool isPositive)
        {
            SetAxis(btn, string.Format("Axis{0}", num), isPositive);
        }

        private static bool IsSameType(eButton btn1, eButton btn2)
        {
            if (btn1 < eButton.OK && btn2 < eButton.OK)
                return true;
            else if (btn1 >= eButton.OK && btn2 >= eButton.OK)
                return true;
            else
                return false;
        }
#endif



        public static float GetAxisHorizontal()
        {
#if NPAD
            return npadState.analogStickL.fx;
#elif USE_INPUT_SYSTEM
            return Gamepad.current.leftStick.ReadValue().x;
#else
            return Input.GetAxis("Horizontal");
#endif
        }

        public static float GetAxisVertical()
        {
#if NPAD
            return npadState.analogStickL.fy;
#elif USE_INPUT_SYSTEM
            return Gamepad.current.leftStick.ReadValue().y;
#else
            return Input.GetAxis("Vertical");
#endif
        }

        public static float GetAxisRawHorizontal()
        {
#if NPAD
            return npadState.analogStickL.fx;
#elif USE_INPUT_SYSTEM
            return Gamepad.current.leftStick.ReadUnprocessedValue().x;
#else
            return Input.GetAxisRaw("Horizontal");
#endif
        }

        public static float GetAxisRawVertical()
        {
#if NPAD
            return npadState.analogStickL.fy;
#elif USE_INPUT_SYSTEM
            return Gamepad.current.leftStick.ReadUnprocessedValue().y;
#else
            return Input.GetAxisRaw("Vertical");
#endif
        }

        static float GetAxis(eButton btn)
        {
#if NPAD
            if ((joyList.ContainsKey(btn) && npadState.GetButton(joyList[btn])))
#elif USE_INPUT_SYSTEM
            if ((joyList.ContainsKey(btn) && Gamepad.current != null && Gamepad.current[joyList[btn]].isPressed) ||
                (keyList.ContainsKey(btn) && Keyboard.current[keyList[btn]].isPressed))
#else
            if (axisList.ContainsKey(btn) && axisList[btn].Name.Length > 0)
            {
                float axis = Input.GetAxis(axisList[btn].Name);
                if (axisList[btn].IsPositive)
                {
                    if (axis > AXIS_THRESHOLD) return axis;
                }
                else
                {
                    if (axis < -AXIS_THRESHOLD) return -axis;
                }
            }
            if ((joyList.ContainsKey(btn) && Input.GetKey(joyList[btn])) ||
                (keyList.ContainsKey(btn) && Input.GetKey(keyList[btn])))
#endif
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static Vector2 GetAxis(bool withDPad = true)
        {
#if NPAD
            return new Vector2(GetAxis(eButton.D_R) - GetAxis(eButton.D_L), GetAxis(eButton.D_U) - GetAxis(eButton.D_D));
#elif USE_INPUT_SYSTEM
            if (withDPad)
            {
                var ret = new Vector2(GetAxis(eButton.D_R) - GetAxis(eButton.D_L), GetAxis(eButton.D_U) - GetAxis(eButton.D_D));
                if (Gamepad.current != null)
                {
                    return Gamepad.current.leftStick.ReadValue() + ret;
                }
                else
                {
                    return ret;
                }
            }
            else if (Gamepad.current != null)
            {
                return Gamepad.current.leftStick.ReadValue();
            }
            return Vector2.zero;
#else
            return new Vector2(GetAxis(eButton.D_R) - GetAxis(eButton.D_L), GetAxis(eButton.D_U) - GetAxis(eButton.D_D));
#endif
        }

        public static bool GetButton(eButton btn)
        {
#if NPAD
            return joyList.ContainsKey(btn) && npadState.GetButton(joyList[btn]);
#elif USE_INPUT_SYSTEM
            return (joyList.ContainsKey(btn) && Gamepad.current != null && Gamepad.current[joyList[btn]].isPressed) ||
                (keyList.ContainsKey(btn) && Keyboard.current[keyList[btn]].isPressed);
#else
            if (axisList.ContainsKey(btn) && axisList[btn].Name.Length > 0)
            {
                if (axisList[btn].IsPositive)
                {
                    if (Input.GetAxisRaw(axisList[btn].Name) >= 1) return true;
                }
                else
                {
                    if (Input.GetAxisRaw(axisList[btn].Name) <= -1) return true;
                }
            }
            return (joyList.ContainsKey(btn) && Input.GetKey(joyList[btn])) |
                    (keyList.ContainsKey(btn) && Input.GetKey(keyList[btn]));
#endif
        }

        public static bool GetButtonDown(eButton btn)
        {
#if NPAD
            return joyList.ContainsKey(btn) && npadState.GetButtonDown(joyList[btn]);
#elif USE_INPUT_SYSTEM
            return (joyList.ContainsKey(btn) && Gamepad.current != null && Gamepad.current[joyList[btn]].wasPressedThisFrame) ||
                (keyList.ContainsKey(btn) && Keyboard.current[keyList[btn]].wasPressedThisFrame);
#else
            if (axisList.ContainsKey(btn) && axisList[btn].Name.Length > 0)
            {
                bool ret = false;
                if (GetButton(btn))
                {
                    float t = repeatTime[(int)btn];
                    if (t == 0) ret = true;
                    else if (t > REP_FIRST + REP)
                    {
                        t -= REP;
                    }
                    repeatTime[(int)btn] = t + Time.deltaTime;
                }
                else
                {
                    repeatTime[(int)btn] = 0;
                }
                return ret;
            }
            return (joyList.ContainsKey(btn) && Input.GetKeyDown(joyList[btn])) |
                    (keyList.ContainsKey(btn) && Input.GetKeyDown(keyList[btn]));
#endif
        }

        public static bool GetButtonUp(eButton btn)
        {
#if NPAD
            return joyList.ContainsKey(btn) && npadState.GetButtonUp(joyList[btn]);
#elif USE_INPUT_SYSTEM
            return (joyList.ContainsKey(btn) && Gamepad.current != null && Gamepad.current[joyList[btn]].wasReleasedThisFrame) ||
                (keyList.ContainsKey(btn) && Keyboard.current[keyList[btn]].wasReleasedThisFrame);
#else
            if (axisList.ContainsKey(btn) && axisList[btn].Name.Length > 0)
            {
                bool ret = false;
                if (GetButton(btn))
                {
                    float t = repeatTime[(int)btn];
                    if (t > REP_FIRST + REP)
                    {
                        t -= REP;
                    }
                    repeatTime[(int)btn] = t + Time.deltaTime;
                }
                else if (repeatTime[(int)btn] > 0)
                {
                    repeatTime[(int)btn] = 0;
                    ret = true;
                }
                return ret;
            }
            return (joyList.ContainsKey(btn) && Input.GetKeyUp(joyList[btn])) |
                    (keyList.ContainsKey(btn) && Input.GetKeyUp(keyList[btn]));
#endif
        }

        public static bool GetButtonRepeat(eButton btn)
        {
            bool ret = false;
            if (GetButton(btn))
            {
                float t = repeatTime[(int)btn];
                if (t == 0) ret = true;
                else if (t > REP_FIRST + REP)
                {
                    t -= REP;
                    ret = true;
                }
                repeatTime[(int)btn] = t + Time.deltaTime;
            }
            else
            {
                repeatTime[(int)btn] = 0;
            }
            return ret;
        }

        public static bool GetButtonLong(eButton btn)
        {
            bool ret = false;
            if (GetButton(btn))
            {
                float t = pressTime[(int)btn];
                if (t >= 0)
                {
                    if (t > LONG)
                    {
                        t = -1;
                        ret = true;
                    }
                    pressTime[(int)btn] = t + Time.deltaTime;
                }
            }
            else
            {
                pressTime[(int)btn] = 0;
            }
            return ret;
        }

        public static void ResetButtonLong()
        {
            for (var b = (eButton)0; b < eButton.MAX; b++)
            {
                repeatTime[(int)b] = 0;
                pressTime[(int)b] = 0;
            }
        }

        public static float GetButtonLongProgress(eButton btn)
        {
            float ret = 0;
            if (GetButton(btn))
            {
                float t = pressTime[(int)btn];
                if (t >= 0)
                {
                    ret = Mathf.Clamp01(pressTime[(int)btn] / LONG);
                }
            }
            else
            {
                ret = 0;
            }
            return ret;
        }

        [RuntimeInitializeOnLoadMethod]
        public static void Init()
        {
#if NPAD
            Npad.Initialize();
            Npad.SetSupportedIdType(new NpadId[] { NpadId.Handheld, NpadId.No1 });
            Npad.SetSupportedStyleSet(NpadStyle.FullKey | NpadStyle.Handheld | NpadStyle.JoyDual);

            SetJoy(eButton.D_U, NpadButton.Up);
            SetJoy(eButton.D_D, NpadButton.Down);
            SetJoy(eButton.D_L, NpadButton.Left);
            SetJoy(eButton.D_R, NpadButton.Right);
            SetJoy(eButton.OK, NpadButton.A);
            SetJoy(eButton.CANCEL, NpadButton.B);
            SetJoy(eButton.A, NpadButton.A);
            SetJoy(eButton.B, NpadButton.B);
            SetJoy(eButton.X, NpadButton.X);
            SetJoy(eButton.Y, NpadButton.Y);
            SetJoy(eButton.L2, NpadButton.ZL);
            SetJoy(eButton.R2, NpadButton.ZR);
            SetJoy(eButton.L1, NpadButton.L);
            SetJoy(eButton.R1, NpadButton.R);
            SetJoy(eButton.START, NpadButton.Plus);
            SetJoy(eButton.SELECT, NpadButton.Minus);
#elif USE_INPUT_SYSTEM
            SetJoy(eButton.D_U, GamepadButton.DpadUp);
            SetJoy(eButton.D_D, GamepadButton.DpadDown);
            SetJoy(eButton.D_L, GamepadButton.DpadLeft);
            SetJoy(eButton.D_R, GamepadButton.DpadRight);
            SetJoy(eButton.OK, GamepadButton.A);
            SetJoy(eButton.CANCEL, GamepadButton.B);
            SetJoy(eButton.A, GamepadButton.B);
            SetJoy(eButton.B, GamepadButton.A);
            SetJoy(eButton.X, GamepadButton.Y);
            SetJoy(eButton.Y, GamepadButton.X);
            SetJoy(eButton.L2, GamepadButton.LeftShoulder);
            SetJoy(eButton.R2, GamepadButton.RightShoulder);
            SetJoy(eButton.L1, GamepadButton.LeftTrigger);
            SetJoy(eButton.R1, GamepadButton.RightTrigger);
            SetJoy(eButton.START, GamepadButton.Start);
            SetJoy(eButton.SELECT, GamepadButton.Select);

            SetKey(eButton.D_U, Key.UpArrow);
            SetKey(eButton.D_D, Key.DownArrow);
            SetKey(eButton.D_L, Key.LeftArrow);
            SetKey(eButton.D_R, Key.RightArrow);
            SetKey(eButton.OK, Key.Z);
            SetKey(eButton.CANCEL, Key.X);
            SetKey(eButton.A, Key.Z);
            SetKey(eButton.B, Key.X);
            SetKey(eButton.X, Key.C);
            SetKey(eButton.Y, Key.V);
            SetKey(eButton.L2, Key.Q);
            SetKey(eButton.R2, Key.W);
            SetKey(eButton.L1, Key.A);
            SetKey(eButton.R1, Key.S);
            SetKey(eButton.START, Key.Space);
            SetKey(eButton.SELECT, Key.Enter);
#else
            SetAxis(eButton.D_U, "Vertical", true);
            SetAxis(eButton.D_D, "Vertical", false);
            SetAxis(eButton.D_L, "Horizontal", false);
            SetAxis(eButton.D_R, "Horizontal", true);
            SetJoy(eButton.OK, KeyCode.JoystickButton0);
            SetJoy(eButton.CANCEL, KeyCode.JoystickButton1);
            SetJoy(eButton.A, KeyCode.JoystickButton0);
            SetJoy(eButton.B, KeyCode.JoystickButton1);
            SetJoy(eButton.X, KeyCode.JoystickButton2);
            SetJoy(eButton.Y, KeyCode.JoystickButton3);
            SetJoy(eButton.L2, KeyCode.JoystickButton6);
            SetJoy(eButton.R2, KeyCode.JoystickButton7);
            SetAxis(eButton.L2, "Axis3", false);
            SetAxis(eButton.R2, "Axis3", true);
            SetJoy(eButton.L1, KeyCode.JoystickButton4);
            SetJoy(eButton.R1, KeyCode.JoystickButton5);
            SetJoy(eButton.START, KeyCode.Joystick8Button8);
            SetJoy(eButton.SELECT, KeyCode.JoystickButton9);

            SetKey(eButton.D_U, KeyCode.UpArrow);
            SetKey(eButton.D_D, KeyCode.DownArrow);
            SetKey(eButton.D_L, KeyCode.LeftArrow);
            SetKey(eButton.D_R, KeyCode.RightArrow);
            SetKey(eButton.OK, KeyCode.Z);
            SetKey(eButton.CANCEL, KeyCode.X);
            SetKey(eButton.A, KeyCode.Z);
            SetKey(eButton.B, KeyCode.X);
            SetKey(eButton.X, KeyCode.C);
            SetKey(eButton.Y, KeyCode.V);
            SetKey(eButton.L2, KeyCode.Q);
            SetKey(eButton.R2, KeyCode.W);
            SetKey(eButton.L1, KeyCode.A);
            SetKey(eButton.R1, KeyCode.S);
            SetKey(eButton.START, KeyCode.Space);
            SetKey(eButton.SELECT, KeyCode.Return);
#endif

            for (var b = (eButton)0; b < eButton.MAX; b++)
            {
                repeatTime.Add(0);
                pressTime.Add(0);
            }
        }

        public static void Set<Content>(List<ConfigSaveData.Key> keyConfig)
            where Content : ISerializationCallbackReceiver, new()
        {
            for (int i = 0; i < keyConfig.Count; i++)
            {
#if NPAD
                if (keyConfig[i].key > 0)
                {
                    SetJoy((eButton)i, (NpadButton)keyConfig[i].key);
                }
                else
                {
                    SetJoy((eButton)i, NpadButton.None);
                }
#elif USE_INPUT_SYSTEM
                SetJoy((eButton)i, (GamepadButton)keyConfig[i].key);
#else
                if (keyConfig[i].key > 0)
                {
                    SetJoy((eButton)i, (KeyCode)keyConfig[i].key);
                    SetAxis((eButton)i, "", keyConfig[i].positive);
                }
                else
                {
                    SetJoy((eButton)i, KeyCode.None);
                    SetAxis((eButton)i, keyConfig[i].axis, keyConfig[i].positive);
                }
#endif
            }
        }

        public static KeyCode GetInputKeycode(bool isJoystick = false)
        {
            if (isJoystick)
            {
                for (KeyCode i = KeyCode.JoystickButton0; i <= KeyCode.JoystickButton19; i++)
                {
                    if (Input.GetKey(i))
                    {
                        return i;
                    }
                }
            }
            else
            {
                for (KeyCode i = KeyCode.Backspace; i <= KeyCode.Z; i++)
                {
                    if (Input.GetKey(i))
                    {
                        return i;
                    }
                }
            }
            return KeyCode.None;
        }

        public static ConfigSaveData.Key GetInputAxis<Content>()
            where Content : ISerializationCallbackReceiver, new()
        {
            ConfigSaveData.Key key = new ConfigSaveData.Key();
            for (int i = 0; i < 16; i++)
            {
                float ret = Input.GetAxisRaw(string.Format("Axis{0}", i + 1));
                if (ret >= 1)
                {
                    key.axis = i + 1;
                    key.positive = true;
                    return key;
                }
                else if (ret <= -1)
                {
                    key.axis = i + 1;
                    key.positive = false;
                    return key;
                }
            }
            return null;
        }

        public static bool UpdatePadState()
        {
#if NPAD
            NpadStyle handheldStyle = Npad.GetStyleSet(NpadId.Handheld);
            NpadState handheldState = npadState;
            if (handheldStyle != NpadStyle.None)
            {
                Npad.GetState(ref handheldState, NpadId.Handheld, handheldStyle);
                if (handheldState.buttons != NpadButton.None)
                {
                    npadId = NpadId.Handheld;
                    npadStyle = handheldStyle;
                    npadState = handheldState;
                    return true;
                }
            }

            NpadStyle no1Style = Npad.GetStyleSet(NpadId.No1);
            NpadState no1State = npadState;
            if (no1Style != NpadStyle.None)
            {
                Npad.GetState(ref no1State, NpadId.No1, no1Style);
                if (no1State.buttons != NpadButton.None)
                {
                    npadId = NpadId.No1;
                    npadStyle = no1Style;
                    npadState = no1State;
                    return true;
                }
            }

            if ((npadId == NpadId.Handheld) && (handheldStyle != NpadStyle.None))
            {
                npadId = NpadId.Handheld;
                npadStyle = handheldStyle;
                npadState = handheldState;
            }
            else if ((npadId == NpadId.No1) && (no1Style != NpadStyle.None))
            {
                npadId = NpadId.No1;
                npadStyle = no1Style;
                npadState = no1State;
            }
            else
            {
                npadId = NpadId.Invalid;
                npadStyle = NpadStyle.Invalid;
                npadState.Clear();
                return false;
            }
            return true;
#else
            return true;
#endif
        }

    }
}