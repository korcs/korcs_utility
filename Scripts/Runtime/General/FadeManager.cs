﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace CommonPackage
{
	/// <summary>
	/// シーン遷移時のフェードイン・アウトを制御するためのクラス .
	/// </summary>
	public class FadeManager : SingletonMonoBehaviour<FadeManager>
	{
		/// <summary>フェード中の透明度</summary>
		private float fadeAlpha = 0;
		private bool isFadeIn = true;
		/// <summary>フェード中かどうか</summary>
		public bool isFading { get; private set; }
		/// <summary>フェード色</summary>
		public Color fadeColor = Color.black;

		public eTransitionType transitionType = eTransitionType.FADE;
		[Range(0, 360)]
		public float angle = 30f;

		public enum eTransitionType
		{
			FADE = 0,
			SLIDE,
		}

		new void Awake()
		{
			dontDestroy = true;
			base.Awake();
		}

		public void OnGUI()
		{

			// Fade .
			if (this.isFading)
			{
				switch (transitionType)
				{
					default:
					case eTransitionType.FADE:
						//色と透明度を更新して白テクスチャを描画 .
						fadeColor.a = fadeAlpha;
						GUI.color = fadeColor;
						GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
						break;
					case eTransitionType.SLIDE:
						if (Event.current.type.Equals(EventType.Repaint))
						{
							fadeColor.a = 1;

							float a = Mathf.Abs(Screen.width * Mathf.Cos(angle * Mathf.Deg2Rad));
							float b = Mathf.Abs(Screen.width * Mathf.Sin(angle * Mathf.Deg2Rad));
							float c = Mathf.Abs(Screen.height * Mathf.Cos(angle * Mathf.Deg2Rad));
							float d = Mathf.Abs(Screen.height * Mathf.Sin(angle * Mathf.Deg2Rad));
							float width = a + d;
							float height = b + c;
							Vector2 start = new Vector2(width * Mathf.Cos(angle * Mathf.Deg2Rad), width * Mathf.Sin(angle * Mathf.Deg2Rad)) * (isFadeIn ? 1 : -1);
							Vector2 end = Vector2.zero;
							Vector2 pos = start + (end - start) * fadeAlpha;

							Rect rect = new Rect(pos.x - (width - Screen.width) / 2, pos.y - (height - Screen.height) / 2, width, height);
							GUIUtility.RotateAroundPivot(angle, rect.center);
							Graphics.DrawTexture(rect, Texture2D.whiteTexture, new Rect(0, 0, 1, 1), 0, 0, 0, 0, fadeColor);
							GUI.matrix = Matrix4x4.identity;
						}
						break;
				}
			}
		}

		/// <summary>
		/// 画面遷移 .
		/// </summary>
		/// <param name='scene'>シーン名</param>
		/// <param name='interval'>暗転にかかる時間(秒)</param>
		public void LoadLevel(string scene, float interval)
		{
			StartCoroutine(TransScene(scene, interval));
		}

		/// <summary>
		/// シーン遷移用コルーチン .
		/// </summary>
		/// <param name='scene'>シーン名</param>
		/// <param name='interval'>暗転にかかる時間(秒)</param>
		private IEnumerator TransScene(string scene, float interval)
		{
			//だんだん暗く .
			isFading = true;
			isFadeIn = true;
			float time = 0;
			while (time <= interval)
			{
				fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
				time += Time.deltaTime;
				yield return 0;
			}

			//シーン切替 .
			SceneManager.LoadScene(scene);

			//だんだん明るく .
			isFadeIn = false;
			time = 0;
			while (time <= interval)
			{
				fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
				time += Time.deltaTime;
				yield return 0;
			}

			isFading = false;
		}

		// 追加関数
		/// <summary>
		/// フェードしてコールバック関数実行
		/// </summary>
		/// <param name="action">コールバック関数</param>
		/// <param name="interval">暗転にかかる時間(秒)</param>
		public void CallFunc(UnityEngine.Events.UnityAction action, float interval)
		{
			StartCoroutine(TransAndFunc(action, interval));
		}

		/// <summary>
		/// コールバック用コルーチン
		/// </summary>
		/// <param name="action">コールバック関数</param>
		/// <param name="interval">暗転にかかる時間(秒)</param>
		/// <returns></returns>
		private IEnumerator TransAndFunc(UnityEngine.Events.UnityAction action, float interval)
		{
			// シーン遷移などでフェード中なら終了まで待つ
			while (isFading)
			{
				yield return 0;
			}
			//だんだん暗く .
			isFading = true;
			isFadeIn = true;
			float time = 0;
			while (time <= interval)
			{
				fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
				time += Time.deltaTime;
				yield return 0;
			}

			//コールバック関数 .
			action();

			//だんだん明るく .
			isFadeIn = false;
			time = 0;
			while (time <= interval)
			{
				fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
				time += Time.deltaTime;
				yield return 0;
			}

			isFading = false;
		}
	}

}