﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace CommonPackage
{
    // 毎回newしたりAddCompornentしたりしないTween
    public class SingletonTween : MonoBehaviour
    {
        #region Singleton
        protected static SingletonTween instance;
        public static SingletonTween Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (SingletonTween)FindObjectOfType(typeof(SingletonTween));

                    if (instance == null)
                    {
                        instance = new GameObject("SingletonTween").AddComponent<SingletonTween>();
                        instance.Start();
                    }
                }

                return instance;
            }
        }

        protected void Awake()
        {
            CheckInstance();
        }

        protected bool CheckInstance()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
                return true;
            }
            else if (Instance == this)
            {
                return true;
            }

            Destroy(gameObject);
            return false;
        }
        #endregion

        const int POOL_SIZE = 100;
        const int UI_POOL_SIZE = 10;

        List<Tween> tweenList = new List<Tween>();
        List<GUITween> UItweenList = new List<GUITween>();

        float speed = 1;

        public delegate float EasingFunction(float start, float end, float Value);

        public enum EaseType
        {
            easeInQuad = 0,
            easeOutQuad,
            easeInOutQuad,
            easeInCubic,
            easeOutCubic,
            easeInOutCubic,
            easeInQuart,
            easeOutQuart,
            easeInOutQuart,
            easeInQuint,
            easeOutQuint,
            easeInOutQuint,
            easeInSine,
            easeOutSine,
            easeInOutSine,
            easeInExpo,
            easeOutExpo,
            easeInOutExpo,
            easeInCirc,
            easeOutCirc,
            easeInOutCirc,
            linear,
            spring,
            easeInBounce,
            easeOutBounce,
            easeInOutBounce,
            easeInBack,
            easeOutBack,
            easeInOutBack,
            easeInElastic,
            easeOutElastic,
            easeInOutElastic,
            punch
        }

        public class TweenBase
        {
            public bool isEnable = false;
            internal bool isDelay = false;
            public Transform target { protected set; get; }
            protected float startTime;
            protected float endTime;
            protected float delay;

            protected bool isLocal;
            protected Vector3 positionFrom;
            protected Vector3 eulerAnglesFrom;
            protected Vector3 scaleFrom;

            protected Vector3 positionTo;
            protected Vector3 eulerAnglesTo;
            protected Vector3 scaleTo;

            protected Vector3 controlPos;
            protected bool isBezier = false;

            protected UnityAction callFunc = null;
            protected TweenBase next = null;

            protected EasingFunction ease;

            protected float t;

            public void SetTarget(Transform target, float duration, EaseType easeType, bool isLocal = true, bool isDelay = false)
            {
                isEnable = true;
                this.isDelay = isDelay;
                this.target = target;
                startTime = Time.time;
                endTime = Time.time + Mathf.Max(duration, 0);
                delay = 0;
                ease = Instance.GetEasingFunction(easeType);
                this.isLocal = isLocal;
                if (isLocal)
                {
                    positionFrom = positionTo = target.localPosition;
                    eulerAnglesFrom = eulerAnglesTo = target.localEulerAngles;
                    scaleFrom = scaleTo = target.localScale;
                }
                else
                {
                    positionFrom = positionTo = target.position;
                    eulerAnglesFrom = eulerAnglesTo = target.eulerAngles;
                    scaleFrom = scaleTo = target.localScale;
                }
                callFunc = null;
                next = null;
                isBezier = false;
                t = 0;
                SetSpeed(Instance.speed);
            }

            public bool Update()
            {
                if (!isEnable || isDelay || startTime >= Time.time) return false;

                if (endTime > startTime)
                {
                    t = (Time.time - startTime) / (endTime - startTime);
                }
                else
                {
                    t = 1;
                }
                if (t >= 1 || target == null)
                {
                    isEnable = false;
                    t = 1;
                    if (callFunc != null)
                    {
                        callFunc();
                        callFunc = null;
                    }
                    if (next != null)
                    {
                        next.isDelay = false;
                        float duration = next.endTime - next.startTime;
                        next.startTime = Time.time + next.delay;
                        next.endTime = Time.time + Mathf.Max(duration, 0) + next.delay;
                    }
                    if (target == null)
                    {
                        return false;
                    }
                }
                var pos = target.position;
                if (isBezier)
                {
                    pos = Utility.GetBezier(positionFrom, controlPos, positionTo, t);
                }
                else
                {
                    pos.Set(ease(positionFrom.x, positionTo.x, t),
                            ease(positionFrom.y, positionTo.y, t),
                            ease(positionFrom.z, positionTo.z, t));
                }
                var rotate = target.eulerAngles;
                rotate.Set(ease(eulerAnglesFrom.x, eulerAnglesTo.x, t),
                        ease(eulerAnglesFrom.y, eulerAnglesTo.y, t),
                        ease(eulerAnglesFrom.z, eulerAnglesTo.z, t));
                var scale = target.localScale;
                scale.Set(ease(scaleFrom.x, scaleTo.x, t),
                        ease(scaleFrom.y, scaleTo.y, t),
                        ease(scaleFrom.z, scaleTo.z, t));
                if (isLocal)
                {
                    target.localPosition = pos;
                    target.localEulerAngles = rotate;
                    target.localScale = scale;
                }
                else
                {
                    target.position = pos;
                    target.eulerAngles = rotate;
                    target.localScale = scale;
                }
                return true;
            }

            public void SetSpeed(float speed)
            {
                float current = (endTime - startTime) * t + startTime;
                float tmp1 = current - startTime;
                float tmp2 = endTime - current;
                if (speed > 0)
                {
                    tmp1 /= speed;
                    tmp2 /= speed;
                }
                else
                {
                    tmp1 = tmp2 = 0;
                }
                startTime = current - tmp1;
                endTime = current + tmp2;
            }
        }

        public class Tween : TweenBase
        {
            public SpriteRenderer targetSpriteRenderer { protected set; get; }
            float alphaFrom;
            float alphaTo;

            public void SetSpriteRenderer(SpriteRenderer renderer)
            {
                targetSpriteRenderer = renderer;
                if (renderer != null)
                {
                    alphaFrom = alphaTo = renderer.color.a;
                }
            }

            public Tween MoveTo(Vector3 position)
            {
                positionTo = position;
                isBezier = false;
                return this;
            }

            public Tween MoveTo(float x, float y, float z)
            {
                positionTo.x = x;
                positionTo.y = y;
                positionTo.z = z;
                isBezier = false;
                return this;
            }

            public Tween MoveBy(Vector3 position)
            {
                positionTo += position;
                isBezier = false;
                return this;
            }

            public Tween MoveBy(float x, float y, float z)
            {
                positionTo.x += x;
                positionTo.y += y;
                positionTo.z += z;
                isBezier = false;
                return this;
            }

            public Tween BezierTo(Vector3 controlPosition, Vector3 position)
            {
                controlPos = controlPosition;
                positionTo = position;
                isBezier = true;
                return this;
            }

            public Tween ScaleTo(Vector3 scale)
            {
                scaleTo = scale;
                return this;
            }

            public Tween ScaleTo(float x, float y, float z)
            {
                scaleTo.x = x;
                scaleTo.y = y;
                scaleTo.z = z;
                return this;
            }

            public Tween ScaleTo(float scale)
            {
                scaleTo.x = scale;
                scaleTo.y = scale;
                scaleTo.z = scale;
                return this;
            }

            public Tween ScaleBy(Vector3 scale)
            {
                scaleTo.x *= scale.x;
                scaleTo.y *= scale.y;
                scaleTo.z *= scale.z;
                return this;
            }

            public Tween ScaleBy(float x, float y, float z)
            {
                scaleTo.x *= x;
                scaleTo.y *= y;
                scaleTo.z *= z;
                return this;
            }

            public Tween ScaleBy(float scale)
            {
                scaleTo.x *= scale;
                scaleTo.y *= scale;
                scaleTo.z *= scale;
                return this;
            }

            public Tween RotateTo(Vector3 angle)
            {
                RotateTo(angle.x, angle.y, angle.z);
                return this;
            }

            public Tween RotateTo(float x, float y, float z)
            {
                if (eulerAnglesFrom.x - x > 180) x += 360;
                if (eulerAnglesFrom.y - y > 180) y += 360;
                if (eulerAnglesFrom.z - z > 180) z += 360;
                if (eulerAnglesFrom.x - x < -180) x -= 360;
                if (eulerAnglesFrom.y - y < -180) y -= 360;
                if (eulerAnglesFrom.z - z < -180) z -= 360;
                eulerAnglesTo.x = x;
                eulerAnglesTo.y = y;
                eulerAnglesTo.z = z;
                return this;
            }

            public Tween RotateBy(Vector3 angle)
            {
                eulerAnglesTo += angle;
                return this;
            }

            public Tween RotateBy(float x, float y, float z)
            {
                eulerAnglesTo.x += x;
                eulerAnglesTo.y += y;
                eulerAnglesTo.z += z;
                return this;
            }

            public Tween AlphaTo(float alpha)
            {
                alphaTo = alpha;
                return this;
            }

            public Tween Delay(float delay)
            {
                this.delay = delay;
                startTime += delay;
                endTime += delay;
                return this;
            }

            public Tween CallFunc(UnityAction callFunc)
            {
                this.callFunc = callFunc;
                return this;
            }

            public Tween Next(float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true)
            {
                var ret = RunRaw(target, duration, easeType, isLocal, true);
                ret.positionFrom = ret.positionTo = positionTo;
                ret.eulerAnglesFrom = ret.eulerAnglesTo = eulerAnglesTo;
                ret.scaleFrom = ret.scaleTo = scaleTo;
                next = ret;
                return ret;
            }

            public new bool Update()
            {
                if (!base.Update()) return false;

                if (targetSpriteRenderer != null)
                {
                    var color = targetSpriteRenderer.color;
                    color.a = ease(alphaFrom, alphaTo, t);
                    targetSpriteRenderer.color = color;
                }

                return true;
            }
        }

        public class GUITween : TweenBase
        {
            public new RectTransform target { protected set; get; }
            public CanvasGroup targetCanvasGroup { protected set; get; }

            Vector2 sizeDeltaFrom;
            Vector2 pivotFrom;
            Vector2 anchorMinFrom;
            Vector2 anchorMaxFrom;
            float alphaFrom;

            Vector2 sizeDeltaTo;
            Vector2 pivotTo;
            Vector2 anchorMinTo;
            Vector2 anchorMaxTo;
            float alphaTo;

            public void SetTarget(RectTransform target, float duration, EaseType easeType, bool isLocal = true, bool isDelay = false)
            {
                base.SetTarget(target, duration, easeType, isLocal, isDelay);
                this.target = target;
                sizeDeltaFrom = sizeDeltaTo = target.sizeDelta;
                pivotFrom = pivotTo = target.pivot;
                anchorMinFrom = anchorMinTo = target.anchorMin;
                anchorMaxFrom = anchorMaxTo = target.anchorMax;
            }

            public void SetCanvasGroup(CanvasGroup canvasGroup)
            {
                targetCanvasGroup = canvasGroup;
                if (canvasGroup != null)
                {
                    alphaFrom = alphaTo = canvasGroup.alpha;
                }
            }

            public GUITween MoveTo(Vector2 positon)
            {
                positionTo.x = positon.x;
                positionTo.y = positon.y;
                return this;
            }

            public GUITween MoveTo(float x, float y)
            {
                positionTo.x = x;
                positionTo.y = y;
                return this;
            }

            public GUITween MoveBy(Vector2 positon)
            {
                positionTo.x += positon.x;
                positionTo.y += positon.y;
                return this;
            }

            public GUITween MoveBy(float x, float y)
            {
                positionTo.x += x;
                positionTo.y += y;
                return this;
            }

            public GUITween BezierTo(Vector2 controlPosition, Vector2 position)
            {
                controlPos = controlPosition;
                positionTo = position;
                isBezier = true;
                return this;
            }

            public GUITween SizeTo(Vector2 size)
            {
                sizeDeltaTo = size;
                return this;
            }

            public GUITween SizeTo(float w, float h)
            {
                sizeDeltaTo.x = w;
                sizeDeltaTo.y = h;
                return this;
            }

            public GUITween PivotTo(Vector2 pivot)
            {
                pivotTo = pivot;
                return this;
            }

            public GUITween PivotTo(float x, float y)
            {
                pivotTo.x = x;
                pivotTo.y = y;
                return this;
            }

            public GUITween AnchorTo(Vector2 anchorMin, Vector2 anchorMax)
            {
                anchorMinTo = anchorMin;
                anchorMaxTo = anchorMax;
                return this;
            }

            public GUITween AnchorTo(float minX, float minY, float maxX, float maxY)
            {
                anchorMinTo.x = minX;
                anchorMinTo.y = minY;
                anchorMaxTo.x = maxX;
                anchorMaxTo.y = maxY;
                return this;
            }

            public GUITween ScaleTo(Vector3 scale)
            {
                scaleTo = scale;
                return this;
            }

            public GUITween ScaleTo(float x, float y, float z)
            {
                scaleTo.x = x;
                scaleTo.y = y;
                scaleTo.z = z;
                return this;
            }

            public GUITween ScaleTo(float scale)
            {
                scaleTo.x = scale;
                scaleTo.y = scale;
                scaleTo.z = scale;
                return this;
            }

            public GUITween RotateTo(Vector3 angle)
            {
                RotateTo(angle.x, angle.y, angle.z);
                return this;
            }

            public GUITween RotateTo(float x, float y, float z)
            {
                if (eulerAnglesFrom.x - x > 180) x += 360;
                if (eulerAnglesFrom.y - y > 180) y += 360;
                if (eulerAnglesFrom.z - z > 180) z += 360;
                if (eulerAnglesFrom.x - x < -180) x -= 360;
                if (eulerAnglesFrom.y - y < -180) y -= 360;
                if (eulerAnglesFrom.z - z < -180) z -= 360;
                eulerAnglesTo.x = x;
                eulerAnglesTo.y = y;
                eulerAnglesTo.z = z;
                return this;
            }

            public GUITween AlphaTo(float alpha)
            {
                alphaTo = alpha;
                return this;
            }

            public GUITween Delay(float delay)
            {
                this.delay = delay;
                startTime += delay;
                endTime += delay;
                return this;
            }

            public GUITween CallFunc(UnityAction callFunc)
            {
                this.callFunc = callFunc;
                return this;
            }

            public GUITween Next(float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true)
            {
                var ret = RunGUIRaw(target, targetCanvasGroup, duration, easeType, isLocal, true);
                ret.positionFrom = ret.positionTo = positionTo;
                ret.eulerAnglesFrom = ret.eulerAnglesTo = eulerAnglesTo;
                ret.scaleFrom = ret.scaleTo = scaleTo;
                ret.sizeDeltaFrom = ret.sizeDeltaTo = sizeDeltaTo;
                ret.pivotFrom = ret.pivotTo = pivotTo;
                ret.anchorMinFrom = ret.anchorMinTo = anchorMinTo;
                ret.anchorMaxFrom = ret.anchorMaxTo = anchorMaxTo;
                ret.alphaFrom = ret.alphaTo = alphaTo;
                next = ret;
                return ret;
            }

            public new bool Update()
            {
                if (!base.Update()) return false;

                var pos = target.anchoredPosition;
                if (isBezier)
                {
                    pos = Utility.GetBezier(positionFrom, controlPos, positionTo, t);
                }
                else
                {
                    pos.Set(ease(positionFrom.x, positionTo.x, t),
                            ease(positionFrom.y, positionTo.y, t));
                }
                if (isLocal)
                {
                    target.anchoredPosition = pos;
                }
                else
                {
                    target.position = pos;
                }

                var size = target.sizeDelta;
                size.Set(ease(sizeDeltaFrom.x, sizeDeltaTo.x, t),
                        ease(sizeDeltaFrom.y, sizeDeltaTo.y, t));
                target.sizeDelta = size;

                var pivot = target.pivot;
                pivot.Set(ease(pivotFrom.x, pivotTo.x, t),
                        ease(pivotFrom.y, pivotTo.y, t));
                target.pivot = pivot;

                var anchorMin = target.anchorMin;
                anchorMin.Set(ease(anchorMinFrom.x, anchorMinTo.x, t),
                        ease(anchorMinFrom.y, anchorMinTo.y, t));
                target.anchorMin = anchorMin;

                var anchorMax = target.anchorMax;
                anchorMax.Set(ease(anchorMaxFrom.x, anchorMaxTo.x, t),
                        ease(anchorMaxFrom.y, anchorMaxTo.y, t));
                target.anchorMax = anchorMax;

                if (targetCanvasGroup != null)
                {
                    targetCanvasGroup.alpha = ease(alphaFrom, alphaTo, t);

                }

                return true;
            }
        }

        void Start()
        {
            for (int i = tweenList.Count; i < POOL_SIZE; i++)
            {
                tweenList.Add(new Tween());
            }
            for (int i = UItweenList.Count; i < UI_POOL_SIZE; i++)
            {
                UItweenList.Add(new GUITween());
            }
        }

        void Update()
        {
            foreach (var tween in tweenList)
            {
                tween.Update();
            }
            foreach (var tween in UItweenList)
            {
                tween.Update();
            }
        }

        static Tween RunRaw(Transform target, float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true, bool isDelay = false)
        {
            Tween ret = null;
            foreach (var tween in Instance.tweenList)
            {
                if (!tween.isEnable && ret == null)
                {
                    tween.SetTarget(target, duration, easeType, isLocal, isDelay);
                    tween.SetSpriteRenderer(target.GetComponentInChildren<SpriteRenderer>());
                    ret = tween;
                }
                else if (!isDelay && tween.target == target)
                {
                    tween.isEnable = false;
                }
            }
            if (ret == null)
            {
                Debug.LogWarning("[SingletonTween] Over limit " + Instance.tweenList.Count);
                ret = Instance.tweenList[0];
                ret.SetTarget(target, duration, easeType, isLocal, isDelay);
                ret.SetSpriteRenderer(target.GetComponentInChildren<SpriteRenderer>());
            }
            return ret;
        }

        public static Tween Run(Transform target, float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true)
        {
            return RunRaw(target, duration, easeType, isLocal);
        }

        public static Tween Run(GameObject obj, float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true)
        {
            return RunRaw(obj.transform, duration, easeType, isLocal);
        }

        static GUITween RunGUIRaw(RectTransform target, CanvasGroup canvasGroup, float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true, bool isDelay = false)
        {
            GUITween ret = null;
            foreach (var tween in Instance.UItweenList)
            {
                if (!tween.isEnable && ret == null)
                {
                    tween.SetTarget(target, duration, easeType, isLocal, isDelay);
                    tween.SetCanvasGroup(canvasGroup);
                    ret = tween;
                }
                else if (!isDelay && tween.target == target)
                {
                    tween.isEnable = false;
                }
            }
            if (ret == null)
            {
                Debug.LogWarning("[SingletonTween] Over limit " + Instance.UItweenList.Count);
                ret = Instance.UItweenList[0];
                ret.SetTarget(target, duration, easeType, isLocal, isDelay);
                ret.SetCanvasGroup(canvasGroup);
            }
            return ret;
        }

        public static GUITween RunGUI(RectTransform target, float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true)
        {
            return RunGUIRaw(target, null, duration, easeType, isLocal);
        }

        public static GUITween RunGUI(RectTransform target, CanvasGroup canvasGroup, float duration, EaseType easeType = EaseType.easeOutExpo, bool isLocal = true)
        {
            return RunGUIRaw(target, canvasGroup, duration, easeType, isLocal);
        }

        public static void SetSpeed(float speed)
        {
            SetSpeedOnce(speed / Instance.speed);
            Instance.speed = speed;
        }

        public static void SetSpeedOnce(float speed)
        {
            foreach (var tween in Instance.tweenList)
            {
                if (tween.isEnable)
                {
                    tween.SetSpeed(speed);
                }
            }
            foreach (var tween in Instance.UItweenList)
            {
                if (tween.isEnable)
                {
                    tween.SetSpeed(speed);
                }
            }
        }

        public static void StopAll()
        {
            foreach (var tween in Instance.tweenList)
            {
                tween.isEnable = false;
            }
            foreach (var tween in Instance.UItweenList)
            {
                tween.isEnable = false;
            }
        }

        #region Easing Curves
        private float linear(float start, float end, float value)
        {
            return Mathf.Lerp(start, end, value);
        }

        private float clerp(float start, float end, float value)
        {
            float min = 0.0f;
            float max = 360.0f;
            float half = Mathf.Abs((max - min) * 0.5f);
            float retval = 0.0f;
            float diff = 0.0f;
            if ((end - start) < -half)
            {
                diff = ((max - start) + end) * value;
                retval = start + diff;
            }
            else if ((end - start) > half)
            {
                diff = -((max - end) + start) * value;
                retval = start + diff;
            }
            else retval = start + (end - start) * value;
            return retval;
        }

        private float spring(float start, float end, float value)
        {
            value = Mathf.Clamp01(value);
            value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
            return start + (end - start) * value;
        }

        private float easeInQuad(float start, float end, float value)
        {
            end -= start;
            return end * value * value + start;
        }

        private float easeOutQuad(float start, float end, float value)
        {
            end -= start;
            return -end * value * (value - 2) + start;
        }

        private float easeInOutQuad(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * value * value + start;
            value--;
            return -end * 0.5f * (value * (value - 2) - 1) + start;
        }

        private float easeInCubic(float start, float end, float value)
        {
            end -= start;
            return end * value * value * value + start;
        }

        private float easeOutCubic(float start, float end, float value)
        {
            value--;
            end -= start;
            return end * (value * value * value + 1) + start;
        }

        private float easeInOutCubic(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * value * value * value + start;
            value -= 2;
            return end * 0.5f * (value * value * value + 2) + start;
        }

        private float easeInQuart(float start, float end, float value)
        {
            end -= start;
            return end * value * value * value * value + start;
        }

        private float easeOutQuart(float start, float end, float value)
        {
            value--;
            end -= start;
            return -end * (value * value * value * value - 1) + start;
        }

        private float easeInOutQuart(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * value * value * value * value + start;
            value -= 2;
            return -end * 0.5f * (value * value * value * value - 2) + start;
        }

        private float easeInQuint(float start, float end, float value)
        {
            end -= start;
            return end * value * value * value * value * value + start;
        }

        private float easeOutQuint(float start, float end, float value)
        {
            value--;
            end -= start;
            return end * (value * value * value * value * value + 1) + start;
        }

        private float easeInOutQuint(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * value * value * value * value * value + start;
            value -= 2;
            return end * 0.5f * (value * value * value * value * value + 2) + start;
        }

        private float easeInSine(float start, float end, float value)
        {
            end -= start;
            return -end * Mathf.Cos(value * (Mathf.PI * 0.5f)) + end + start;
        }

        private float easeOutSine(float start, float end, float value)
        {
            end -= start;
            return end * Mathf.Sin(value * (Mathf.PI * 0.5f)) + start;
        }

        private float easeInOutSine(float start, float end, float value)
        {
            end -= start;
            return -end * 0.5f * (Mathf.Cos(Mathf.PI * value) - 1) + start;
        }

        private float easeInExpo(float start, float end, float value)
        {
            end -= start;
            return end * Mathf.Pow(2, 10 * (value - 1)) + start;
        }

        private float easeOutExpo(float start, float end, float value)
        {
            end -= start;
            return end * (-Mathf.Pow(2, -10 * value) + 1) + start;
        }

        private float easeInOutExpo(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * Mathf.Pow(2, 10 * (value - 1)) + start;
            value--;
            return end * 0.5f * (-Mathf.Pow(2, -10 * value) + 2) + start;
        }

        private float easeInCirc(float start, float end, float value)
        {
            end -= start;
            return -end * (Mathf.Sqrt(1 - value * value) - 1) + start;
        }

        private float easeOutCirc(float start, float end, float value)
        {
            value--;
            end -= start;
            return end * Mathf.Sqrt(1 - value * value) + start;
        }

        private float easeInOutCirc(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return -end * 0.5f * (Mathf.Sqrt(1 - value * value) - 1) + start;
            value -= 2;
            return end * 0.5f * (Mathf.Sqrt(1 - value * value) + 1) + start;
        }

        /* GFX47 MOD START */
        private float easeInBounce(float start, float end, float value)
        {
            end -= start;
            float d = 1f;
            return end - easeOutBounce(0, end, d - value) + start;
        }
        /* GFX47 MOD END */

        /* GFX47 MOD START */
        //private float bounce(float start, float end, float value){
        private float easeOutBounce(float start, float end, float value)
        {
            value /= 1f;
            end -= start;
            if (value < (1 / 2.75f))
            {
                return end * (7.5625f * value * value) + start;
            }
            else if (value < (2 / 2.75f))
            {
                value -= (1.5f / 2.75f);
                return end * (7.5625f * (value) * value + .75f) + start;
            }
            else if (value < (2.5 / 2.75))
            {
                value -= (2.25f / 2.75f);
                return end * (7.5625f * (value) * value + .9375f) + start;
            }
            else
            {
                value -= (2.625f / 2.75f);
                return end * (7.5625f * (value) * value + .984375f) + start;
            }
        }
        /* GFX47 MOD END */

        /* GFX47 MOD START */
        private float easeInOutBounce(float start, float end, float value)
        {
            end -= start;
            float d = 1f;
            if (value < d * 0.5f) return easeInBounce(0, end, value * 2) * 0.5f + start;
            else return easeOutBounce(0, end, value * 2 - d) * 0.5f + end * 0.5f + start;
        }
        /* GFX47 MOD END */

        private float easeInBack(float start, float end, float value)
        {
            end -= start;
            value /= 1;
            float s = 1.70158f;
            return end * (value) * value * ((s + 1) * value - s) + start;
        }

        private float easeOutBack(float start, float end, float value)
        {
            float s = 1.70158f;
            end -= start;
            value = (value) - 1;
            return end * ((value) * value * ((s + 1) * value + s) + 1) + start;
        }

        private float easeInOutBack(float start, float end, float value)
        {
            float s = 1.70158f;
            end -= start;
            value /= .5f;
            if ((value) < 1)
            {
                s *= (1.525f);
                return end * 0.5f * (value * value * (((s) + 1) * value - s)) + start;
            }
            value -= 2;
            s *= (1.525f);
            return end * 0.5f * ((value) * value * (((s) + 1) * value + s) + 2) + start;
        }

        private float punch(float amplitude, float value)
        {
            float s = 9;
            if (value == 0)
            {
                return 0;
            }
            else if (value == 1)
            {
                return 0;
            }
            float period = 1 * 0.3f;
            s = period / (2 * Mathf.PI) * Mathf.Asin(0);
            return (amplitude * Mathf.Pow(2, -10 * value) * Mathf.Sin((value * 1 - s) * (2 * Mathf.PI) / period));
        }

        /* GFX47 MOD START */
        private float easeInElastic(float start, float end, float value)
        {
            end -= start;

            float d = 1f;
            float p = d * .3f;
            float s = 0;
            float a = 0;

            if (value == 0) return start;

            if ((value /= d) == 1) return start + end;

            if (a == 0f || a < Mathf.Abs(end))
            {
                a = end;
                s = p / 4;
            }
            else
            {
                s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
            }

            return -(a * Mathf.Pow(2, 10 * (value -= 1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p)) + start;
        }
        /* GFX47 MOD END */

        /* GFX47 MOD START */
        //private float elastic(float start, float end, float value){
        private float easeOutElastic(float start, float end, float value)
        {
            /* GFX47 MOD END */
            //Thank you to rafael.marteleto for fixing this as a port over from Pedro's UnityTween
            end -= start;

            float d = 1f;
            float p = d * .3f;
            float s = 0;
            float a = 0;

            if (value == 0) return start;

            if ((value /= d) == 1) return start + end;

            if (a == 0f || a < Mathf.Abs(end))
            {
                a = end;
                s = p * 0.25f;
            }
            else
            {
                s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
            }

            return (a * Mathf.Pow(2, -10 * value) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p) + end + start);
        }

        /* GFX47 MOD START */
        private float easeInOutElastic(float start, float end, float value)
        {
            end -= start;

            float d = 1f;
            float p = d * .3f;
            float s = 0;
            float a = 0;

            if (value == 0) return start;

            if ((value /= d * 0.5f) == 2) return start + end;

            if (a == 0f || a < Mathf.Abs(end))
            {
                a = end;
                s = p / 4;
            }
            else
            {
                s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
            }

            if (value < 1) return -0.5f * (a * Mathf.Pow(2, 10 * (value -= 1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p)) + start;
            return a * Mathf.Pow(2, -10 * (value -= 1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p) * 0.5f + end + start;
        }
        /* GFX47 MOD END */

        EasingFunction GetEasingFunction(EaseType easeType)
        {
            EasingFunction ease;
            switch (easeType)
            {
                case EaseType.easeInQuad:
                    ease = new EasingFunction(easeInQuad);
                    break;
                case EaseType.easeOutQuad:
                    ease = new EasingFunction(easeOutQuad);
                    break;
                case EaseType.easeInOutQuad:
                    ease = new EasingFunction(easeInOutQuad);
                    break;
                case EaseType.easeInCubic:
                    ease = new EasingFunction(easeInCubic);
                    break;
                case EaseType.easeOutCubic:
                    ease = new EasingFunction(easeOutCubic);
                    break;
                case EaseType.easeInOutCubic:
                    ease = new EasingFunction(easeInOutCubic);
                    break;
                case EaseType.easeInQuart:
                    ease = new EasingFunction(easeInQuart);
                    break;
                case EaseType.easeOutQuart:
                    ease = new EasingFunction(easeOutQuart);
                    break;
                case EaseType.easeInOutQuart:
                    ease = new EasingFunction(easeInOutQuart);
                    break;
                case EaseType.easeInQuint:
                    ease = new EasingFunction(easeInQuint);
                    break;
                case EaseType.easeOutQuint:
                    ease = new EasingFunction(easeOutQuint);
                    break;
                case EaseType.easeInOutQuint:
                    ease = new EasingFunction(easeInOutQuint);
                    break;
                case EaseType.easeInSine:
                    ease = new EasingFunction(easeInSine);
                    break;
                case EaseType.easeOutSine:
                    ease = new EasingFunction(easeOutSine);
                    break;
                case EaseType.easeInOutSine:
                    ease = new EasingFunction(easeInOutSine);
                    break;
                case EaseType.easeInExpo:
                    ease = new EasingFunction(easeInExpo);
                    break;
                case EaseType.easeOutExpo:
                    ease = new EasingFunction(easeOutExpo);
                    break;
                case EaseType.easeInOutExpo:
                    ease = new EasingFunction(easeInOutExpo);
                    break;
                case EaseType.easeInCirc:
                    ease = new EasingFunction(easeInCirc);
                    break;
                case EaseType.easeOutCirc:
                    ease = new EasingFunction(easeOutCirc);
                    break;
                case EaseType.easeInOutCirc:
                    ease = new EasingFunction(easeInOutCirc);
                    break;
                default:
                case EaseType.linear:
                    ease = new EasingFunction(linear);
                    break;
                case EaseType.spring:
                    ease = new EasingFunction(spring);
                    break;
                /* GFX47 MOD START */
                /*case EaseType.bounce:
                    ease = new EasingFunction(bounce);
                    break;*/
                case EaseType.easeInBounce:
                    ease = new EasingFunction(easeInBounce);
                    break;
                case EaseType.easeOutBounce:
                    ease = new EasingFunction(easeOutBounce);
                    break;
                case EaseType.easeInOutBounce:
                    ease = new EasingFunction(easeInOutBounce);
                    break;
                /* GFX47 MOD END */
                case EaseType.easeInBack:
                    ease = new EasingFunction(easeInBack);
                    break;
                case EaseType.easeOutBack:
                    ease = new EasingFunction(easeOutBack);
                    break;
                case EaseType.easeInOutBack:
                    ease = new EasingFunction(easeInOutBack);
                    break;
                /* GFX47 MOD START */
                /*case EaseType.elastic:
                    ease = new EasingFunction(elastic);
                    break;*/
                case EaseType.easeInElastic:
                    ease = new EasingFunction(easeInElastic);
                    break;
                case EaseType.easeOutElastic:
                    ease = new EasingFunction(easeOutElastic);
                    break;
                case EaseType.easeInOutElastic:
                    ease = new EasingFunction(easeInOutElastic);
                    break;
                    /* GFX47 MOD END */
            }
            return ease;
        }
        #endregion
    }
}