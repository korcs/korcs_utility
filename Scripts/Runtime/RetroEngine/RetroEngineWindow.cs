﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.RetroEngine
{
	public class RetroEngineWindow : MonoBehaviour
	{
		[SerializeField]
		public RetroEngineBuffer buffer = null;
		[SerializeField]
		public Vector2Int size = new Vector2Int(16, 16);
		
		public int x { get; set; }
		public int y { get; set; }

		void Start()
		{

		}

		void Update()
		{

		}

		public static RetroEngineWindow Create(string name, RetroEngineBuffer buffer, int x, int y, int w, int h)
		{
			var ret = new GameObject(name).AddComponent<RetroEngineWindow>();
			ret.buffer = buffer;
			ret.size = new Vector2Int(w, h);
			ret.x = x;
			ret.y = y;
			return ret;
		}
	}
}