﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CommonPackage.RetroEngine
{
	public class RetroEngineTextBuffer : RetroEngineBuffer
	{
		[SerializeField, Multiline(10)]
		string textList = "";
		[SerializeField]
		public int size = 8;

		List<char> _charList = null;
		List<char> charList
		{
			get
			{
				if (_charList == null)
				{
					_charList = textList.ToCharArray().ToList();
					_charList.RemoveAll(a => a == "\n".ToCharArray()[0]);
				}
				return _charList;
			}
		}

		public int hNum { get { return width / size; } }

		public int GetIndex(char ch)
		{
			if (charList == null) return -1;
			return charList.IndexOf(ch);
		}

	}
}