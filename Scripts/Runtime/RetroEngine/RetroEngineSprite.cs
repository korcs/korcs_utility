﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.RetroEngine
{
	public class RetroEngineSprite : MonoBehaviour
	{
		[SerializeField]
		public RetroEngineBuffer buffer = null;
		[SerializeField]
		public Vector2Int size = new Vector2Int(16, 16);

		public int hNum { get { return buffer.width / size.x; } }
		public int index { get; set; }
		public float x { get; set; }
		public float y { get; set; }
		public bool isFlip { get; set; }
		public Vector2 pivot { get; set; } = new Vector2(0.5f, 0.5f);

		void Start()
		{

		}

		void Update()
		{

		}

		public static RetroEngineSprite Create(string name, RetroEngineBuffer buffer, int x, int y, int w, int h, int index = 0, bool isFlip = false)
		{
			var ret = new GameObject(name).AddComponent<RetroEngineSprite>();
			ret.buffer = buffer;
			ret.size = new Vector2Int(w, h);
			ret.x = x;
			ret.y = y;
			ret.index = index;
			ret.isFlip = isFlip;
			return ret;
		}
	}
}