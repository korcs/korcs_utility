﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CommonPackage.RetroEngine
{
	public class RetroEngineTexture : MonoBehaviour
	{
		[SerializeField]
		Vector2Int size;

		[SerializeField]
		TextureFormat textureFormat = TextureFormat.RGB24;
		[SerializeField]
		FilterMode filterMode = FilterMode.Point;

		Texture2D tex = null;
		Color[] colors;
		int[] colorIndexsBuffer;
		int[] colorIndexsRender;
		Rect mask;

		[SerializeField]
		public List<Color> colorList = new List<Color>();

		[SerializeField]
		List<Texture2D> textureList = new List<Texture2D>();
		Dictionary<Texture2D, RetroEngineBuffer> bufferDic = new Dictionary<Texture2D, RetroEngineBuffer>();
		[SerializeField]
		RetroEngineTextBuffer textBuffer = null;

		public double timeCount { get; private set; } = 0;

		double transitionTime = 0;
		float transitionDuration = 0;
		eTransition transitionType = eTransition.NONE;
		public bool isTransitionIn { get; private set; }
		public bool isTransition { get; private set; }

		public int height => tex.height;
		public int width => tex.width;
		public int speed { get; set; } = 1;

		public enum eTransition
		{
			NONE = 0,
			PINCH,
			STRIPE,
			TOBIRA,
			FADE_WHITE,
			FADE_BLACK,
			MOSAIC,
		}

		void Awake()
		{
			tex = CreateTexture();
			RawImage raw = GetComponent<RawImage>();
			if (raw != null)
			{
				raw.texture = tex;
			}
			MeshRenderer mesh = GetComponent<MeshRenderer>();
			if (mesh != null && mesh.sharedMaterial != null)
			{
				Material mat = new Material(mesh.sharedMaterial);
				mat.mainTexture = tex;
				mesh.sharedMaterial = mat;
			}

			var buffers = FindObjectsOfType<RetroEngineBuffer>();
			foreach (var b in buffers)
			{
				b.transform.SetParent(transform);
				b.transform.localPosition = Vector3.zero;
				bufferDic.Add(b.texture, b);
			}
			foreach (var t in textureList)
			{
				GetBuffer(t);
			}

			if (textBuffer == null)
			{
				textBuffer = FindObjectOfType<RetroEngineTextBuffer>();
			}

			ResetMask();

			Application.targetFrameRate = 60;
		}

		void LateUpdate()
		{
			timeCount += Time.deltaTime * speed;
			if (tex != null)
			{
				TransitionUpdate();

				for (int i = 0; i < colorIndexsRender.Length; i++)
				{
					colors[i] = colorList[colorIndexsRender[i]];
				}

				tex.SetPixels(colors);
				tex.Apply();
			}
		}

		void OnDestroy()
		{
			Destroy(tex);
			colors = null;
		}

		Texture2D CreateTexture()
		{
			colors = new Color[size.x * size.y];
			colorIndexsBuffer = new int[size.x * size.y];
			colorIndexsRender = new int[size.x * size.y];
			Texture2D ret = new Texture2D(size.x, size.y, textureFormat, false, false);

			ret.filterMode = filterMode;
			ret.wrapMode = TextureWrapMode.Clamp;
			//ret.alphaIsTransparency = false;
			for (int x = 0; x < ret.width; x++)
			{
				for (int y = 0; y < ret.height; y++)
				{
					colors[x + y * ret.width] = colorList[0];
				}
			}
			ret.SetPixels(colors);
			ret.Apply();

			return ret;
		}

		public void Clear(int color)
		{
			for (int x = 0; x < tex.width; x++)
			{
				for (int y = 0; y < tex.height; y++)
				{
					DrawPixel(color, x, y);
				}
			}
		}

		public void ResetMask()
		{
			mask.xMin = 0;
			mask.yMin = 0;
			mask.xMax = width;
			mask.yMax = height;
		}

		public void SetMask(int x, int y, int w, int h)
		{
			mask.xMin = x - w / 2;
			mask.yMin = y - h / 2;
			mask.xMax = x + w / 2;
			mask.yMax = y + h / 2;
		}

		public void DrawPixel(int color, int x, int y)
		{
			if (color >= colorList.Count || color < 0) return;
			if (x < 0 || x >= tex.width || y < 0 || y >= tex.height) return;
			if (x < mask.xMin || x > mask.xMax || y < mask.yMin || y >= mask.yMax) return;

			colorIndexsBuffer[x + (tex.height - y - 1) * tex.width] = color;
		}

		public void DrawPixel(int color, Vector2Int p)
		{
			DrawPixel(color, p.x, p.y);
		}

		public void DrawLine(int color, Vector2Int p1, Vector2Int p2)
		{

		}

		public void DrawRect(int color, Vector2Int p1, Vector2Int p2, bool isWired = false)
		{
			DrawRect(color, p1.x, p1.y, p2.x, p2.y, isWired);
		}

		public void DrawRect(int color, int x1, int y1, int x2, int y2, bool isWired = false)
		{
			int minX = Mathf.Min(x1, x2);
			int maxX = Mathf.Max(x1, x2);
			int minY = Mathf.Min(y1, y2);
			int maxY = Mathf.Max(y1, y2);

			for (int x = minX; x <= maxX; x++)
			{
				for (int y = minY; y <= maxY; y++)
				{
					if (isWired && x != minX && x != maxX && y != minY && y != maxY)
					{
						continue;
					}
					DrawPixel(color, x, y);
				}
			}
		}

		public void DrawCircle(int color, Vector2Int p1, Vector2Int p2, bool isWired = false)
		{

		}

		public void DrawSprite(Texture2D tex, int srcX, int srcY, int w, int h, int dstX, int dstY, int color = -1)
		{
			DrawSprite(GetBuffer(tex), srcX, srcY, w, h, dstX, dstY, color);
		}

		public void DrawSprite(RetroEngineBuffer buffer, int srcX, int srcY, int w, int h, int dstX, int dstY, int color = -1)
		{
			if (buffer == null) return;
			for (int x = 0; x < w; x++)
			{
				for (int y = 0; y < h; y++)
				{
					int c = buffer.GetPixel(x + srcX, y + srcY);
					if (color >= 0 && c >= 0)
					{
						DrawPixel(color, x + dstX, y + dstY);
					}
					else
					{
						DrawPixel(c, x + dstX, y + dstY);
					}
				}
			}
		}

		public void DrawSprite(RetroEngineBuffer buffer, int srcX, int srcY, int w, int h, int dstX, int dstY, int dstw, int dsth)
		{
			if (buffer == null) return;
			for (int x = 0; x < dstw; x++)
			{
				for (int y = 0; y < dsth; y++)
				{
					int c = buffer.GetPixel(x % w + srcX, y % h + srcY);
					DrawPixel(c, x + dstX, y + dstY);
				}
			}
		}

		public void DrawSprite(RetroEngineSprite sprite, int x, int y, int color = -1)
		{
			DrawSprite(sprite.buffer,
				(sprite.index % sprite.hNum * sprite.size.x), (sprite.index / sprite.hNum * sprite.size.y),
				sprite.size.x, sprite.size.y,
				x + (int)(sprite.size.x * (sprite.pivot.x - 1.0f)), y + (int)(sprite.size.y * (sprite.pivot.y - 1.0f)), color);
		}

		public void DrawSprite(RetroEngineSprite sprite, int color = -1)
		{
			DrawSprite(sprite, Mathf.RoundToInt(sprite.x), Mathf.RoundToInt(sprite.y), color);
		}

		public void DrawWindow(RetroEngineWindow window)
		{
			// 角 
			int w = window.buffer.width / 3;
			int h = window.buffer.height / 3;
			DrawSprite(window.buffer, 0, 0, w, h, window.x - window.size.x / 2, window.y - window.size.y / 2);
			DrawSprite(window.buffer, w * 2, 0, w, h, window.x + window.size.x / 2 - w, window.y - window.size.y / 2);
			DrawSprite(window.buffer, 0, h * 2, w, h, window.x - window.size.x / 2, window.y + window.size.y / 2 - h);
			DrawSprite(window.buffer, w * 2, h * 2, w, h, window.x + window.size.x / 2 - w, window.y + window.size.y / 2 - h);

			// 側面
			DrawSprite(window.buffer, 0, h, w, h, window.x - window.size.x / 2, window.y - window.size.y / 2 + h, w, window.size.y - h * 2);
			DrawSprite(window.buffer, w * 2, h, w, h, window.x + window.size.x / 2 - w, window.y - window.size.y / 2 + h, w, window.size.y - h * 2);
			DrawSprite(window.buffer, w, 0, w, h, window.x - window.size.x / 2 + w, window.y - window.size.y / 2, window.size.x - w * 2, h);
			DrawSprite(window.buffer, w, h * 2, w, h, window.x - window.size.x / 2 + w, window.y + window.size.y / 2 - h, window.size.x - w * 2, h);

			// 真ん中
			DrawSprite(window.buffer, w, h, w, h, window.x - window.size.x / 2 + w, window.y - window.size.y / 2 + h, window.size.x - w * 2, window.size.y - h * 2);
		}

		public void DrawMap(StageDataObject stage, int offsetX, int offsetY)
		{
			DrawMap(GetBuffer(stage.tileData.texture), stage, offsetX, offsetY);
		}

		public void DrawMap(RetroEngineBuffer buffer, StageDataObject stage, int offsetX, int offsetY)
		{
			if (buffer == null || stage == null) return;

			int size = stage.tileData.size;
			int hNum = buffer.width / size;

			int xMin = Mathf.Max(0, -offsetX / size - 1);
			int xMax = Mathf.Min(stage.blocks.Count, (-offsetX + tex.width) / size + 1);
			int yMin = Mathf.Max(0, -offsetY / size - 1);
			int yMax = Mathf.Min(stage.blocks[0].y.Count, (-offsetY + tex.height) / size + 1);
			int i = 0;
			int n = 0;
			int index = 0;

			for (int x = xMin; x < xMax; x++)
			{
				for (int y = yMin; y < yMax; y++)
				{
					index = stage.blocks[x].y[y].i;
					n = 0;
					if (stage.tileData.tiles[index].isAnimate)
					{
						n = (int)((timeCount * stage.tileData.tiles[index].speed) % stage.tileData.tiles[index].ids.Count);
					}
					i = stage.tileData.tiles[index].ids[n];
					DrawSprite(buffer,
						(i % hNum * size), (i / hNum * size),
						size, size,
						x * size + offsetX, y * size + offsetY);
				}
			}
		}

		public void DrawText(string text, int offsetX, int offsetY, int hNum = 16, int color = 3)
		{
			if (textBuffer == null) return;
			var array = text.Normalize(System.Text.NormalizationForm.FormD).ToCharArray();
			int x = 0, y = 0;
			for (int i = 0; i < array.Length; i++)
			{
				int index = textBuffer.GetIndex(array[i]);
				DrawChar(index, x, y, offsetX, offsetY, color);

				if (index >= 2)
				{
					x++;
				}
				if (x >= hNum || array[i] == '\n')
				{
					x = 0;
					y += 2;
				}
			}
		}

		void DrawChar(int index, int x, int y, int offsetX, int offsetY, int color = -1)
		{
			if (index < 0) return;
			if (index < 2)
			{
				x -= 1;
				y -= 1;
			}
			DrawSprite(textBuffer,
				(index % textBuffer.hNum * textBuffer.size), (index / textBuffer.hNum * textBuffer.size),
				textBuffer.size, textBuffer.size,
				offsetX + x * textBuffer.size, offsetY + y * textBuffer.size, color);
		}

		public void SetColor(List<Color> colorList)
		{
			for (int i = 0; i < colorList.Count && i < this.colorList.Count; i++)
			{
				this.colorList[i] = colorList[i];
			}
		}

		public void ShiftX()
		{

		}

		void TransitionUpdate()
		{
			if (transitionType == eTransition.NONE)
			{
				TranscribeRender();
			}
			float n = (float)(timeCount - transitionTime) / transitionDuration;
			if (n > 1)
			{
				n = 1;
				isTransition = false;
			}
			else if (n < 0)
			{
				n = 0;
			}
			if (!isTransitionIn)
			{
				n = 1 - n;
			}

			switch (transitionType)
			{
				case eTransition.PINCH:
					{
						int h = (int)(tex.height * n / 2);
						DrawRect(3, new Vector2Int(0, 0), new Vector2Int(tex.width, h));
						DrawRect(3, new Vector2Int(0, tex.height - h), new Vector2Int(tex.width, tex.height));
						TranscribeRender();
					}
					break;
				case eTransition.STRIPE:
					{
						int x, y;
						int h = 4;
						int w = (int)(tex.width * n);
						for (int i = 0; i < tex.height / h; i++)
						{
							y = h * i;
							x = (i % 2 == 0) ? 0 : tex.width - w;
							DrawRect(3, new Vector2Int(x, y), new Vector2Int(x + w, y + h));
						}
						TranscribeRender();
					}
					break;
				case eTransition.TOBIRA:
					{
						int add = (int)(-n * 3);
						//Clear(0);
						for (int y = 0; y < tex.height; y++)
						{
							float s = Mathf.Sin(y / 5.0f);
							int val = (int)(s * n * 100);
							for (int x = 0; x < tex.width; x++)
							{
								if (x + val >= 0 && x + val < tex.width)
								{
									colorIndexsRender[x + val + y * tex.width] = Mathf.Clamp(colorIndexsBuffer[x + y * tex.width] + add, 0, 3);
								}
							}
						}
					}
					break;
				case eTransition.FADE_WHITE:
					{
						int add = (int)(-n * 3);
						//Clear(0);
						for (int y = 0; y < tex.height; y++)
						{
							for (int x = 0; x < tex.width; x++)
							{
								colorIndexsRender[x + y * tex.width] = Mathf.Clamp(colorIndexsBuffer[x + y * tex.width] + add, 0, 3);
							}
						}
					}
					break;
				case eTransition.FADE_BLACK:
					{
						int add = (int)(n * 3);
						//Clear(0);
						for (int y = 0; y < tex.height; y++)
						{
							for (int x = 0; x < tex.width; x++)
							{
								colorIndexsRender[x + y * tex.width] = Mathf.Clamp(colorIndexsBuffer[x + y * tex.width] + add, 0, 3);
							}
						}
					}
					break;
				case eTransition.MOSAIC:
					{
						if (n >= 1)
						{
							Clear(0);
							break;
						}
						int mosaic = (int)Mathf.Pow(2, (int)(n * 8));
						int i = 0;
						int j = 0;
						for (int y = 0; y < tex.height; y++)
						{
							j = Mathf.Clamp(((y + mosaic / 2) / mosaic) * mosaic, 0, tex.height - 1); 
							for (int x = 0; x < tex.width; x++)
							{
								i = Mathf.Clamp(((x + mosaic / 2) / mosaic) * mosaic, 0, tex.width - 1);
								colorIndexsRender[x + y * tex.width] = colorIndexsBuffer[i + j * tex.width];
							}
						}
					}
					break;
			}
		}

		public void SetTransition(eTransition transition, float duration, bool isIn, float wait = 0)
		{
			transitionType = transition;
			transitionDuration = duration;
			transitionTime = timeCount + wait;
			isTransitionIn = isIn;
			isTransition = true;
		}

		void TranscribeRender()
		{
			//colorIndexsRender = colorIndexsBuffer.Clone() as byte[];
			for(int i = 0; i < colorIndexsBuffer.Length; i++)
			{
				colorIndexsRender[i] = colorIndexsBuffer[i];
			}
		}

		public RetroEngineBuffer GetBuffer(Texture2D tex)
		{
			if (bufferDic.ContainsKey(tex))
			{
				return bufferDic[tex];
			}
			else
			{
				var obj = new GameObject(tex.name);
				obj.transform.SetParent(transform);
				obj.transform.localPosition = Vector3.zero;
				var buf = obj.AddComponent<RetroEngineBuffer>();
				buf.Load(this, tex);
				bufferDic.Add(tex, buf);
				return buf;
			}
		}

		public RetroEngineBuffer GetBuffer(string name)
		{
			foreach(var buffer in bufferDic.Values)
			{
				if (buffer.name == name)
				{
					return buffer;
				}
			}

			return null;
		}
	}
}