﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.RetroEngine
{
	public class RetroEngineBuffer : MonoBehaviour
	{
		[SerializeField]
		Texture2D tex;
#if UNITY_EDITOR
		[SerializeField]
		Texture2D loadedTex;
#endif

		[SerializeField, HideInInspector]
		List<int> indexList = new List<int>();

		public Texture2D texture { get { return tex; } }

		public void Load(RetroEngineTexture gb, Texture2D tex)
		{
			this.tex = tex;
			Load(gb);
		}

		public void Load(RetroEngineTexture gb)
		{
			indexList.Clear();
			for (int y = 0; y < tex.height; y++)
			{
				for (int x = 0; x < tex.width; x++)
				{
					Color32 c = tex.GetPixel(x, y);
					int n = -1;
					int diff = -1;
					if (c.a > 0)
					{
						for (int i = 0; i < gb.colorList.Count; i++)
						{
							Color32 color = gb.colorList[i];
							int d = Mathf.Abs(color.r - c.r) + Mathf.Abs(color.g - c.g) + Mathf.Abs(color.b - c.b);
							if (diff < 0 || d < diff)
							{
								n = i;
								diff = d;
							}
						}
					}
					indexList.Add(n);
				}
			}

		}

		public int GetPixel(int x, int y)
		{
			if (x < tex.width && y < tex.height && indexList.Count > x + y * tex.width)
			{
				return indexList[x + (tex.height - y - 1) * tex.width];
			}
			return 0;
		}

		public int width { get { return tex.width; } }
		public int height { get { return tex.height; } }

#if UNITY_EDITOR
		[ContextMenu("PreLoad")]
		public void PreLoad()
		{
			var gb = FindObjectOfType<RetroEngineTexture>();
			if (gb != null)
			{
				UnityEditor.Undo.RecordObject(this, "RetroEngineBuffer PreLoad");
				Load(gb);
				loadedTex = CreateTexture(gb);
			}
		}

		Texture2D CreateTexture(RetroEngineTexture gb)
		{
			var colors = new Color[tex.width * tex.height];
			Texture2D ret = new Texture2D(tex.width, tex.height);

			for (int x = 0; x < ret.width; x++)
			{
				for (int y = 0; y < ret.height; y++)
				{
					if (GetPixel(x, y) < 0)
					{
						colors[x + y * ret.width] = Color.clear;
					}
					else
					{
						colors[x + y * ret.width] = gb.colorList[GetPixel(x, y)];
					}
				}
			}
			ret.SetPixels(colors);
			ret.Apply();

			return ret;
		}
#endif
	}
}