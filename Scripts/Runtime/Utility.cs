﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CommonPackage
{
    /// <summary>
    /// 便利機能
    /// </summary>
    public static class Utility
    {
        const float POLAR_RADIUS = 6378150;

        // 緯度経度をメートルに変換
        public static double[] LatLon2Meter(double lat, double lon)
        {
            double[] ret = new double[]{
                //(POLAR_RADIUS * Math.Cos((float)(lat/180.0f*Math.PI)) * 2 * Math.PI / 360.0f * lon) / 10,
                (POLAR_RADIUS * Math.Cos((float)(35/180.0f*Math.PI)) * 2 * Math.PI / 360.0f * lon) / 10,
                ((2 * Math.PI * POLAR_RADIUS) / 360.0f * lat) / 10
            };

            return ret;
        }

        // 日時の文字列からDateTimeを取得
        public static DateTime GetDateTime(string datetime)
        {
            DateTime ret;
            if (DateTime.TryParse(datetime, out ret))
            {
                return ret;
            }
            else
            {
                Debug.LogWarning("DateTime Parse Error! : " + datetime);
                return DateTime.Now;
            }
        }

        public static bool IsDateTime(string datetime)
        {
            DateTime ret;
            return DateTime.TryParse(datetime, out ret);
        }

        public static bool IsTimeSpan(string time)
        {
            TimeSpan ret;
            return TimeSpan.TryParse(time, out ret);
        }

        public static string GetTimeSpanText(TimeSpan timespan)
        {
            string time;
            if (timespan.TotalDays >= 1)
            {
                time = string.Format("{0}日", (int)timespan.TotalDays);
            }
            else if (timespan.TotalHours >= 1)
            {
                time = string.Format("{0}時間", (int)timespan.TotalHours);
            }
            else if (timespan.TotalMinutes >= 1)
            {
                time = string.Format("{0}分", (int)timespan.TotalMinutes);
            }
            else
            {
                time = string.Format("{0}秒", (int)timespan.TotalSeconds);
            }
            return time;
        }

        // 遅延実行
        public static void Delay(float duration, UnityAction callFunc, MonoBehaviour monoBehaviour)
        {
            monoBehaviour.StartCoroutine(DelayCoroutine(duration, callFunc));
        }
        static IEnumerator DelayCoroutine(float duration, UnityAction callFunc)
        {
            yield return new WaitForSeconds(duration);
            callFunc();
        }

        // シーンの追加
        public static void AddScene(string name)
        {
            if (!SceneManager.GetSceneByName(name).isLoaded)
            {
                SceneManager.LoadScene(name, LoadSceneMode.Additive);
            }
        } 

        // シーンの削除
        public static void UnloadScene(string name)
        {
            if (SceneManager.GetSceneByName(name).isLoaded)
            {
                SceneManager.UnloadSceneAsync(name);
            }
        }

        // EventTriggerに追加
        public static void AddEventTrigger(EventTrigger trigger, UnityAction<BaseEventData> call, EventTriggerType type)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = type;
            entry.callback.AddListener(call);
            trigger.triggers.Add(entry);
        }

        // ベジェ曲線取得
        public static Vector3 GetBezier(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            return (1 - t) * (1 - t) * p0 + 2 * (1 - t) * t * p1 + t * t * p2;
        }

        public static Vector2 GetBezier(Vector2 p0, Vector2 p1, Vector2 p2, float t)
        {
            return (1 - t) * (1 - t) * p0 + 2 * (1 - t) * t * p1 + t * t * p2;
        }

        public static float GetBezier(float p0, float p1, float p2, float t)
        {
            return (1 - t) * (1 - t) * p0 + 2 * (1 - t) * t * p1 + t * t * p2;
        }

        // 数値をループ
        public static int Loop(int value, int min, int max)
        {
            int ret = value;
            while(ret < min)
            {
                ret += (max - min + 1);
            }
            while (ret > max)
            {
                ret -= (max - min + 1);
            }
            return ret;
        }

        public static T Clamp<T>(T value, T min, T max)
            where T : IComparable
        {
            T ret = value;
            if (ret.CompareTo(min) < 0) ret = min;
            if (ret.CompareTo(max) > 0) ret = max;
            return ret;
        }

        // バイト配列から16進数の文字列を生成
        public static string ToHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder(bytes.Length * 2);
            foreach (byte b in bytes)
            {
                if (b < 16) sb.Append('0'); // 二桁になるよう0を追加
                sb.Append(Convert.ToString(b, 16));
            }
            return sb.ToString();
        }

        // 16進数の文字列からバイト配列を生成
        public static byte[] FromHexString(string str)
        {
            int length = str.Length / 2;
            byte[] bytes = new byte[length];
            int j = 0;
            for (int i = 0; i < length; i++)
            {
                bytes[i] = Convert.ToByte(str.Substring(j, 2), 16);
                j += 2;
            }
            return bytes;
        }

        // 文字コードから文字を取得
        public static string GetStringFromCharCode(string charCode)
        {
            int charCode16 = Convert.ToInt32(charCode, 16);  // 16進数文字列 -> 数値
            char c = Convert.ToChar(charCode16);  // 数値(文字コード) -> 文字
            return c.ToString();    
        }

        public static IEnumerator WaitCoroutine(Animator animator, string stateName, UnityAction call, float normalizedTime = 0)
        {
            int hash = Animator.StringToHash(stateName);
            while (true)
            {
                yield return null;
                var state = animator.GetCurrentAnimatorStateInfo(0);
                if (state.fullPathHash == hash && state.normalizedTime >= normalizedTime)
                {
                    if (call != null) call();
                    yield break;
                }
            }
        }

		public static bool IsAnimatorState(Animator animator, string stateName)
		{
			int hash = Animator.StringToHash(stateName);
			var state = animator.GetCurrentAnimatorStateInfo(0);
			return state.fullPathHash == hash;
		}

		public static ulong GetFNV64A1(string _str)
		{
			const ulong fnv64Offset = 14695981039346656037;
			const ulong fnv64Prime = 0x100000001b3;
			ulong hash = fnv64Offset;

			foreach (var c in _str)
			{
				hash = hash ^ (ulong)c;
				hash *= fnv64Prime;
			}

			return hash;
		}

		// breadth first find child by name
		public static Transform FindChildRecursive(this Transform trans, string name)
        {
            for (int i = 0; i < trans.childCount; i++)
            {
                Transform child = trans.GetChild(i);
                string childName = child.name;
                if (childName == name) return child;
            }

            for (int i = 0; i < trans.childCount; i++)
            {
                Transform child = trans.GetChild(i);
                Transform found = child.FindChildRecursive(name);
                if (found) return found;
            }

            return null;
        }

        public static void DestroyAllChildren (this Transform trans)
        {
            var children = new List<GameObject>();
            foreach (Transform child in trans) children.Add(child.gameObject);
            children.ForEach(child => UnityEngine.Object.Destroy(child));
        }
		
		public static string SnakeToUpperCamel(this string self)
		{
			if (string.IsNullOrEmpty(self)) return self;

			return self
				.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => char.ToUpperInvariant(s[0]) + s.ToLower().Substring(1, s.Length - 1))
				.Aggregate(string.Empty, (s1, s2) => s1 + s2)
			;
		}

		public static string UpperCamelToSnake(this string self)
		{
			if (string.IsNullOrEmpty(self)) return self;
			string ret = "";
			for (int i = 0; i < self.Length; i++)
			{
				if (char.IsUpper(self[i]) && i != 0)
				{
					ret += "_";
				}
				ret += char.ToUpperInvariant(self[i]);
			}
			return ret;
		}

#if UNITY_EDITOR
		public static void DrawTexture(Rect srcRect, Rect rect, Color color, Texture2D texture, Rect maskRect = new Rect(), float angle = 0)
        {
            if (Event.current.type != EventType.Repaint) return;

			if (maskRect.width > 0 && maskRect.height > 0)
			{
				if (rect.x >= maskRect.x + maskRect.width || rect.y >= maskRect.y + maskRect.height)
				{
					return;
				}

				MaskeRect(ref srcRect, ref rect, maskRect, (int)angle);
			}
			GUIUtility.RotateAroundPivot(angle, rect.center);
			Graphics.DrawTexture(rect, texture, srcRect, 0, 0, 0, 0, color);
			GUI.matrix = Matrix4x4.identity;
        }

        static void MaskeRect(ref Rect srcRect, ref Rect rect, Rect maskRect, int angle)
		{
            var originalRect = new Rect(rect);
            bool isOffsetX = false;
            bool isOffsetY = false;
            if (angle == 0 || angle == 180)
			{
                if (originalRect.x < maskRect.x)
                {
                    rect.width = Mathf.Max(originalRect.width - (maskRect.x - originalRect.x), 0);
                    rect.x = maskRect.x;
                    isOffsetX = angle == 0;
                }
                if (originalRect.y < maskRect.y)
                {
                    rect.height = Mathf.Max(originalRect.height - (maskRect.y - originalRect.y), 0);
                    rect.y = maskRect.y;
                    isOffsetY = angle == 180;
                }
                if (originalRect.xMax > maskRect.xMax)
                {
                    rect.width += maskRect.xMax - originalRect.xMax;
                    isOffsetX = angle == 180;
                }
                if (originalRect.yMax > maskRect.yMax)
                {
                    rect.height += maskRect.yMax - originalRect.yMax;
                    isOffsetY = angle == 0;
                }
            }
            else if (angle == 90 || angle == 270 || angle == -90)
			{
				if (originalRect.x < maskRect.x)
				{
					rect.height = Mathf.Max(originalRect.width - (maskRect.x - originalRect.x), 0);
					//rect.x = maskRect.x;
					isOffsetY = angle != 90;
				}
				if (originalRect.y < maskRect.y)
				{
					rect.width = Mathf.Max(originalRect.height - (maskRect.y - originalRect.y), 0);
					//rect.y = maskRect.y;
					isOffsetX = angle == 90;
				}
				if (originalRect.xMax > maskRect.xMax)
				{
					rect.height += maskRect.xMax - originalRect.xMax;
                    rect.x = maskRect.xMax - rect.height;
					isOffsetY = angle == 90;
				}
				if (originalRect.yMax > maskRect.yMax)
				{
					rect.width += maskRect.yMax - originalRect.yMax;
                    rect.y = maskRect.yMax - rect.width;
                    isOffsetX = angle != 90;
				}
			}

            if (originalRect.width > rect.width)
            {
                float w = srcRect.width;
                srcRect.width *= rect.width / originalRect.width;
                if (isOffsetX)
                {
                    srcRect.x += w - srcRect.width;
                }
            }
            if (originalRect.height > rect.height)
            {
                float h = srcRect.height;
                srcRect.height *= rect.height / originalRect.height;
                if (isOffsetY)
                {
                    srcRect.y += h - srcRect.height;
                }
            }
        }

        public static void CreateFolder(string path)
		{
            string[] split = path.Split('/');
            string newPath = split[0];
            for (int i = 1; i < split.Length - 1; i++)
			{
                if (!AssetDatabase.IsValidFolder(newPath + "/" + split[i]))
				{
                    AssetDatabase.CreateFolder(newPath, split[i]);
                }
                newPath = newPath + "/" + split[i];
			}
		}

		public static List<Type> GetSubClases<T>()
		{
			return Assembly.GetAssembly(typeof(T)).GetTypes().Where(t => t.IsSubclassOf(typeof(T)) && !t.IsAbstract).ToList();
		}

        public static List<Type> GetInheritedTypes<T>()
		{
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(T).IsAssignableFrom(p) && p.IsClass)
                .Prepend(null)
                .ToList();
        }

		[MenuItem("Assets/Get PNG Data")]
		public static void GetPngData()
		{
			int instanceID = Selection.activeInstanceID;
			string path = AssetDatabase.GetAssetPath(instanceID);
			var tex = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
			if (tex != null)
			{
				var p = tex.EncodeToPNG();
				StringBuilder sb = new StringBuilder();
				sb.Append("{");
				for(int i = 0; i < p.Length; i++)
				{
					sb.Append(p[i]);
					if (i < p.Length - 1) sb.Append(",");
				}
				sb.Append("}");
				GUIUtility.systemCopyBuffer = sb.ToString();
				Debug.Log(GUIUtility.systemCopyBuffer);
			}
		}
#endif

#if !UNITY_EDITOR
        private static Vector3 TouchPosition = Vector3.zero;
#endif

        /// <summary>
        /// タッチ情報を取得(エディタと実機を考慮)
        /// </summary>
        /// <returns>タッチ情報。タッチされていない場合は null</returns>
        public static TouchInfo GetTouch()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
			if (Input.GetMouseButtonDown(0)) { return TouchInfo.Began; }
            if (Input.GetMouseButton(0)) { return TouchInfo.Moved; }
            if (Input.GetMouseButtonUp(0)) { return TouchInfo.Ended; }
#else
            if (Input.touchCount > 0)
            {
                return (TouchInfo)((int)Input.GetTouch(0).phase);
            }
#endif
            return TouchInfo.None;
        }

        /// <summary>
        /// タッチポジションを取得(エディタと実機を考慮)
        /// </summary>
        /// <returns>タッチポジション。タッチされていない場合は (0, 0, 0)</returns>
        public static Vector3 GetTouchPosition()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
			TouchInfo touch = GetTouch();
            if (touch != TouchInfo.None) { return Input.mousePosition; }
#else
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                TouchPosition.x = touch.position.x;
                TouchPosition.y = touch.position.y;
                return TouchPosition;
            }
#endif
            return Vector3.zero;
        }

        /// <summary>
        /// タッチワールドポジションを取得(エディタと実機を考慮)
        /// </summary>
        /// <param name='camera'>カメラ</param>
        /// <returns>タッチワールドポジション。タッチされていない場合は (0, 0, 0)</returns>
        public static Vector3 GetTouchWorldPosition(Camera camera)
        {
            return camera.ScreenToWorldPoint(GetTouchPosition());
        }

		public static Vector3 GetHoverPosition()
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			return Input.mousePosition;
#else
            return GetTouchPosition();
#endif
		}
	}

    public enum TouchInfo
    {
        /// <summary>
        /// タッチなし
        /// </summary>
        None = 99,

        // 以下は UnityEngine.TouchPhase の値に対応
        /// <summary>
        /// タッチ開始
        /// </summary>
        Began = 0,
        /// <summary>
        /// タッチ移動
        /// </summary>
        Moved = 1,
        /// <summary>
        /// タッチ静止
        /// </summary>
        Stationary = 2,
        /// <summary>
        /// タッチ終了
        /// </summary>
        Ended = 3,
        /// <summary>
        /// タッチキャンセル
        /// </summary>
        Canceled = 4,
    }


}
