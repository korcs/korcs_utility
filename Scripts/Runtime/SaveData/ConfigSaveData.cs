﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage
{
    public class ConfigSaveData : SaveData<ConfigSaveData.Content, ConfigSaveData>
    {

        protected override string FILE_NAME { get { return "config.dat"; } }

        // この内容を保存する
        [Serializable]
        public class Content : ISerializationCallbackReceiver
        {
            public int screen = 2;
            public double BGMVolume = 0.5;
            public double SEVolume = 0.5;
            public int direction = 1;
            public List<Key> keyConfig = new List<Key>();
            public bool first = false;
            public eLanguage language = 0;

            public void OnBeforeSerialize()
            {
            }

            public void OnAfterDeserialize()
            {
            }
        }

        public enum eLanguage : int
        {
            JA = 0,
            EN,
        }

        public void Save()
        {
            Save(FILE_NAME);
        }

        public void Load()
        {
            Load(FILE_NAME);
        }
    }
}