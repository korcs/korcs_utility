﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;

namespace CommonPackage
{
    /// <summary>
    /// ローカルのセーブデータ管理
    /// </summary>
    public class SaveData<Content, T>
    where Content : ISerializationCallbackReceiver, new()
    where T : SaveData<Content, T>, new()
    {
        protected static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }
                return instance;
            }
        }

        protected virtual string FILE_NAME { get { return "save.dat"; } }
        private const string FILE_KEY = "data";
        private const string ENCRYPT_KEY = @"2EgLZ6vHU701edE4qTCBZFnZ7KSfXRfU";
        private const string ENCRYPT_IV = @"bvn88CcHKnRDZYZclYKanvYf7OJy3EzV";

#if UNITY_SWITCH
        private nn.account.Uid userId;
        private const string mountName = "MySave";
        private const string fileName = "save";
        private string filePath = string.Format("{0}:/{1}", mountName, fileName);
        private nn.fs.FileHandle fileHandle = new nn.fs.FileHandle();
#endif

        [Serializable]
        public class Key
        {
            public int key = -1;
            public int axis = -1;
            public bool positive = true;
        }

        private Content content = new Content();
        private Content emptyContent = new Content();

        static public int current = 0;


        // セーブデータの内容取得
        public static Content GetContent()
        {
            return Instance.content;
        }

        // セーブ
        protected void Save(string name)
        {
#if UNITY_SWITCH
            byte[] data;

            Encoding enc = Encoding.UTF8;
            string json = JsonUtility.ToJson(content);
            data = enc.GetBytes(Encrypt(json));

            // Nintendo Switch Guideline 0080
            UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();

            nn.Result result = nn.fs.File.Delete(filePath);
            if (!nn.fs.FileSystem.ResultPathNotFound.Includes(result))
            {
                result.abortUnlessSuccess();
            }

            result = nn.fs.File.Create(filePath, data.Length);
            result.abortUnlessSuccess();

            result = nn.fs.File.Open(ref fileHandle, filePath, nn.fs.OpenFileMode.Write);
            result.abortUnlessSuccess();

            result = nn.fs.File.Write(fileHandle, 0, data, data.LongLength, nn.fs.WriteOption.Flush);
            result.abortUnlessSuccess();

            nn.fs.File.Close(fileHandle);
            result = nn.fs.FileSystem.Commit(mountName);
            result.abortUnlessSuccess();

            // Nintendo Switch Guideline 0080
            UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();


#elif UNITY_N3DS
            UnityEngine.N3DS.FileSystemSave.Mount();

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            string fileName = "data:/" + FILE_NAME;
            FileStream file = File.Create(fileName);
            binaryFormatter.Serialize(file, content);
            file.Close();

            UnityEngine.N3DS.FileSystemSave.Unmount();
#else
            string path = Application.persistentDataPath + "/" + name;

            string json = JsonUtility.ToJson(content);
            Debug.Log("Save json \n" + json);
            Encoding enc = Encoding.UTF8;
            File.WriteAllText(path, Encrypt(json), enc);
#endif
        }

        // ロード
        protected Content Load(string name, bool isTemp = false)
        {
#if UNITY_SWITCH
            /*
			nn.Result result;
			if (filePath == null || filePath.Length == 0)
			{
				nn.account.Account.Initialize();
				nn.account.UserHandle userHandle = new nn.account.UserHandle();

				if (nn.account.Account.TryOpenPreselectedUser(ref userHandle))
				{
					nn.account.Account.GetUserId(ref userId, userHandle);
				}
                else
				{
                    nn.account.Account.GetLastOpenedUser(ref userId);
				}

				result = nn.fs.SaveData.Mount(mountName, userId);
				result.abortUnlessSuccess();

				filePath = string.Format("{0}:/{1}", mountName, fileName);
			}

			nn.fs.EntryType entryType = 0;
			result = nn.fs.FileSystem.GetEntryType(ref entryType, filePath);
			if (nn.fs.FileSystem.ResultPathNotFound.Includes(result)) { return new Content(); }
			result.abortUnlessSuccess();

			result = nn.fs.File.Open(ref fileHandle, filePath, nn.fs.OpenFileMode.Read);
			result.abortUnlessSuccess();

			long fileSize = 0;
			result = nn.fs.File.GetSize(ref fileSize, fileHandle);
			result.abortUnlessSuccess();

			byte[] data = new byte[fileSize];
			result = nn.fs.File.Read(fileHandle, 0, data, fileSize);
			result.abortUnlessSuccess();

			nn.fs.File.Close(fileHandle);

			Encoding enc = Encoding.UTF8;
			string json = Decrypt(enc.GetString(data)) + "  \n\n";
			Debug.Log("Load json \n" + json);
			var content = JsonUtility.FromJson<Content>(json);
			if (!isTemp)
			{
				this.content = content;
			}
			return content;
            */
            nn.fs.EntryType entryType = 0;
            nn.Result result = nn.fs.FileSystem.GetEntryType(ref entryType, filePath);
            if (nn.fs.FileSystem.ResultPathNotFound.Includes(result)) { return new Content(); }
            result.abortUnlessSuccess();

            result = nn.fs.File.Open(ref fileHandle, filePath, nn.fs.OpenFileMode.Read);
            result.abortUnlessSuccess();

            long fileSize = 0;
            result = nn.fs.File.GetSize(ref fileSize, fileHandle);
            result.abortUnlessSuccess();

            byte[] data = new byte[fileSize];
            result = nn.fs.File.Read(fileHandle, 0, data, fileSize);
            result.abortUnlessSuccess();

            nn.fs.File.Close(fileHandle);

            Encoding enc = Encoding.UTF8;
            string json = Decrypt(enc.GetString(data)) + "  \n\n";
            Debug.Log("Load json \n" + json);
            var content = JsonUtility.FromJson<Content>(json);
            if (!isTemp)
            {
                this.content = content;
            }
            return content;
#elif UNITY_N3DS
            string fileName = "data:/" + FILE_NAME;
            if (File.Exists(fileName))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream file = File.Open(fileName, FileMode.Open, FileAccess.Read);
                var content = (Content)binaryFormatter.Deserialize(file);
                file.Close();
			    if (!isTemp)
			    {
				    this.content = content;
			    }
			    return content;
            }
		    else
            {
			    Reset();
			    return this.content;
            }
#else
            string path = Application.persistentDataPath + "/" + name;
            if (File.Exists(path))
            {
                Encoding enc = Encoding.UTF8;
                string json = Decrypt(File.ReadAllText(path, enc)) + "  \n\n";
                Debug.Log("Load json \n" + json);
                var content = JsonUtility.FromJson<Content>(json);
                if (!isTemp)
                {
                    this.content = content;
                }
                return content;
            }
            else if (!isTemp)
            {
                Reset();
                return this.content;
            }
            else
            {
                return emptyContent;
            }
#endif
        }

        // データの暗号化

        public static string Encrypt(string text)
        {
            RijndaelManaged rijndeal = new RijndaelManaged();
            rijndeal.BlockSize = 256;
            rijndeal.KeySize = 256;
            rijndeal.IV = Encoding.UTF8.GetBytes(ENCRYPT_IV);
            rijndeal.Key = Encoding.UTF8.GetBytes(ENCRYPT_KEY);
            rijndeal.Mode = CipherMode.CBC;
            rijndeal.Padding = PaddingMode.PKCS7;

            // 文字列をバイト型配列に変換
            byte[] src = Encoding.UTF8.GetBytes(text);

            // 暗号化する
            using (ICryptoTransform encrypt = rijndeal.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // バイト型配列からBase64形式の文字列に変換
                return Convert.ToBase64String(dest);
            }
        }

        public static string Decrypt(string text)
        {
            RijndaelManaged rijndeal = new RijndaelManaged();
            rijndeal.BlockSize = 256;
            rijndeal.KeySize = 256;
            rijndeal.IV = Encoding.UTF8.GetBytes(ENCRYPT_IV);
            rijndeal.Key = Encoding.UTF8.GetBytes(ENCRYPT_KEY);
            rijndeal.Mode = CipherMode.CBC;
            rijndeal.Padding = PaddingMode.PKCS7;

            // Base64形式の文字列からバイト型配列に変換
            byte[] src = Convert.FromBase64String(text);

            // 複号化する
            using (ICryptoTransform decrypt = rijndeal.CreateDecryptor())
            {
                byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                return Encoding.UTF8.GetString(dest);
            }
        }


        public void Reset()
        {
            content = new Content();
        }
    }
}