﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage
{
    public class DatabaseLorder : SingletonMonoBehaviour<DatabaseLorder>
    {
        [SerializeField]
        DatabaseObject _blockData = null;
        [SerializeField]
        DatabaseObject _itemData = null;
		[SerializeField]
        DatabaseObject _characterData = null;

		public DatabaseObject blockData { get { return _blockData; } }
        public DatabaseObject itemData { get { return _itemData; } }
        public DatabaseObject characterData { get { return _characterData; } }

        //public DatabaseObject.ItemData GetItemData(int id, int hash)
        //{
        //    DatabaseObject.ItemData ret;

        //    if (hash == itemData.items[id].hash)
        //    {
        //        ret = new DatabaseObject.ItemData(itemData.items[id]);
        //    }
        //    else
        //    {
        //        var item = itemData.items.Find(a => a.hash == hash);
        //        if (item != null)
        //            ret = new DatabaseObject.ItemData(item);
        //        else
        //            ret = new DatabaseObject.ItemData(itemData.items[id]);
        //    }

        //    return ret;
        //}

    }
}
