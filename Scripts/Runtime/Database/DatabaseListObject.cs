﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace CommonPackage
{
	[Serializable]
	public class DatabaseListObject : ScriptableObject
	{
		[SerializeField]
		List<DatabaseObject> _items = new List<DatabaseObject>();

		public List<DatabaseObject> items
		{
			get
			{
				return _items;
			}
			set
			{
				_items = value;
				foreach(var i in _items)
				{
					if (i == null)
					{
						_items.Remove(i);
					}
				}
			}
		}

		static public DatabaseListObject Create()
		{
			var ret = CreateInstance<DatabaseListObject>();
			
			return ret;
		}

#if UNITY_EDITOR
		[CustomEditor(typeof(DatabaseListObject))]
		public class DatabaseListObjectEditor : Editor
		{
			ReorderableList reorderableList = null;

			#region ReorderableList
			public void InitReorderableList()
			{
				DatabaseListObject instance = target as DatabaseListObject;
				if (instance != null)
				{
					reorderableList = new ReorderableList(instance.items, typeof(DatabaseObject.ItemSetting), true, false, false, false);

					reorderableList.onAddCallback += Add;
					reorderableList.onRemoveCallback += Remove;
					reorderableList.onCanAddCallback += CanAdd;
					reorderableList.onCanRemoveCallback += CanRemove;
					reorderableList.onMouseUpCallback += OnMouseUp;
					reorderableList.drawElementCallback += DrawElement;
					reorderableList.drawElementBackgroundCallback += DrawElementBackground;
					reorderableList.drawHeaderCallback += DrawHeader;
					reorderableList.headerHeight = 0;
					reorderableList.footerHeight = 0;
				}
			}

			void Add(ReorderableList list)
			{
				DatabaseListObject instance = target as DatabaseListObject;
				if (reorderableList.index >= 0)
					instance.items.Insert(reorderableList.index, null);
				else
					instance.items.Add(null);
			}
			void Remove(ReorderableList list)
			{
				DatabaseListObject instance = target as DatabaseListObject;
				instance.items.RemoveAt(reorderableList.index);
			}
			bool CanAdd(ReorderableList list)
			{
				DatabaseListObject instance = target as DatabaseListObject;
				return instance != null;
			}
			bool CanRemove(ReorderableList list)
			{
				DatabaseListObject instance = target as DatabaseListObject;
				return instance != null && instance.items.Count > reorderableList.index && reorderableList.index >= 0;
			}
			void OnMouseUp(ReorderableList list)
			{

			}
			void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
			{
				DatabaseListObject instance = target as DatabaseListObject;
				GUI.Label(rect, instance.items[index].dataName);
			}
			void DrawElementBackground(Rect rect, int index, bool isActive, bool isFocused)
			{

			}
			void DrawHeader(Rect rect)
			{
				EditorGUI.LabelField(rect, "データベースリスト");
			}
			#endregion
			public void OnEnable()
			{
				InitReorderableList();
			}

			public override void OnInspectorGUI()
			{
				DatabaseListObject instance = target as DatabaseListObject;

				reorderableList.DoLayoutList();
			}
		}
#endif
	}
}