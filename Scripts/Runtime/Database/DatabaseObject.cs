﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CommonPackage
{
	[Serializable]
	public class DatabaseObject : ScriptableObject
	{
#if UNITY_EDITOR
		static Color NORMAL_COLOR = new Color(0.5f, 0.5f, 0.5f, 0.5f);

		public string dataName = "";
		
		public int chipSize = 16;
		public int chipHNum = 16;
		public float animSpeed = 8;

		//GUIStyle _popupStyle = null;
		GUIStyle popupStyle
		{
			get
			{
				return EditorStyles.popup;
				//if (_popupStyle == null)
				//{
				//	_popupStyle = new GUIStyle(EditorStyles.popup);
				//	_popupStyle.fontSize = 12;
				//}
				//return _popupStyle;
			}
		}

		public int contentHeight = 100;
		public int contentWidth = 100;
		public int contentHNum = 1;

		public List<ItemSetting> itemSettings = new List<ItemSetting>();

		List<int> animIndexes = new List<int>();
#endif
		public Texture2D texture = null;

		public List<Item> items = new List<Item>();

		static public DatabaseObject Create()
		{
			var ret = CreateInstance<DatabaseObject>();

			ret.items.Add(new Item());

			return ret;
		}


		[Serializable]
		public class Item
		{
			[SerializeField]
			public List<int> intValues = new List<int>();
			[SerializeField]
			public List<string> intKeys = new List<string>();
			[SerializeField]
			public List<float> floatValues = new List<float>();
			[SerializeField]
			public List<string> floatKeys = new List<string>();
			[SerializeField]
			public List<string> stringValues = new List<string>();
			[SerializeField]
			public List<string> stringKeys = new List<string>();
			[SerializeField]
			public List<Color> colorValues = new List<Color>();
			[SerializeField]
			public List<string> colorKeys = new List<string>();
			[SerializeField]
			public List<UnityEngine.Object> objectValues = new List<UnityEngine.Object>();
			[SerializeField]
			public List<string> objectKeys = new List<string>();
			[SerializeField]
			public int hash;

			protected void CheckInt(int index)
			{
				while (intValues.Count <= index)
				{
					intValues.Add(0);
				}
				while (intKeys.Count <= index)
				{
					intKeys.Add(string.Empty);
				}
			}
			public int GetInt(int index)
			{
				CheckInt(index);
				if (index < 0) return 0;
				return intValues[index];
			}
			public int GetInt(string key)
			{
				return GetInt(intKeys.IndexOf(key));
			}
			public void SetInt(int index, string key, int val)
			{
				CheckInt(index);
				intValues[index] = val;
				intKeys[index] = key;
			}

			protected void CheckFloat(int index)
			{
				while (floatValues.Count <= index)
				{
					floatValues.Add(0);
				}
				while (floatKeys.Count <= index)
				{
					floatKeys.Add(string.Empty);
				}
			}
			public float GetFloat(int index)
			{
				CheckFloat(index);
				if (index < 0) return 0;
				return floatValues[index];
			}
			public float GetFloat(string key)
			{
				return GetFloat(floatKeys.IndexOf(key));
			}
			public void SetFloat(int index, string key, float val)
			{
				CheckFloat(index);
				floatValues[index] = val;
				floatKeys[index] = key;
			}

			protected void CheckString(int index)
			{
				while (stringValues.Count <= index)
				{
					stringValues.Add(string.Empty);
				}
				while (stringKeys.Count <= index)
				{
					stringKeys.Add(string.Empty);
				}
			}
			public string GetString(int index)
			{
				CheckString(index);
				if (index < 0) return string.Empty;
				return stringValues[index];
			}
			public string GetString(string key)
			{
				return GetString(stringKeys.IndexOf(key));
			}
			public void SetString(int index, string key, string val)
			{
				CheckString(index);
				stringValues[index] = val;
				stringKeys[index] = key;
			}

			protected void CheckColor(int index)
			{
				while (colorValues.Count <= index)
				{
					colorValues.Add(Color.white);
				}
				while (colorKeys.Count <= index)
				{
					colorKeys.Add(string.Empty);
				}
			}
			public Color GetColor(int index)
			{
				CheckColor(index);
				if (index < 0) return Color.white;
				return colorValues[index];
			}
			public Color GetColor(string key)
			{
				return GetColor(colorKeys.IndexOf(key));
			}
			public void SetColor(int index, string key, Color val)
			{
				CheckColor(index);
				colorValues[index] = val;
				colorKeys[index] = key;
			}

			protected void CheckObject(int index)
			{
				while (objectValues.Count <= index)
				{
					objectValues.Add(null);
				}
				while (objectKeys.Count <= index)
				{
					objectKeys.Add(string.Empty);
				}
			}
			public T GetObject<T>(int index) where T : UnityEngine.Object
			{
				CheckObject(index);
				if (index >= 0 && objectValues[index] is T)
					return (T)objectValues[index];
				else
					return null;
			}
			public T GetObject<T>(string key) where T : UnityEngine.Object
			{
				return GetObject<T>(objectKeys.IndexOf(key));
			}
			public void SetObject(int index, string key, UnityEngine.Object val)
			{
				CheckObject(index);
				objectValues[index] = val;
				objectKeys[index] = key;
			}

			public Item GetItem()
			{
				var ret = new Item();
				ret.intValues = intValues;
				ret.intKeys = intKeys;
				ret.floatValues = floatValues;
				ret.floatKeys = floatKeys;
				ret.stringValues = stringValues;
				ret.stringKeys = stringKeys;
				ret.colorValues = colorValues;
				ret.colorKeys = colorKeys;
				ret.objectValues = objectValues;
				ret.objectKeys = objectKeys;
				ret.hash = hash;
				return ret;
			}
		}

#if UNITY_EDITOR
		Color lightGrey = new Color(1, 1, 0.95f, 1);
		public void OnEditorGUI(Rect maskRect, double timeCount)
		{
			EditorGUILayout.BeginVertical();
			int min = Mathf.Max(0, (int)(maskRect.yMin / ((float)contentHeight / contentHNum)) - 1);
			int max = Mathf.Min(items.Count, (int)(maskRect.yMax / ((float)contentHeight / contentHNum)) + 1);
			min = Mathf.Clamp(min - (min % contentHNum), 0, items.Count);
			max = Mathf.Clamp(max + (contentHNum - (max % contentHNum)), 0, items.Count);
			EditorGUILayout.GetControlRect(GUILayout.Height((min) * contentHeight / contentHNum));
			GUILayout.BeginHorizontal();
			for (int i = min; i < max; i++)
			{
				if (i % contentHNum == 0 && i != 0)
				{
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();
				}

				//GUI.backgroundColor = i % 2 == 0 ? Color.white : lightGrey;
				OnEditorGUI(maskRect, i, timeCount);
			}
			GUI.backgroundColor = Color.white;
			GUILayout.EndHorizontal();
			EditorGUILayout.GetControlRect(GUILayout.Height(((items.Count - max) + 1) * contentHeight / contentHNum));
			EditorGUILayout.EndVertical();
		}

		public void OnEditorGUI(Rect maskRect, int i, double timeCount)
		{

			if (i >= items.Count) return;
			GUILayout.BeginVertical(GUI.skin.box, GUILayout.Height(contentHeight), GUILayout.Width(contentWidth));
			CommonHeader(i);

			int intCnt = 0, floatCnt = 0, stringCnt = 0, objectCnt = 0, colorCnt = 0;
			for (int j = 0; j < itemSettings.Count; j++)
			{
				var setting = itemSettings[j];
				switch (setting.type)
				{
					case ItemSetting.eType.START_HORIZONTAL:
						GUILayout.BeginHorizontal();
						break;
					case ItemSetting.eType.END_HORIZONTAL:
						GUILayout.EndHorizontal();
						break;
					case ItemSetting.eType.START_VERTICAL:
						GUILayout.BeginVertical();
						break;
					case ItemSetting.eType.END_VERTICAL:
						GUILayout.EndVertical();
						break;
					case ItemSetting.eType.INT_FIELD:
						items[i].SetInt(intCnt, setting.key, IntField(setting.key, items[i].GetInt(intCnt), setting.width, setting.height));
						intCnt++;
						break;
					case ItemSetting.eType.FLOAT_FIELD:
						items[i].SetFloat(floatCnt, setting.key, FloatField(setting.key, items[i].GetFloat(floatCnt), setting.width, setting.height));
						floatCnt++;
						break;
					case ItemSetting.eType.STRING_FIELD:
						items[i].SetString(stringCnt, setting.key, TextField(setting.key, items[i].GetString(stringCnt), setting.width, setting.height));
						stringCnt++;
						break;
					case ItemSetting.eType.COLOR_FIELD:
						items[i].SetColor(colorCnt, setting.key, ColorField(setting.key, items[i].GetColor(colorCnt), setting.width, setting.height));
						colorCnt++;
						break;
					case ItemSetting.eType.GAME_OBJECT_FIELD:
						items[i].SetObject(objectCnt, setting.key, ObjectField(setting.key, items[i].GetObject<GameObject>(objectCnt), setting.width, setting.height));
						objectCnt++;
						break;
					case ItemSetting.eType.TEXTURE_FIELD:
						items[i].SetObject(objectCnt, setting.key, ObjectField(setting.key, items[i].GetObject<Texture2D>(objectCnt), setting.width, setting.height));
						objectCnt++;
						break;
					case ItemSetting.eType.SPRITE_FIELD:
						items[i].SetObject(objectCnt, setting.key, ObjectField(setting.key, items[i].GetObject<Sprite>(objectCnt), setting.width, setting.height));
						objectCnt++;
						break;
					case ItemSetting.eType.DRAW_TEXTURE:
						var rect = EditorGUILayout.GetControlRect(GetOption(setting.width, setting.height));
						//DrawChip(rect, items[i].GetInt(setting.key), NORMAL_COLOR, texture, maskRect);
						DrawTextures(rect, maskRect, i, ref j, timeCount);
						break;
					case ItemSetting.eType.SCRIPTABLE_OBJECT_FIELD:
						items[i].SetObject(objectCnt, setting.key, ObjectField(setting.key, items[i].GetObject<ScriptableObject>(objectCnt), setting.width, setting.height));
						objectCnt++;
						break;
				}
			}

			GUILayout.EndVertical();
		}

		public void DrawTextures(Rect rect, Rect maskRect, int i, ref int j, double timeCount)
		{
			animIndexes.Clear();
			for (int k = j; k < itemSettings.Count; k++)
			{
				var setting = itemSettings[k];
				if (setting.type == ItemSetting.eType.DRAW_TEXTURE)
				{
					animIndexes.Add(items[i].GetInt(setting.key));
					j = k; 
				}
				else
				{
					break;
				}
			}

			DrawChip(rect, animIndexes[(int)((timeCount * animSpeed) % animIndexes.Count)], NORMAL_COLOR, texture, maskRect);
		}
#endif

#if UNITY_EDITOR
		#region Helper
		GUILayoutOption[] GetOption(int width, int height)
		{
			var ret = new List<GUILayoutOption>();
			if (width > 0)
			{
				ret.Add(GUILayout.Width(width));
			}
			if (height > 0)
			{
				ret.Add(GUILayout.Height(height));
			}
			return ret.ToArray();
		}

		int IntField(string label, int value, int width = -1, int height = -1)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = EditorGUILayout.IntField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
		}

		int BoolField(string label, int value, int width = -1, int height = -1)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = EditorGUILayout.Toggle(value > 0, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret ? 1 : 0;
		}

		bool BoolField(string label, bool value, int width = -1, int height = -1)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = EditorGUILayout.Toggle(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
		}

		float FloatField(string label, float value, int width = -1, int height = -1)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = EditorGUILayout.FloatField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
		}

		string TextField(string label, string value, int width = -1, int height = -1, bool isMultiLine = false)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = isMultiLine ? EditorGUILayout.TextArea(value, GetOption(width, height)) : EditorGUILayout.TextField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
		}

		Color ColorField(string label, Color value, int width = -1, int height = -1)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = EditorGUILayout.ColorField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
		}

		T ObjectField<T>(string label, T value, int width = -1, int height = -1) where T : UnityEngine.Object
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(label);
			var ret = EditorGUILayout.ObjectField(value, typeof(T), false, GetOption(width, height)) as T;
			GUILayout.EndHorizontal();
			return ret;
		}

		void CommonHeader(int i)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("ID" + i.ToString("000"));

			//GUILayout.Label(" hash" + items[i].hash);

			GUILayout.FlexibleSpace();
			if (GUILayout.Button("↑Add", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(42)))
			{
				Undo.RecordObject(this, "Database Editor : Add");
				items.Insert(i, new Item());
			}
			EditorGUI.BeginDisabledGroup(i == 0);
			if (GUILayout.Button("▲", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(32)))
			{
				Undo.RecordObject(this, "Database Editor : Move");
				var tmp = items[i];
				items.RemoveAt(i);
				items.Insert(i - 1, tmp);
			}
			EditorGUI.EndDisabledGroup();
			EditorGUI.BeginDisabledGroup(i == items.Count - 1);
			if (GUILayout.Button("▼", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(32)))
			{
				Undo.RecordObject(this, "Database Editor : Move");
				var tmp = items[i];
				items.RemoveAt(i);
				items.Insert(i + 1, tmp);
			}
			EditorGUI.EndDisabledGroup();
			if (GUILayout.Button("×", EditorStyles.miniButton, GUILayout.Height(12), GUILayout.Width(20)))
			{
				Undo.RecordObject(this, "Database Editor : Remove");
				items.RemoveAt(i);
				return;
			}
			GUILayout.EndHorizontal();
		}

		void SetHash(ref int h)
		{
			if (h <= 0)
			{
				bool loop = true;
				while (loop)
				{
					loop = false;
					int hash = UnityEngine.Random.Range(0, 2147483646);
					foreach (var item in items)
					{
						if (item.hash == hash)
						{
							loop = true;
							continue;
						}
					}
					h = hash;
				}
			}
		}

		public void DrawChip(Rect rect, int index, Color color, Texture2D texture, Rect maskRect = new Rect(), float angle = 0)
		{
			if (texture == null) return;
			Rect srcRect = new Rect(
				(float)(index % chipHNum * chipSize) / texture.width,
				(float)(texture.height - index / chipHNum * chipSize - chipSize) / texture.height,
				(float)chipSize / texture.width,
				(float)chipSize / texture.height);
			Utility.DrawTexture(srcRect, rect, color, texture, maskRect, angle);
			//DrawTexture(srcRect, rect, color, texture, maskRect, angle);
		}
		#endregion

		[Serializable]
		public class ItemSetting
		{
			public enum eType
			{
				NONE,
				START_HORIZONTAL,
				END_HORIZONTAL,
				START_VERTICAL,
				END_VERTICAL,
				INT_FIELD,
				FLOAT_FIELD,
				STRING_FIELD,
				COLOR_FIELD,
				GAME_OBJECT_FIELD,
				TEXTURE_FIELD,
				SPRITE_FIELD,
				DRAW_TEXTURE,
				SCRIPTABLE_OBJECT_FIELD,
			}

			[SerializeField]
			public eType type = eType.NONE;
			[SerializeField]
			public string key = string.Empty;
			[SerializeField]
			public int width = -1;
			[SerializeField]
			public int height = -1;
		}
#endif
	}
}