﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CommonPackage
{
	[Serializable]
	public class TagDataObject : ScriptableObject
	{
		public List<Tag> tags = new List<Tag>();

		public string fileName = "tag000";

		[Serializable]
		public class Tag
		{
			public string name;
#if UNITY_EDITOR
			public string label;
			public Color color = Color.white;
			public GUIStyle style = new GUIStyle();
#endif
		}

		static public TagDataObject Create()
		{
			var ret = CreateInstance<TagDataObject>();
			return ret;
		}
	}
}