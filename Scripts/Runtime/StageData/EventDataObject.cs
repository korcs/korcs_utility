﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using CommonPackage.Chiptune;
using CommonPackage.RetroEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CommonPackage
{
	[Serializable]
	public class EventDataObject : ScriptableObject
	{
		[SerializeReference]
		public List<IEvent> events = new List<IEvent>();

		public string fileName = "event000";

		public Texture2D tex = null;
		public Vector2Int size = new Vector2Int(16, 16);
		public List<int> indexes = new List<int>();
		public float speed = 1;

		// 起動条件

		// 当たり判定
		public Rect collision = new Rect(0, 0, 16, 16);

		public int hNum { get { return tex != null ? tex.width / size.x : 1; } }
		public int width { get { return tex != null ? tex.width : 0; } }
		public int height { get { return tex != null ? tex.height : 0; } }

		const int DEFAULT_WIDTH = 300;

		/*
		 * メッセージ 
		 * 移動
		 * マップ移動
		 * 画像変更
		 * 画面効果（トランジション）
		 * 消滅
		 * 変数
		 * アイテム獲得
		 * 条件分岐
		 * 分岐終了
		 * 並列実行
		 * BGM
		 * SE
		 * 敵
		 * 
		 */
		
		public interface IEvent
		{
			string name { get; }
			string content { get; }

			void Exec();
			bool IsNext();
			int priority { get; }
			void Edit();
		}

		[Serializable]
		public class EventBranch : IEvent
		{
			public string name { get { return "条件分岐"; } }
			public string content { get; }

			public bool isStart = true;
			public EventBranch connect = null;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return -10; } }
			public void Edit()
			{

			}
		}

		[Serializable]
		public class EventParallel : IEvent
		{
			public string name { get { return "並列実行"; } }
			public string content { get; }

			public bool isStart = true;
			public EventParallel connect = null;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return -1; } }
			public void Edit()
			{

			}
		}

		[Serializable]
		public class EventWait : IEvent
		{
			public string name { get { return "ウェイト"; } }
			public string content { get { return string.Format("{0:0.00}", wait); } }

			public float wait = 0;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 0; } }
			public void Edit()
			{
				wait = FloatField("時間", wait);
			}
		}

		[Serializable]
		public class EventMessage : IEvent
		{
			public string name { get { return "メッセージ"; } }
			public string content { get { 
					return (message.Length > 5 ? message.Replace('\n', ' ').Substring(0, 5) + "…" : message.Replace('\n', ' ')); 
				} }

			public string message = "";
			public float speed = 0;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 1; } }
			public void Edit()
			{
				message = TextField("テキスト", message, true, DEFAULT_WIDTH, 80);
				speed = FloatField("表示速度", speed);
			}
		}

		[Serializable]
		public class EventMove : IEvent
		{
			public string name { get { return "場所移動"; } }
			public string content { get { return pos.ToString(); } }

			public Vector2Int pos = Vector2Int.zero;
			public float duration = 0;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 10; } }
			public void Edit()
			{
				pos = Vector2IntField("座標", pos);
				duration = FloatField("時間", duration);
			}
		}

		[Serializable]
		public class EventMap : IEvent
		{
			public string name { get { return "マップ移動"; } }
			public string content { get { return data != null ? data.fileName : "None"; } }

			public StageDataObject data = null;
			public Vector2Int pos = Vector2Int.zero;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 11; } }
			public void Edit()
			{
				data = ObjectField("マップ", data);
				pos = Vector2IntField("座標", pos);
			}
		}

		[Serializable]
		public class EventImage : IEvent
		{
			public string name { get { return "画像変更"; } }
			public string content { get { return tex != null ? tex.name : "None"; } }

			public Texture2D tex = null;
			public Vector2Int size = new Vector2Int(16, 16);
			public List<int> indexes = new List<int>();
			public float speed = 1;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 20; } }
			public void Edit()
			{
				tex = ObjectField("テクスチャ", tex);
				size = Vector2IntField("サイズ", size);
				indexes = ListIntField("インデックス", indexes);
				speed = FloatField("速度", speed);
			}
		}

		[Serializable]
		public class EventScreenEffect : IEvent
		{
			public string name { get { return "画面効果"; } }
			public string content { get { return transition.ToString(); } }

			public RetroEngineTexture.eTransition transition = RetroEngineTexture.eTransition.NONE;
			public float duration = 1;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 21; } }
			public void Edit()
			{
				transition = EnumField("画面効果", transition);
				duration = FloatField("時間", duration);
			}
		}

		[Serializable]
		public class EventRemove : IEvent
		{
			public string name { get { return "消滅"; } }
			public string content { get; }


			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 22; } }
			public void Edit()
			{

			}
		}

		[Serializable]
		public class EventVariable : IEvent
		{
			public string name { get { return "変数"; } }
			public string content { get; }


			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 30; } }
			public void Edit()
			{

			}
		}

		[Serializable]
		public class EventBGM : IEvent
		{
			public string name { get { return "BGM"; } }
			public string content { get { return data != null ? data.fileName : "None"; } }

			public ChiptuneDataObject data = null;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 50; } }
			public void Edit()
			{
				data = ObjectField("BGM", data);
			}
		}

		[Serializable]
		public class EventSE : IEvent
		{
			public string name { get { return "SE"; } }
			public string content { get { return data != null ? data.fileName : "None"; } }

			public ChiptuneDataObject data = null;

			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 51; } }
			public void Edit()
			{
				data = ObjectField("SE", data);
			}
		}

		/*
		[Serializable]
		public class EventTest : IEvent
		{
			public string name { get { return "Test2"; } }
			public string content { get; }
			public void Exec() { }
			public bool IsNext() { return true; }
			public int priority { get { return 1; } }
			public void Edit()
			{
				
			}
		}
		*/

		static public EventDataObject Create()
		{
			var ret = CreateInstance<EventDataObject>();
			return ret;
		}

		#region Helper
		static public GUILayoutOption[] GetOption(int width, int height)
		{
			var ret = new List<GUILayoutOption>();
			if (width > 0)
			{
				ret.Add(GUILayout.Width(width));
			}
			if (height > 0)
			{
				ret.Add(GUILayout.Height(height));
			}
			return ret.ToArray();
		}

		static public int IntField(string label, int value, int width = DEFAULT_WIDTH, int height = -1)
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = EditorGUILayout.IntField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
#else
			return 0;
#endif
		}

		static public bool BoolField(string label, bool value, int width = DEFAULT_WIDTH, int height = -1)
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = EditorGUILayout.Toggle(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
#else
			return false;
#endif
		}

		static public float FloatField(string label, float value, int width = DEFAULT_WIDTH, int height = -1)
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = EditorGUILayout.FloatField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
#else
			return 0;
#endif
		}

		static public Vector2Int Vector2IntField(string label, Vector2Int value, int width = -1, int height = -1)
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			var ret = EditorGUILayout.Vector2IntField(label, value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
#else
			return Vector2Int.zero;
#endif
		}

		static public string TextField(string label, string value, bool isMultiLine = false, int width = DEFAULT_WIDTH, int height = -1)
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = isMultiLine ? EditorGUILayout.TextArea(value, GetOption(width, height)) : EditorGUILayout.TextField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
#else
			return string.Empty;
#endif
		}

		static public Color ColorField(string label, Color value, int width = DEFAULT_WIDTH, int height = -1)
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = EditorGUILayout.ColorField(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return ret;
#else
			return Color.clear;
#endif
		}

		static public T ObjectField<T>(string label, T value, int width = DEFAULT_WIDTH, int height = -1) where T : UnityEngine.Object
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = EditorGUILayout.ObjectField(value, typeof(T), false, GetOption(width, height)) as T;
			GUILayout.EndHorizontal();
			return ret;
#else
			return null;
#endif
		}

		static public T EnumField<T>(string label, T value, int width = DEFAULT_WIDTH, int height = -1) where T : Enum
		{
#if UNITY_EDITOR
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(label);
			var ret = EditorGUILayout.EnumPopup(value, GetOption(width, height));
			GUILayout.EndHorizontal();
			return (T)ret;
#else
			return value;
#endif
		}

		static public List<int> ListIntField(string label, List<int> value, int width = -1, int height = -1)
		{
#if UNITY_EDITOR
			var ret = value;
			int n = ret.Count;
			EditorGUI.BeginChangeCheck();
			n = Mathf.Clamp(IntField(label, n), 0, 16);
			if (EditorGUI.EndChangeCheck())
			{
				while (n > ret.Count)
				{
					ret.Add(0);
				}
				while (n < ret.Count)
				{
					ret.RemoveAt(ret.Count - 1);
				}
			}
			using (new GUILayout.HorizontalScope(GUI.skin.box))
			{
				for (int i = 0; i < ret.Count; i++)
				{
					ret[i] = EditorGUILayout.IntField(ret[i], GUILayout.Width(32));
				}
			}
			return ret;
#else
			return value;
#endif
		}
		#endregion
	}
}