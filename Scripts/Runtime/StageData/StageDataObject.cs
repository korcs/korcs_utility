﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using CommonPackage.Chiptune;

namespace CommonPackage
{
    [Serializable]
    public class StageDataObject : ScriptableObject
    {
        public List<X> blocks = new List<X>();

        public new string name = "empty";
        public string fileName = "stage000";
        public byte sizeX = 9;
        public byte sizeY = 9;
        public byte screenSizeX = 9;
        public byte screenSizeY = 9;
		
		public List<Event> events = new List<Event>();

        //public int bgmID = 0;
        public ChiptuneDataObject bgmData = null;
        //public int exp = 0;
        public TileDataObject tileData = null;

        [Serializable]
        public class X
        {
            public List<Y> y = new List<Y>();
        }

        [Serializable]
        public class Y
        {
            public int i = 0;
            public int hash = 0;
            public int tag = 0;
        }

        [Serializable]
        public class Event
        {
            public int x;
            public int y;

			[SerializeReference]
            public EventDataObject ev = null;
            public bool isLocal;
        }

        static public StageDataObject Create()
        {
            var ret = CreateInstance<StageDataObject>();
            for (int x = 0; x < 9; x++)
            {
                var tmpY = new X();
                for (int y = 0; y < 9; y++)
                {
                    var tmpZ = new Y();
                    tmpY.y.Add(tmpZ);
                }
                ret.blocks.Add(tmpY);
            }
			ret.events = new List<Event>();
            return ret;
        }

    }
}