﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CommonPackage
{
	[Serializable]
	public class TileDataObject : ScriptableObject
	{
		public List<Tile> tiles = new List<Tile>();
		public int size = 16;
		public Texture2D texture = null;
		public TagDataObject tagData = null;

		public string fileName = "tile000";

		[Serializable]
		public class Tile
		{
			public List<int> ids = new List<int>();
			public bool isAnimate = false;
			public float speed = 0.5f;
			public bool isMovable = true;
			public int tag = 0;
			public int hash = 0;
		}

		static public TileDataObject Create()
		{
			var ret = CreateInstance<TileDataObject>();
			return ret;
		}

		public void SetHash(int i)
		{
			if (i < 0 || i >= tiles.Count) return;

			if (tiles[i].hash <= 0)
			{
				bool loop = true;
				while (loop)
				{
					loop = false;
					int hash = UnityEngine.Random.Range(0, 2147483646);
					foreach (var tile in tiles)
					{
						if (tile.hash == hash)
						{
							loop = true;
							continue;
						}
					}
					tiles[i].hash = hash;
				}
			}
		}
	}
}