﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.Chiptune
{
	public class ChiptuneWave
	{
		const float PI = Mathf.PI;
		const float PI2 = PI * 2.0f;
		const float PI_2 = PI / 2.0f;
		const float sampling_frequency = 48000;
		const float PI2_SR = PI2 / sampling_frequency;
		public enum PlayState
		{
			None,
			SquareWave_0125,
			SquareWave_0250,
			SquareWave_0500,
			TriangleWave,
			PseudoTriangleWave,
			SineWave,
			SawtoothWave,
			Noise,
			ShortNoise,
			WaveformMemory,
		}

		[SerializeField]
		PlayState playState = PlayState.None;
		float frequency = 440;
		[SerializeField]
		float gain = 0.05f;
		[SerializeField]
		int musicalScale = 48;
		//[SerializeField]
		//float noisePitch = 1;
		[SerializeField]
		List<byte> waveformMemorys = new List<byte>(16);

		float increment;
		float time;
		int noise = 0xffff;
		int noiseOutput = 1;

		float[] frequencys = new float[88];

		public ChiptuneWave()
		{
			// 音階周波数初期化
			var step = Mathf.Pow(2f, 1f / 12f);
			var f = 440f / 16f;
			for (int i = 0; i < frequencys.Length; i++)
			{
				frequencys[i] = f;
				f *= step;
			}
		}

		public void Set(PlayState state, float gain = 0.05f, int scale = 48)
		{
			if (playState != state)
			{
				noise = 0xffff;
				noiseOutput = 1;
			}
			playState = state;
			this.gain = gain;
			musicalScale = scale;
			//if (state == PlayState.Noise || state == PlayState.ShortNoise)
			//{
			//	audioSource.pitch = (float)scale / frequencys.Length * 3.0f;
			//}
			//else
			//{
			//	audioSource.pitch = 1;
			//}
		}

		public void SetWaveformMemory(List<byte> waveformMemorys)
		{
			this.waveformMemorys = waveformMemorys;
		}

		float SineWave()
		{
			return (gain * Mathf.Sin(time));
		}

		float SquareWave(float duty)
		{
			return (float)(gain * ((time % PI2) < PI2 * duty ? 0.5f : -0.5f));
		}

		float TriangleWave(bool pseudo)
		{
			float t = (time + PI_2) % PI2;
			if (pseudo)
			{
				// 疑似三角波
				int h = (int)(((t < PI ? t - PI : PI - t) / PI_2 + 1.0f) * 8);
				return (gain * (h / 8.0f));
			}
			else
			{
				return (gain * ((t < PI ? t - PI : PI - t) / PI_2 + 1.0f));
			}
		}


		float SawtoothWave()
		{
			return (gain * ((time + PI) % PI2) / PI - 1.0f);
		}

		float Noise(bool shortFreq)
		{
			if (noise == 0) noise = 1;
			noise += noise + (((noise >> (shortFreq ? 6 : 14)) ^ (noise >> (shortFreq ? 5 : 13))) & 1);
			noiseOutput ^= noise & 1;
			return (gain * noiseOutput);
		}

		float WaveformMemory()
		{
			int t = (int)Mathf.Clamp((time % PI2) / PI2 * waveformMemorys.Count, 0, waveformMemorys.Count);
			return gain * (waveformMemorys[t] - 127.0f) / 256.0f;
		}

		public void OnAudioRead(float[] data, int channels)
		{
			if (musicalScale < frequencys.Length && musicalScale >= 0)
				frequency = frequencys[musicalScale];

			increment = frequency * 2 * PI / sampling_frequency;
			for (var i = 0; i < data.Length; i = i + channels)
			{
				time = time + increment;
				switch (playState)
				{
					case PlayState.SineWave:
						data[i] = SineWave();
						break;
					case PlayState.SquareWave_0125:
						data[i] = SquareWave(0.125f);
						break;
					case PlayState.SquareWave_0250:
						data[i] = SquareWave(0.25f);
						break;
					case PlayState.SquareWave_0500:
						data[i] = SquareWave(0.5f);
						break;
					case PlayState.TriangleWave:
						data[i] = TriangleWave(false);
						break;
					case PlayState.PseudoTriangleWave:
						data[i] = TriangleWave(true);
						break;
					case PlayState.SawtoothWave:
						data[i] = SawtoothWave();
						break;
					case PlayState.Noise:
						data[i] = Noise(false);
						break;
					case PlayState.ShortNoise:
						data[i] = Noise(true);
						break;
					case PlayState.WaveformMemory:
						data[i] = WaveformMemory();
						break;
				}
				if (channels == 2)
					data[i + 1] = data[i];
				if (time > PI2)
					time -= PI2;
			}
		}
	}
}