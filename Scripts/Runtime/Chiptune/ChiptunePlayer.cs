﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.Chiptune
{
	public class ChiptunePlayer : MonoBehaviour
	{
		[SerializeField, Range(0, 1)]
		float volume = 0.5f;

		[SerializeField]
		ChiptuneDataObject bgmData = null;
		[SerializeField]
		ChiptuneDataObject seData = null;

		List<ChiptuneChannel> channels = new List<ChiptuneChannel>();

		const int CHANNEL_NUM = 4;

		float bgmTime = 0;
		float seTime = 0;
		bool isPlaying = false;

		List<int> playingNoteIndexs = new List<int>();
		List<int> playingSENoteIndexs = new List<int>();

		public float Volume { get { return volume; } set { volume = value; } }

		void Awake()
		{
			for (int i = 0; i < CHANNEL_NUM; i++)
			{
				var obj = new GameObject("ch" + (i + 1));
				obj.transform.SetParent(transform);
				obj.transform.localPosition = Vector3.zero;

				var ch = obj.AddComponent<ChiptuneChannel>();
				ch.Set(ChiptuneWave.PlayState.None);
				ch.audioSource.volume = volume;
				channels.Add(ch);

				playingNoteIndexs.Add(0);
				playingSENoteIndexs.Add(0);
			}
		}

		void FixedUpdate()
		{
			if (bgmData != null && isPlaying)
			{
				bgmTime += Time.fixedDeltaTime;
				float t = bgmTime * (bgmData.bpm / 15.0f);
				if (t >= bgmData.length)
				{
					t = t - bgmData.length + bgmData.loopPoint;
					bgmTime = t / (bgmData.bpm / 15.0f);
				}
				for (int ch = 0; ch < CHANNEL_NUM; ch++)
				{
					// 再生中のノート
					var n = bgmData.channels[ch].noteDatas[playingNoteIndexs[ch]];
					if (n == null || t > playingNoteIndexs[ch] + n.length || t < playingNoteIndexs[ch])
					{
						playingNoteIndexs[ch] = (int)t;
						n = bgmData.channels[ch].noteDatas[playingNoteIndexs[ch]];
					}

					if (n != null && n.length > 0)
					{
						float f = t - playingNoteIndexs[ch];
						float gain = (n.beginGain + (n.endGain - n.beginGain) * (f / n.length)) / 128.0f;
						channels[ch].Set(bgmData.channels[ch].state, gain, n.musicalScale);
						channels[ch].SetWaveformMemory(bgmData.waveformMemorys);
						channels[ch].audioSource.volume = volume;
					}
					else
					{
						channels[ch].Set(ChiptuneWave.PlayState.None);
					}

				}
			}
			if (seData != null)
			{
				seTime += Time.fixedDeltaTime;
				float t = seTime * (seData.bpm / 15.0f);
				if (t >= seData.length)
				{
					seData = null;
					return;
				}
				for (int ch = 0; ch < CHANNEL_NUM; ch++)
				{
					// 再生中のノート
					var n = seData.channels[ch].noteDatas[playingSENoteIndexs[ch]];
					if (n == null || t > playingSENoteIndexs[ch] + n.length || t < playingSENoteIndexs[ch])
					{
						playingSENoteIndexs[ch] = (int)t;
						n = seData.channels[ch].noteDatas[playingSENoteIndexs[ch]];
					}

					if (n != null && n.length > 0)
					{
						float f = t - playingSENoteIndexs[ch];
						float gain = (n.beginGain + (n.endGain - n.beginGain) * (f / n.length)) / 128.0f;
						channels[ch].Set(seData.channels[ch].state, gain, n.musicalScale);
						channels[ch].SetWaveformMemory(seData.waveformMemorys);
						channels[ch].audioSource.volume = volume;
					}
				}
			}
		}

		public void PlayBGM(ChiptuneDataObject data, int time = 0)
		{
			bgmData = data;
			if (data == null) return;
			bgmTime = time / (bgmData.bpm / 15.0f);
			isPlaying = true;
			for (int ch = 0; ch < CHANNEL_NUM; ch++)
			{
				playingNoteIndexs[ch] = 0;
			}
		}

		public void PlaySE(ChiptuneDataObject data)
		{
			seData = data;
			if (data == null) return;
			seTime = 0;
			for (int ch = 0; ch < CHANNEL_NUM; ch++)
			{
				playingSENoteIndexs[ch] = 0;
			}
		}

		public bool IsPlayingSE()
		{
			return seData != null;
		}

		public void StopBGM()
		{
			bgmData = null;
			isPlaying = false;

			for (int ch = 0; ch < CHANNEL_NUM; ch++)
			{
				channels[ch].Set(ChiptuneWave.PlayState.None);
			}
		}

		public void PlayTone(ChiptuneWave.PlayState state, int beginGain, int endGain, int scale, int length, int bpm)
		{
			StopBGM();
			StopAllCoroutines();
			StartCoroutine(Tone(state, beginGain, endGain, scale, length, bpm));
		}

		IEnumerator Tone(ChiptuneWave.PlayState state, int beginGain, int endGain, int scale, int length, int bpm)
		{
			float t = 0;
			while (t < length)
			{
				t += Time.deltaTime * (bpm / 15.0f);

				float gain = (beginGain + (endGain - beginGain) * (t / length)) / 128.0f;
				channels[0].Set(state, gain, scale);
				channels[0].audioSource.volume = volume;
				yield return null;
			}
			channels[0].Set(ChiptuneWave.PlayState.None);
		}

		public void StopTone()
		{
			StopBGM();
			StopAllCoroutines();
		}

		public float GetTime()
		{
			if (bgmData == null) return 0;
			return bgmTime * (bgmData.bpm / 15.0f);
		}
	}
}