﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.Chiptune
{
	[Serializable]
	public class ChiptuneDataObject : ScriptableObject
	{
		public string fileName = "";

		public List<ChannelData> channels = new List<ChannelData>();
		public int bpm = 120;// 1分当たりの4分音符の数
		public int loopPoint = 0;
		public int length = 16;

		public List<byte> waveformMemorys = new List<byte>(16);

		[Serializable]
		public class ChannelData
		{
			public ChiptuneWave.PlayState state = ChiptuneWave.PlayState.None;
			public List<NoteData> noteDatas = new List<NoteData>();
		}

		[Serializable]
		public class NoteData
		{
			public byte beginGain = 128;
			public byte endGain = 128;
			public byte musicalScale = 48;
			public byte length = 0;
		}

		static public ChiptuneDataObject Create()
		{
			var ret = CreateInstance<ChiptuneDataObject>();
			for (int i = 0; i < 4; i++)
			{
				ret.channels.Add(new ChannelData());
				for (int j = 0; j < 16; j++)
				{
					ret.channels[i].noteDatas.Add(new NoteData());
				}
			}
			return ret;
		}
	}
}