﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonPackage.Chiptune
{
	public class ChiptuneChannel : MonoBehaviour
	{
		ChiptuneWave wave = null;

		AudioSource _audioSource;
		public AudioSource audioSource
		{
			get
			{
				if (_audioSource == null)
				{
					_audioSource = gameObject.AddComponent<AudioSource>();
				}
				return _audioSource;
			}
		}

		void Awake()
		{
			wave = new ChiptuneWave();
		}

		public void Set(ChiptuneWave.PlayState state, float gain = 0.05f, int scale = 48)
		{
			wave.Set(state, gain, scale);
			if (state == ChiptuneWave.PlayState.Noise || state == ChiptuneWave.PlayState.ShortNoise)
			{
				audioSource.pitch = (float)scale / 88 * 3.0f;
			}
			else
			{
				audioSource.pitch = 1;
			}
		}

		public void SetWaveformMemory(List<byte> waveformMemorys)
		{
			wave.SetWaveformMemory(waveformMemorys);
		}

		void OnAudioFilterRead(float[] data, int channels)
		{
			wave.OnAudioRead(data, channels);
		}
	}
}